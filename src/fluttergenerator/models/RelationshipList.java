/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fluttergenerator.models;

import java.util.ArrayList;

/**
 *
 * @author Jasper
 */
public class RelationshipList {
    ArrayList<Relationship> relationships;

    public RelationshipList() {

    }

    public RelationshipList(ArrayList<Relationship> relationships) {
        this.relationships = relationships;
    }

    public ArrayList<Relationship> getRelationships() {
        return relationships;
    }

    public void setRelationships(ArrayList<Relationship> relationships) {
        this.relationships = relationships;
    }

    public ArrayList<Relationship> getSuperTableRelationships(String table) {
        ArrayList<Relationship> rs = new ArrayList();
        for (Relationship r : relationships) {
            if (r.table2.contentEquals(table))
                rs.add(r);
        }
        return rs;
    }

    public ArrayList<Relationship> getSubTableRelationships(String table) {
        ArrayList<Relationship> rs = new ArrayList<Relationship>();
        for (Relationship r : relationships) {
            if (r.table1.contentEquals(table))
                rs.add(r);
        }
        return rs;
    }

}
