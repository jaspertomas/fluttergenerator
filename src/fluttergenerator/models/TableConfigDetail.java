/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fluttergenerator.models;

import java.util.ArrayList;

/**
 *
 * Represents a single super table, sub table or field. in the individual table
 * configuration files in generator_configs
 */
public class TableConfigDetail {
    String name;
    String field;
    String singular;
    String plural;
    String namefield;
    String type;
    String code;
    ArrayList<CustomTag> customTags;
    ArrayList<Option> options;

    public TableConfigDetail() {
    }

    public TableConfigDetail(String name, String field, String singular, String plural, String namefield, String type,
            String code, ArrayList<CustomTag> customTags, ArrayList<Option> options) {
        this.name = name;
        this.field = field;
        this.singular = singular;
        this.plural = plural;
        this.namefield = namefield;
        this.type = type;
        this.code = code;
        this.customTags = customTags;
        this.options = options;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getField() {
        return field;
    }

    public void setField(String field) {
        this.field = field;
    }

    public String getSingular() {
        return singular;
    }

    public void setSingular(String singular) {
        this.singular = singular;
    }

    public String getPlural() {
        return plural;
    }

    public void setPlural(String plural) {
        this.plural = plural;
    }

    public String getNamefield() {
        return namefield;
    }

    public void setNamefield(String namefield) {
        this.namefield = namefield;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public ArrayList<CustomTag> getCustomTags() {
        return customTags;
    }

    public void setCustomTags(ArrayList<CustomTag> customTags) {
        this.customTags = customTags;
    }

    public ArrayList<Option> getOptions() {
        return options;
    }

    public void setOptions(ArrayList<Option> options) {
        this.options = options;
    }

}
