/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fluttergenerator.models;

import java.util.ArrayList;
import java.util.Collections;

/**
 *
 * Used to read and write the individual configuration files for each table in
 * generator_configs
 */
public class TableConfig {
    // Name of table in database. Snake case please.
    // Some databases use singular form. Others use plural form
    String name;

    // singular form snake case
    String singular;
    // plural form snake case
    String plural;

    // A special field used in searches and other things
    String namefield;

    // Snippet type. Usually used to specify a data type
    // Otherwise allows you to choose from several available snippet types
    String type;

    // abbreviation of table name, usually first 3 letters of each word
    String code;

    // custom tags allow you to create your own string replacement tags
    ArrayList<CustomTag> customTags;

    // options are like "type" but more flexible
    // currently unused
    ArrayList<Option> options;

    // fields to add to the index page.
    // By default they are the first 3 fields except for ID.
    ArrayList<String> indexFields;

    // viewFields are fields to add to the View page.
    // Null by default. If null, will include all fields in "fields"
    ArrayList<String> viewFields;

    // fields to add to the Edit page.
    // Null by default. If null, will include all fields in "fields"
    ArrayList<String> editFields;

    // A list of all fields, with details
    ArrayList<Field> fields;

    // a list of parent tables, with details
    ArrayList<TableConfigDetail> superTables;

    // a list of child tables, with details
    ArrayList<TableConfigDetail> subTables;

    public TableConfig() {

    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSingular() {
        return singular;
    }

    public void setSingular(String singular) {
        this.singular = singular;
    }

    public String getPlural() {
        return plural;
    }

    public void setPlural(String plural) {
        this.plural = plural;
    }

    public String getNamefield() {
        return namefield;
    }

    public void setNamefield(String namefield) {
        this.namefield = namefield;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public ArrayList<Field> getFields() {
        return fields;
    }

    public void setFields(ArrayList<Field> fields) {
        this.fields = fields;
    }

    public ArrayList<TableConfigDetail> getSuperTables() {
        return superTables;
    }

    public void setSuperTables(ArrayList<TableConfigDetail> superTables) {
        this.superTables = superTables;
    }

    public ArrayList<TableConfigDetail> getSubTables() {
        return subTables;
    }

    public void setSubTables(ArrayList<TableConfigDetail> subTables) {
        this.subTables = subTables;
    }

    public void reverseLists() {
        Collections.reverse(this.fields);
        Collections.reverse(this.subTables);
        Collections.reverse(this.superTables);
    }

    public ArrayList<CustomTag> getCustomTags() {
        return customTags;
    }

    public void setCustomTags(ArrayList<CustomTag> customTags) {
        this.customTags = customTags;
    }

    public ArrayList<Option> getOptions() {
        return options;
    }

    public void setOptions(ArrayList<Option> options) {
        this.options = options;
    }

    public ArrayList<String> getIndexFields() {
        return indexFields;
    }

    public void setIndexFields(ArrayList<String> indexFields) {
        this.indexFields = indexFields;
    }

    public ArrayList<String> getViewFields() {
        return viewFields;
    }

    public void setViewFields(ArrayList<String> viewFields) {
        this.viewFields = viewFields;
    }

    public ArrayList<String> getEditFields() {
        return editFields;
    }

    public void setEditFields(ArrayList<String> editFields) {
        this.editFields = editFields;
    }

    public Field getField(String fieldName) {
        for (Field f : fields) {
            if (f.getField().contentEquals(fieldName))
                return f;
        }
        return null;
    }

}
