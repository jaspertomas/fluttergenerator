/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fluttergenerator.models;

import java.util.ArrayList;

/**
 *
 * Used by Schema Generator to write the generator_configs/base/_tables.yml
 * file.
 */
public class Table {
    String table;
    String nameField;
    ArrayList<Field> fields;

    public Table() {

    }

    public Table(String table, String nameField, ArrayList<Field> fields) {
        this.table = table;
        this.nameField = nameField;
        this.fields = fields;
    }

    public String getTable() {
        return table;
    }

    public void setTable(String table) {
        this.table = table;
    }

    public String getNameField() {
        return nameField;
    }

    public void setNameField(String nameField) {
        this.nameField = nameField;
    }

    public ArrayList<Field> getFields() {
        return fields;
    }

    public void setFields(ArrayList<Field> fields) {
        this.fields = fields;
    }

}
