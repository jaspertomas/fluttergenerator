/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fluttergenerator.models;

import java.util.ArrayList;

/**
 *
 * A list of "Tables". Used by Schema Generator to write the
 * generator_configs/base/_tables.yml file.
 */
public class TableList {
    ArrayList<Table> tables;

    public TableList() {

    }

    public TableList(ArrayList<Table> tables) {
        this.tables = tables;
    }

    public ArrayList<Table> getTables() {
        return tables;
    }

    public void setTables(ArrayList<Table> tables) {
        this.tables = tables;
    }

    public Table getTable(String name) {
        for (Table table : tables) {
            if (table.getTable().contentEquals(name))
                return table;
        }
        return null;
    }

}
