package fluttergenerator.models;

import java.util.ArrayList;

/*
This is the switchboard 
and is used to read and write the switchboard yml file
*/
public class TableSwitchBoard {
    ArrayList<TableSwitch> switchboard = new ArrayList<TableSwitch>();

    public TableSwitchBoard() {
    }

    public ArrayList<TableSwitch> getSwitchboard() {
        return this.switchboard;
    }

    public void setSwitchboard(ArrayList<TableSwitch> switchboard) {
        this.switchboard = switchboard;
    }

}
