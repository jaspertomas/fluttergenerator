/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fluttergenerator.models;

import java.util.ArrayList;

/**
 *
 * @author Jasper
 */
public class Field {
    String field;
    String type;
    ArrayList<CustomTag> customTags=new ArrayList<CustomTag>();

    public Field() {
        
    }

    public Field(String field, String type) {
        this.field = field;
        this.type = type;
    }

    public String getField() {
        return field;
    }

    public void setField(String field) {
        this.field = field;
    }


    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public ArrayList<CustomTag> getCustomTags() {
        return customTags;
    }

    public void setCustomTags(ArrayList<CustomTag> customTags) {
        this.customTags = customTags;
    }
    
}
