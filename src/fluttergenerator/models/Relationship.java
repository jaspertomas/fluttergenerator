/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fluttergenerator.models;

/**
 *
 * @author Jasper
 */
public class Relationship {
    String description;
    String table1;
    String table2;
    String field;

    public Relationship() {
        
    }
    public Relationship(String table1, String table2, String field) {
        this.table1 = table1;
        this.table2 = table2;
        this.field = field;
    }

    public void setDescription(String description) {
    }

    //objectmapper uses getters and setters to serialize objects
    public void setTable1(String table1) {
        this.table1 = table1;
    }

    public void setTable2(String table2) {
        this.table2 = table2;
    }

    public String toString() {
        return table1+" has "+table2;
    }

    public String getDescription() {
        return this.toString();
    }

    public String getTable1() {
        return table1;
    }

    public String getTable2() {
        return table2;
    }

    public String getField() {
        return field;
    }

    public void setField(String field) {
        this.field = field;
    }
    
    
}
