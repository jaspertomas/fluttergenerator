package fluttergenerator.models;

import java.util.ArrayList;

/**
 * This represents a single table in the switchboard
 */
public class TableSwitch {
    String table;
    ArrayList<String> skeletons;

    public TableSwitch() {
    }

    public TableSwitch(String table, ArrayList<String> skeletons) {
        this.table = table;
        this.skeletons = skeletons;
    }

    public String getTable() {
        return this.table;
    }

    public void setTable(String table) {
        this.table = table;
    }

    public ArrayList<String> getSkeletons() {
        return skeletons;
    }

    public void setSkeletons(ArrayList<String> skeletons) {
        this.skeletons = skeletons;
    }
}
