/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fluttergenerator.models;

import java.util.ArrayList;

/**
 *
 * Deprecated. This is just an array of strings.
 */
public class TableNames {
    ArrayList<String> tableNames = new ArrayList<String>();

    public TableNames() {
    }

    public TableNames(ArrayList<String> tableNames) {
        this.tableNames = tableNames;
    }

    public ArrayList<String> getTableNames() {
        return tableNames;
    }

    public void setTableNames(ArrayList<String> tableNames) {
        this.tableNames = tableNames;
    }

}
