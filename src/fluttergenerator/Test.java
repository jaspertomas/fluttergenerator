/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fluttergenerator;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.yaml.YAMLFactory;
import static fluttergenerator.SchemaGenerator.config_path;
import fluttergenerator.models.Field;
import fluttergenerator.models.Relationship;
import fluttergenerator.models.RelationshipList;
import fluttergenerator.models.Table;
import fluttergenerator.models.TableConfig;
import fluttergenerator.models.TableList;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Map;
import utils.JsonHelper;
import utils.YamlHelper;
import utils.fileaccess.FileReader;

/**
 *
 * @author Jasper
 */
public class Test {

    public static void main(String args[]) {
        // read mysql config file
        String yml = FileReader.read("config.yml");
        Map<String, Object> json;
        try {
            json = JsonHelper.toMap(YamlHelper.convertYamlToJson(yml));
            config_path = (String) json.get("config_path");
        } catch (IOException ex) {
            System.err.println("error with config file");
            System.err.println(ex.getMessage());
            System.err.println(ex.toString());
            System.exit(0);
        }

        try {
            // read config file for one table
            String tableName = "invoices";
            ObjectMapper om = new ObjectMapper(new YAMLFactory());
            File bfile = new File(config_path + "\\base\\" + tableName + ".yml");
            TableConfig tableConfig = om.readValue(bfile, TableConfig.class);
            File cfile = new File(config_path + "\\" + tableName + ".yml");
            TableConfig tableCustomConfig = om.readValue(cfile, TableConfig.class);

            if (tableCustomConfig.getSingular() != null)
                tableConfig.setSingular(tableCustomConfig.getSingular());
            if (tableCustomConfig.getPlural() != null)
                tableConfig.setPlural(tableCustomConfig.getPlural());
            if (tableCustomConfig.getNamefield() != null)
                tableConfig.setNamefield(tableCustomConfig.getNamefield());
            if (tableCustomConfig.getType() != null)
                tableConfig.setType(tableCustomConfig.getType());
            if (tableCustomConfig.getCode() != null)
                tableConfig.setCode(tableCustomConfig.getCode());
            if (tableCustomConfig.getCustomTags() != null)
                tableConfig.setCustomTags(tableCustomConfig.getCustomTags());
            if (tableCustomConfig.getOptions() != null)
                tableConfig.setOptions(tableCustomConfig.getOptions());
            if (tableCustomConfig.getFields() != null)
                tableConfig.setFields(tableCustomConfig.getFields());
            if (tableCustomConfig.getSuperTables() != null)
                tableConfig.setSuperTables(tableCustomConfig.getSuperTables());
            if (tableCustomConfig.getSubTables() != null)
                tableConfig.setSubTables(tableCustomConfig.getSubTables());

            System.out.println(tableConfig.getCode());

        } catch (IOException ex) {
            System.err.println("error writing mysql config to file");
            System.err.println(ex.getMessage());
            System.err.println(ex.getCause());
        }

    }
}
