/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fluttergenerator;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.yaml.YAMLFactory;
import fluttergenerator.models.*;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Map;

import utils.CaseHelper;
import utils.JsonHelper;
import utils.YamlHelper;
import utils.fileaccess.FileHelper;
import utils.fileaccess.FileReader;
import utils.fileaccess.FileWriter;

/**
 *
 * @author jaspertomas
 * 
 *         main() parses the yml and uses the other functions to generate files
 *         There are 3 main processes connected to the 3 skeleton folders
 * 
 *         HOW THE (MAIN) SKELETON FOLDER WORKS
 * 
 *         for each table specified in the yml main() uses
 *         generateFromSkeleton() to copy all the files and folders from the
 *         skeleton folder to the output folder but before writing to file, it
 *         converts all place holders in the skeleton file with variations of
 *         the table name it also converts place holders in the filename of the
 *         skeleton file with variations of the table name example:
 *         table_name=break_fast [SnakeCaseSingular]=break_fast
 *         [SnakeCasePlural]=break_fasts [CamelCaseSingular]=breakFast
 *         [CamelCasePlural]=breakFasts [CamelCaseCapsSingular]=BreakFast
 *         [CamelCaseCapsPlural]=BreakFasts [TitleCaseCapsSingular]=Break Fast
 *         [TitleCaseCapsPlural]=Break Fasts [SmallCaseCapsSingular]=break fast
 *         [SmallCaseCapsPlural]=break fasts [UpperCaseCapsSingular]=BREAKFAST
 *         [UpperCaseCapsPlural]=BREAKFASTS [CodeCase]=bre
 * 
 *         codecase is an abbreviation of the table name default code is first 3
 *         letters of the table name if a different code is desired, it can be
 *         specified in the yaml config file.
 * 
 *         HOW THE SKELETONSUBTABLES FOLDER WORKS This is for generating files
 *         when each subfolder has a copy of that set of files This is done by
 *         generateFromSkeletonSubtables()
 * 
 *         For each subtable under a table Copy all the files and folders from
 *         skeletonsubtables folder to the output folder before writing the
 *         file, convert the skeleton file's filename and contents from
 *         placeholders to variations of the subtable's name as described in the
 *         previous section (main skeleton folder)
 * 
 *         HOW THE SKELETONSNIPPETS FOLDER WORKS This one's a little different
 *         and depends on a different kind of placeholder, one that represents
 *         several lines of code as opposed to a variation of the name of a
 *         table or subtable
 * 
 *         each file in the SkeletonSnippets contains sections of code preceeded
 *         by a heading, like this
 * 
 *         ===[heading1]=== section of [CodeCase] 1 ===[heading2]=== section of
 *         [CodeCase] 2
 * 
 *         This is like a hash with keys and values, with the heading being the
 *         key and the lines of code after it being the value.
 * 
 *         Each file in the skeletonsnippets folder also has the same name as a
 *         file in the main skeleton folder. Let's call this file the main
 *         skeleton file.
 * 
 *         For each key-value pair, the main skeleton file contains a
 *         placeholder with the same name as the key, denoting the location in
 *         the file where the value will be pasted. If there are multiple
 *         subtables specified in the yaml config file, then multiple copies of
 *         the value will be pasted, each copy being slightly different
 * 
 *         Example: if the main skeleton file contains
 * 
 *         Hello [TitleCaseSingular] World Mom's [heading1] Dad's [heading2]
 * 
 *         if the skeletonsnippet file contains this:
 * 
 *         ===[heading1]=== having [subSmallCasePlural] ===[heading2]=== not
 *         having [subSmallCaseSingular]
 * 
 *         and the yaml config file contains tables: break_fasts: subtables:
 *         pan_cakes: cereals:
 * 
 *         Then the output will be
 * 
 *         Hello Break Fast World Mom's having pan cakes having cereals Dad's
 *         not having pan cake not having cereal
 */
public class BlocGenerator {
    // static String systemPath=System.getProperty("user.dir")+"\\";
    static String configPath;
    static String outputPath;
    static String skeletonPath;
    static ArrayList<String> skeletons;
    static ArrayList<String> tableNames;
    static File outputDir;
    static File skeletonDir;

    public static void main(String args[]) {

        // read mysql config file
        String yml = FileReader.read("config.yml");
        Map<String, Object> json;
        try {
            json = JsonHelper.toMap(YamlHelper.convertYamlToJson(yml));
            tableNames = (ArrayList<String>) json.get("tables_to_generate");
            configPath = (String) json.get("config_path");

            // get output path from yml
            outputPath = (String) json.get("output_path");
            // if no output path specified, use default output path
            if (outputPath == null)
                outputPath = ".\\output\\";
            // make sure there's a slash at the end
            outputPath = (outputPath + "\\").replace("\\\\", "\\");
            // create output dir if necessary
            outputDir = new File(outputPath);
            outputDir.mkdir();
            System.out.println("Output Path: " + outputDir.getCanonicalPath());

            // get skeleton path from yml
            skeletonPath = (String) json.get("skeleton_path");
            skeletons = (ArrayList<String>) json.get("skeletons");
            // if no skeleton path specified, use default skeleton path
            if (skeletonPath == null)
                skeletonPath = ".\\skeleton\\";
            // make sure there's a slash at the end
            skeletonPath = (skeletonPath + "\\").replace("\\\\", "\\");
            // create skeleton dir if necessary
            skeletonDir = new File(skeletonPath);
            System.out.println("Skeleton Path: " + skeletonDir.getCanonicalPath());

            // validations
            if (!skeletonDir.isDirectory()) {
                System.err.println("Error: Skeleton directory must be a directory");
                System.exit(0);
            }
            if (!skeletonDir.canRead()) {
                System.err.println("Error: Cannot read from skeleton directory");
                System.exit(0);
            }

        } catch (IOException ex) {
            System.err.println("error with config file");
            System.err.println(ex.getMessage());
            System.err.println(ex.toString());
            System.exit(0);
        }

        // read config file for one table

        try {
            // generate globals
            for (String skeleton : skeletons) {
                generateLevel0(skeleton);
                generateGlobalSnippets(skeleton);
            }

            ObjectMapper om = new ObjectMapper(new YAMLFactory());
            File sbfile = new File(configPath + "\\_switchboard.yml");
            TableSwitchBoard tableSwitchBoard = om.readValue(sbfile, TableSwitchBoard.class);

            for (TableSwitch tableSwitch : tableSwitchBoard.getSwitchboard()) {
                if (tableSwitch.getSkeletons() == null)
                    continue;

                String tableName = tableSwitch.getTable();
                File bfile = new File(configPath + "\\base\\" + tableName + ".yml");
                TableConfig tableConfig = om.readValue(bfile, TableConfig.class);
                {
                    File cfile = new File(configPath + "\\" + tableName + ".yml");
                    TableConfig tableCustomConfig = om.readValue(cfile, TableConfig.class);

                    if (tableCustomConfig.getSingular() != null)
                        tableConfig.setSingular(tableCustomConfig.getSingular());
                    if (tableCustomConfig.getPlural() != null)
                        tableConfig.setPlural(tableCustomConfig.getPlural());
                    if (tableCustomConfig.getNamefield() != null)
                        tableConfig.setNamefield(tableCustomConfig.getNamefield());
                    if (tableCustomConfig.getType() != null)
                        tableConfig.setType(tableCustomConfig.getType());
                    if (tableCustomConfig.getCode() != null)
                        tableConfig.setCode(tableCustomConfig.getCode());
                    if (tableCustomConfig.getCustomTags() != null)
                        tableConfig.setCustomTags(tableCustomConfig.getCustomTags());
                    if (tableCustomConfig.getOptions() != null)
                        tableConfig.setOptions(tableCustomConfig.getOptions());
                    if (tableCustomConfig.getIndexFields() != null)
                        tableConfig.setIndexFields(tableCustomConfig.getIndexFields());
                    if (tableCustomConfig.getViewFields() != null)
                        tableConfig.setViewFields(tableCustomConfig.getViewFields());
                    if (tableCustomConfig.getEditFields() != null)
                        tableConfig.setEditFields(tableCustomConfig.getEditFields());
                    if (tableCustomConfig.getFields() != null)
                        tableConfig.setFields(tableCustomConfig.getFields());
                    if (tableCustomConfig.getSuperTables() != null)
                        tableConfig.setSuperTables(tableCustomConfig.getSuperTables());
                    if (tableCustomConfig.getSubTables() != null)
                        tableConfig.setSubTables(tableCustomConfig.getSubTables());
                }

                // fields and super/subtables need to be reversed coz
                // they are generated last to first
                tableConfig.reverseLists();

                for (String skeleton : tableSwitch.getSkeletons()) {
                    // generate tables
                    generateLevel1(skeleton, GenData.tableDataToGenData(tableConfig));
                    // generate table snippets
                    generateTableSnippets(skeleton, GenData.tableDataToGenData(tableConfig));

                    // ===============SUBTABLES==============
                    if (tableConfig.getSubTables() != null)
                        // for (TableConfigDetail tableConfigDetail : tableConfig.getSubTables()) {
                        // String subtablesingular = tableConfigDetail.getSingular();
                        // validations here
                        // validate table name is plural
                        // if(subtablesingular.charAt(subtablesingular.length()-1)!='s')
                        // {
                        // System.err.println("Subtable name must be plural: "+subtablesingular);
                        // System.exit(0);
                        // }
                        // System.out.println("Subtable: " + subtablesingular);
                        // System.out.println("plural for " + subtablesingular + ": " +
                        // tableConfigDetail.getPlural());
                        // System.out.println("code for " + subtablesingular + ": " +
                        // tableConfigDetail.getCode());
                        // System.out.println("type for " + subtablesingular + ": " +
                        // tableConfigDetail.getType());
                        // System.out
                        // .println("namefield for " + subtablesingular + ": " +
                        // tableConfigDetail.getNamefield());
                        // }
                        generateLevel2Batch(skeleton, GenData.tableDataToGenData(tableConfig),
                                GenData.tableDataListToGenDataList(tableConfig.getSubTables()), "subtable");
                    // ===============END SUBTABLES==============

                    // ===============FIELDS==============
                    if (tableConfig.getFields() != null)
                        for (Field fieldData : tableConfig.getFields()) {
                            String field = fieldData.getField();
                            // validations here
                            // validate table name is plural
                            // if(subtablesingular.charAt(subtablesingular.length()-1)!='s')
                            // {
                            // System.err.println("Subtable name must be plural: "+subtablesingular);
                            // System.exit(0);
                            // }
                            // System.out.println("Field: " + field);
                            // System.out.println("type for " + field + ": " + fieldData.getType());
                        }

                    // customize gendata depending on skeleton
                    // ArrayList<Field> oldFields = tableConfig.getFields();
                    ArrayList<GenData> oldFields = GenData.fieldListToGenDataList(tableConfig.getFields());
                    ArrayList<GenData> newFields = oldFields;
                    ArrayList<String> fieldFilter;
                    switch (skeleton) {
                        case "flutteredit":
                            // filter fields: if editFields is defined in configuration,
                            // create a new field list according to editFields
                            // and use that to generate code instead of the full field list
                            fieldFilter = tableConfig.getEditFields();
                            if (fieldFilter != null) {
                                // else replace old fields with filtered down version
                                newFields = new ArrayList<GenData>();
                                for (GenData field : oldFields) {
                                    if (fieldFilter.contains(field.getName()))
                                        newFields.add(field);
                                }
                            }
                            break;
                        case "flutterview":
                            // filter fields: if viewFields is defined in configuration,
                            // create a new field list according to viewFields
                            // and use that to generate code instead of the full field list
                            fieldFilter = tableConfig.getViewFields();
                            if (fieldFilter != null) {
                                newFields = new ArrayList<GenData>();
                                for (GenData field : oldFields) {
                                    if (fieldFilter.contains(field.getName()))
                                        newFields.add(field);
                                }
                            }
                            break;
                        case "flutterindex":
                            // filter fields: if viewFields is defined in configuration,
                            // create a new field list according to viewFields
                            // and use that to generate code instead of the full field list
                            fieldFilter = tableConfig.getIndexFields();
                            if (fieldFilter != null) {
                                newFields = new ArrayList<GenData>();
                                for (GenData field : oldFields) {
                                    if (fieldFilter.contains(field.getName()))
                                        newFields.add(field);
                                }
                            }
                            break;
                    }
                    generateLevel2Batch(skeleton, GenData.tableDataToGenData(tableConfig), newFields, "field");
                    // ===============END FIELDS==============

                    // extend here
                    if (tableConfig.getSuperTables() != null)
                        generateLevel2Batch(skeleton, GenData.tableDataToGenData(tableConfig),
                                GenData.tableDataListToGenDataList(tableConfig.getSuperTables()), "supertable");
                    // end extend here
                }
            }

        } catch (IOException ex) {
            System.err.println("error generating files");
            System.err.println(ex.getMessage());
            System.err.println(ex.getCause());
        }

    }

    public static void generateLevel0(String skeleton) {
        // scan skeleton folder and put filenames into array
        ArrayList<String> filenameArray = new ArrayList<String>();
        File folder = new File(skeletonPath + "/" + skeleton + "/" + "globals/");
        try {
            FileHelper.listFiles(folder, filenameArray);
        } catch (Exception e) {
            System.err.println("Your skeleton folders seem to be missing a few subfolders");
            System.err.println(folder);
            System.exit(0);
        }
        // for each filename in skeleton folder
        for (String skeletonfilename : filenameArray) {
            // output filename is skeleton filename with output path
            // copy skeleton filename and
            // replace skeleton path with output path
            String outputfilename = skeletonfilename;
            outputfilename = outputfilename.replace(folder.getAbsolutePath(), outputDir.getAbsolutePath());

            // validation
            // check if output file contains [donotgenerate]
            // if true, do not touch this file
            File outputfile = new File(outputfilename);
            if (outputfile.isFile() && outputfile.canRead()) {
                String outputfilecontents = FileReader.read(outputfilename);
                // if file contents contains [donotgenerate] then skip this file
                if (outputfilecontents.contains("[donotgenerate]"))
                    continue;
            }

            // if filename contains a placeholder, then do not generate this file
            // coz it is missing required info
            if (outputfilename.contains("["))
                continue;

            // get contents of skeleton file
            String contents = FileReader.read(skeletonfilename);
            // write to output file
            FileWriter.write(outputfilename, contents);
        }
    }

    public static void generateLevel1(String skeleton, GenData tableData) {
        // scan skeleton folder and put filenames into array
        ArrayList<String> filenameArray = new ArrayList<String>();
        File folder = new File(skeletonPath + "/" + skeleton + "/" + "tables/");
        FileHelper.listFiles(folder, filenameArray);

        // for each filename in skeleton folder
        for (String skeletonfilename : filenameArray) {
            // replace skeleton filename with output filename
            String outputfilename = convertString(tableData, "table", skeletonfilename);
            // replace skeleton path with output path
            outputfilename = outputfilename.replace(folder.getAbsolutePath(), outputDir.getAbsolutePath());

            // validation
            // check if output file contains [donotgenerate]
            // if true, do not touch this file
            File outputfile = new File(outputfilename);
            if (outputfile.isFile() && outputfile.canRead()) {
                String outputfilecontents = FileReader.read(outputfilename);
                // if file contents contains [donotgenerate] then skip this file
                if (outputfilecontents.contains("[donotgenerate]"))
                    continue;
            }

            // if filename contains a placeholder, then do not generate this file
            // coz it is missing required info
            if (outputfilename.contains("["))
                continue;

            // get contents of skeleton file
            String contents = FileReader.read(skeletonfilename);
            // replace placeholders with keywords (variations of table name)
            contents = convertString(tableData, "table", contents);
            // write to output file
            FileWriter.write(outputfilename, contents);
        }
    }

    // level 2 means subtables, fields, etc.
    public static void generateLevel2Batch(String skeleton, GenData tableData, ArrayList<GenData> subTableDataArray,
            String prefix) {
        if (subTableDataArray == null)
            return;
        for (GenData subtableData : subTableDataArray) {
            // generate subtable files
            generateLevel2(tableData, subtableData, prefix, skeletonPath + "/" + skeleton + "/" + prefix + "s/");
            // generate subtable snippets
            generateLevel2Snippets(tableData, subtableData, prefix,
                    skeletonPath + "/" + skeleton + "/" + prefix + "snippets/");
        }
        // remove subtable snippets placeholders from output files
        for (GenData subtableData : subTableDataArray)
            removeSnippetTags(tableData, subtableData, prefix,
                    skeletonPath + "/" + skeleton + "/" + prefix + "snippets/");
    }

    public static void generateLevel2(GenData tableData, GenData subtableData, String subtableprefix, String path) {
        // scan skeleton folder and put filenames into array
        ArrayList<String> filenameArray = new ArrayList<String>();
        File folder = new File(path);
        FileHelper.listFiles(folder, filenameArray);

        // for each filename in skeleton folder
        for (String skeletonfilename : filenameArray) {
            // replace skeleton filename with output filename
            String outputfilename = skeletonfilename;
            outputfilename = convertString(tableData, "table", outputfilename);
            outputfilename = convertString(subtableData, subtableprefix, outputfilename);
            // replace skeleton path with output path
            outputfilename = outputfilename.replace(folder.getAbsolutePath(), outputDir.getAbsolutePath());

            // validation
            // check if output file contains [donotgenerate]
            // if true, do not touch this file
            File outputfile = new File(outputfilename);
            if (outputfile.isFile() && outputfile.canRead()) {
                String outputfilecontents = FileReader.read(outputfilename);
                // if file contents contains [donotgenerate] then skip this file
                if (outputfilecontents.contains("[donotgenerate]"))
                    continue;
            }

            // if filename contains a placeholder, then do not generate this file
            // coz it is missing required info
            if (outputfilename.contains("["))
                continue;

            // get contents of skeleton file
            String contents = FileReader.read(skeletonfilename);
            // replace placeholders with keywords (variations of table name)
            contents = convertString(tableData, "table", contents);
            contents = convertString(subtableData, subtableprefix, contents);
            // write to output file
            FileWriter.write(outputfilename, contents);
        }
    }

    public static void generateLevel2Snippets(GenData tableData, GenData subtableData, String subtableprefix,
            String path) {
        // scan skeleton folder and put filenames into array
        ArrayList<String> filenameArray = new ArrayList<String>();
        File folder = new File(path);
        FileHelper.listFiles(folder, filenameArray);

        // for each filename in skeleton folder
        for (String snippetsfilename : filenameArray) {
            // CREATE OUTPUT FILENAME
            String outputfilename = snippetsfilename;
            // convert output filename place holders into formatted table strings
            outputfilename = convertString(tableData, "table", outputfilename);
            outputfilename = convertString(subtableData, subtableprefix, outputfilename);
            // move output filename from skeleton path to output path
            outputfilename = outputfilename.replace(folder.getAbsolutePath(), outputDir.getAbsolutePath());
            // PROCESS SUBTABLE CONTENT
            // read skeleton snippet file
            // split it into sections
            String snippetsRaw = FileReader.read(snippetsfilename);
            String[] snippets = snippetsRaw.split("\n======\n");
            String[] keyandvalue;
            String key;
            String value;
            for (String snippet : snippets) {
                // if key value divider is missing, skip this one,
                // it's the last one which is empty
                if (!snippet.contains("\n------\n"))
                    continue;
                // if snippet divider is present, delete it,
                // it's the last divider and the newline at the end was ommitted
                if (snippet.contains("\n======"))
                    snippet = snippet.replace("\n======", "");
                keyandvalue = snippet.split("\n------\n");
                key = keyandvalue[0];
                value = keyandvalue[1];
                value = convertString(tableData, "table", value);
                value = convertString(subtableData, subtableprefix, value);

                // -----------TYPES FOR SNIPPETS----------
                value = processSnippetTypes(subtableData, value, snippet);
                // -----------END TYPES FOR SNIPPETS----------

                // validate file exists
                File file = new File(outputfilename);
                String outputfilecontents;
                if (file.canRead())
                    // read file previously generated using generateFromSkeleton
                    outputfilecontents = FileReader.read(outputfilename);
                // if file missing or unreadable, create file
                else
                    outputfilecontents = "";

                // if file contents contains [donotgenerate] then skip this file
                if (outputfilecontents.contains("[donotgenerate]"))
                    continue;

                // check if placeholder (key) is present in output file contents
                // if not, insert it at the bottom in a comment block
                // do this only if file exists (is readable)
                if (!outputfilecontents.contains(key)) {
                    String startcomment = "";
                    String endcomment = "";
                    if (!outputfilecontents.isEmpty()) {
                        String[] comments = commentsForFiletype(outputfilename);
                        startcomment = comments[0];
                        endcomment = comments[1];
                    }
                    outputfilecontents = outputfilecontents + startcomment + key + endcomment;
                }

                // if filename contains a placeholder, then do not generate this file
                // coz it is missing required info
                if (outputfilename.contains("["))
                    continue;

                // insert subtable snippets into it
                if (!value.isEmpty())
                    outputfilecontents = outputfilecontents.replace(key, key + "\n" + value);
                // and write to same file
                FileWriter.write(outputfilename, outputfilecontents);
            }
        }
    }

    public static String processSnippetTypes(GenData tableData, String value, String snippet) {
        String tableSingular = tableData.getSingular();
        // String tablePlural=tableData.getPlural();
        // String tablecode=tableData.getCode();
        String tabletype = tableData.getType();

        // System.err.println(">>>>>>>>>>>>>>>>>>>>>");
        // System.err.println(value);
        // System.err.println("<<<<<<<<<<<<<<<<<<<<<<");
        // \n---\n divides keys from values
        // \n===\n divides key value pairs from each other
        if (value.contains("\n---\n")) {
            String typekey;
            String typevalue;
            String firsttypekey = "";
            String firsttypevalue = "";
            String[] snippettypes = value.split("\n===\n");
            String[] typekeyandvalue;
            Boolean first = true;
            Boolean found = false;
            ArrayList<String> keys = new ArrayList<String>();
            for (String snippettype : snippettypes) {
                // --handle parsing issues--
                // if key value divider is missing, skip this one,
                // it's the last one which is empty
                if (!snippet.contains("\n---\n"))
                    continue;
                // if snippet divider is present, delete it,
                // it's the last divider and the newline at the end was ommitted
                if (snippet.contains("\n==="))
                    snippet = snippet.replace("\n===", "");
                // --end handle parsing issues--

                typekeyandvalue = snippettype.split("\n---\n");
                typekey = typekeyandvalue[0];
                if (typekeyandvalue.length == 1)
                    typevalue = "";
                else
                    typevalue = convertString(tableData, "table", typekeyandvalue[1]);

                keys.add(typekey);

                // if typekey == type
                // or if type is not specified and this is the first
                // take it
                if (typekey.contentEquals(tabletype) || (first && tabletype.isEmpty())) {
                    found = true;
                    value = typevalue;
                    break;
                }
                if (first) {
                    firsttypekey = typekey;
                    firsttypevalue = typevalue;
                    first = false;
                }
            }

            // given type does not match any types: error
            if (!found) {
                // System.err.println("Table " + tableSingular + ": type " + tabletype + " not
                // found, generating as type "
                // + firsttypekey + ". ");
                // System.err.println("Available types: " + keys.toString());
                value = firsttypevalue;
                // System.exit(0);
            }
        }
        return value;
    }

    public static void generateGlobalSnippets(String skeleton) {
        // scan skeleton folder and put filenames into array
        ArrayList<String> filenameArray = new ArrayList<String>();
        File folder = new File(skeletonPath + "/" + skeleton + "/" + "globalsnippets/");
        FileHelper.listFiles(folder, filenameArray);

        // for each filename in skeleton folder
        for (String snippetsfilename : filenameArray) {
            // CREATE OUTPUT FILENAME
            String outputfilename = snippetsfilename;
            // move output filename from skeleton path to output path
            outputfilename = outputfilename.replace(folder.getAbsolutePath(), outputDir.getAbsolutePath());
            // PROCESS SUBTABLE CONTENT
            // read skeleton snippet file
            // split it into sections
            String snippetsRaw = FileReader.read(snippetsfilename);
            String[] snippets = snippetsRaw.split("\n======\n");
            String[] keyandvalue;
            String key;
            String value;
            for (String snippet : snippets) {
                // if key value divider is missing, skip this one,
                // it's the last one which is empty
                if (!snippet.contains("\n------\n"))
                    continue;
                // if snippet divider is present, delete it,
                // it's the last divider and the newline at the end was ommitted
                if (snippet.contains("\n======"))
                    snippet = snippet.replace("\n======", "");
                keyandvalue = snippet.split("\n------\n");
                key = keyandvalue[0];
                // special case - if value is empty
                if (keyandvalue.length == 1)
                    value = "";
                else
                    value = keyandvalue[1];

                // -----------TYPES FOR SNIPPETS----------
                // value = processSnippetTypes(tableData, value, snippet);
                // -----------END TYPES FOR SNIPPETS----------

                // validate file exists
                File file = new File(outputfilename);
                String outputfilecontents;
                if (file.canRead())
                    // read output file as string
                    outputfilecontents = FileReader.read(outputfilename);
                // if file missing or unreadable, create file
                else
                    outputfilecontents = "";

                // if file contents contains [donotgenerate] then skip this file
                if (outputfilecontents.contains("[donotgenerate]"))
                    continue;

                // check if placeholder (key) is present in output file contents
                // if not, insert it at the bottom in a comment block
                // do this only if file exists (is readable)
                if (!outputfilecontents.contains(key)) {
                    String startcomment = "";
                    String endcomment = "";
                    if (!outputfilecontents.isEmpty()) {
                        String[] comments = commentsForFiletype(outputfilename);
                        startcomment = comments[0];
                        endcomment = comments[1];
                    }
                    outputfilecontents = outputfilecontents + startcomment + key + endcomment;
                }

                // insert table snippets into string
                // add the key to the value
                // so placeholder will not be removed
                outputfilecontents = outputfilecontents.replace(key, key + "\n" + value);
                // and write to same file
                FileWriter.write(outputfilename, outputfilecontents);
            }
        }
    }

    public static void generateTableSnippets(String skeleton, GenData tableData) {
        // scan skeleton folder and put filenames into array
        ArrayList<String> filenameArray = new ArrayList<String>();
        File folder = new File(skeletonPath + "/" + skeleton + "/" + "tablesnippets/");
        FileHelper.listFiles(folder, filenameArray);

        // for each filename in skeleton folder
        for (String snippetsfilename : filenameArray) {
            // CREATE OUTPUT FILENAME
            String outputfilename = snippetsfilename;
            // convert output filename place holders into formatted table strings
            outputfilename = convertString(tableData, "table", outputfilename);
            // move output filename from skeleton path to output path
            outputfilename = outputfilename.replace(folder.getAbsolutePath(), outputDir.getAbsolutePath());
            // PROCESS SUBTABLE CONTENT
            // read skeleton snippet file
            // split it into sections
            String snippetsRaw = FileReader.read(snippetsfilename);
            String[] snippets = snippetsRaw.split("\n======\n");
            String[] keyandvalue;
            String key;
            String value;
            for (String snippet : snippets) {
                // if key value divider is missing, skip this one,
                // it's the last one which is empty
                if (!snippet.contains("\n------\n"))
                    continue;
                // if snippet divider is present, delete it,
                // it's the last divider and the newline at the end was ommitted
                if (snippet.contains("\n======"))
                    snippet = snippet.replace("\n======", "");
                keyandvalue = snippet.split("\n------\n");
                key = keyandvalue[0];
                // special case - if value is empty
                if (keyandvalue.length == 1)
                    value = "";
                else
                    value = convertString(tableData, "table", keyandvalue[1]);

                // -----------TYPES FOR SNIPPETS----------
                value = processSnippetTypes(tableData, value, snippet);
                // -----------END TYPES FOR SNIPPETS----------

                // validate file exists
                File file = new File(outputfilename);
                String outputfilecontents;
                if (file.canRead())
                    // read output file as string
                    outputfilecontents = FileReader.read(outputfilename);
                // if file missing or unreadable, create file
                else
                    outputfilecontents = "";

                // if file contents contains [donotgenerate] then skip this file
                if (outputfilecontents.contains("[donotgenerate]"))
                    continue;

                // check if placeholder (key) is present in output file contents
                // if not, insert it at the bottom in a comment block
                // do this only if file exists (is readable)
                if (!outputfilecontents.contains(key)) {
                    String startcomment = "";
                    String endcomment = "";
                    if (!outputfilecontents.isEmpty()) {
                        String[] comments = commentsForFiletype(outputfilename);
                        startcomment = comments[0];
                        endcomment = comments[1];
                    }
                    outputfilecontents = outputfilecontents + startcomment + key + endcomment;
                }

                // if filename contains a placeholder, then do not generate this file
                // coz it is missing required info
                if (outputfilename.contains("["))
                    continue;

                // insert table snippets into string
                // add the key to the value
                // so placeholder will not be removed
                outputfilecontents = outputfilecontents.replace(key, key + "\n" + value);
                // and write to same file
                FileWriter.write(outputfilename, outputfilecontents);
            }
        }
    }

    public static void removeSnippetTags(GenData tableData, GenData subtableData, String subtableprefix, String path) {
        ArrayList<String> filenameArray = new ArrayList<String>();
        File folder = new File(path);
        FileHelper.listFiles(folder, filenameArray);

        // for each filename in skeleton folder
        for (String snippetsfilename : filenameArray) {
            // CREATE OUTPUT FILENAME
            String outputfilename = snippetsfilename;
            // convert output filename place holders into formatted table strings
            outputfilename = convertString(tableData, "table", outputfilename);
            outputfilename = convertString(subtableData, subtableprefix, outputfilename);
            // move output filename from skeleton path to output path
            outputfilename = outputfilename.replace(folder.getAbsolutePath(), outputDir.getAbsolutePath());

            // PROCESS SUBTABLE CONTENT
            // read skeleton snippet file
            // split it into sections
            String snippetsRaw = FileReader.read(snippetsfilename);
            String[] snippets = snippetsRaw.split("\n======\n");
            String[] keyandvalue;
            String key;
            for (String snippet : snippets) {
                // if key value divider is missing, skip this one,
                // it's the last one which is empty
                if (!snippet.contains("\n------\n"))
                    continue;
                // if snippet divider is present, delete it,
                // it's the last divider and the newline at the end was ommitted
                if (snippet.contains("\n======"))
                    snippet = snippet.replace("\n======", "");
                keyandvalue = snippet.split("\n------\n");
                key = keyandvalue[0];

                // validate file exists
                File file = new File(outputfilename);
                String outputfilecontents;
                if (file.canRead())
                    // read file previously generated using generateFromSkeleton
                    outputfilecontents = FileReader.read(outputfilename);
                // if file missing or unreadable, create file
                else
                    outputfilecontents = "";

                // if file contents contains [donotgenerate] then skip this file
                if (outputfilecontents.contains("[donotgenerate]"))
                    continue;

                // if filename contains a placeholder, then do not generate this file
                // coz it is missing required info
                if (outputfilename.contains("["))
                    continue;

                outputfilecontents = outputfilecontents.replace("\n" + key, "");
                FileWriter.write(outputfilename, outputfilecontents);
            }
        }
    }

    public static String[] commentsForFiletype(String outputfilename) {
        String startcomment;
        String endcomment;
        // determine file type
        String extension = outputfilename.substring(outputfilename.lastIndexOf(".") + 1);
        switch (extension) {
            case "rb":
            case "erb":
                startcomment = "\n=begin\n";
                endcomment = "\n=end\n";
                break;
            case "dart":
                startcomment = "\n/*\n";
                endcomment = "\n*/\n";
                break;
            default:
                startcomment = "\n/*\n";
                endcomment = "\n*/\n";
                break;
        }
        String[] output = new String[2];
        output[0] = startcomment;
        output[1] = endcomment;
        return output;

    }

    public static String convertString(GenData tableData, String prefix, String output) {
        String tableSingular = tableData.getSingular();
        String tablePlural = tableData.getPlural();
        String tableCode = tableData.getCode();

        String tableSnakeCaseSingular = tableSingular;
        String tableSnakeCasePlural = tablePlural;
        if (tablePlural.isEmpty())
            tableSnakeCasePlural = CaseHelper.pluralize(tableSingular);
        String tableCamelCaseSingular = CaseHelper.toCamelCase(tableSnakeCaseSingular);
        String tableCamelCasePlural = CaseHelper.toCamelCase(tableSnakeCasePlural);
        String tableCamelCaseCapsSingular = CaseHelper.toCamelCaseCaps(tableSnakeCaseSingular);
        String tableCamelCaseCapsPlural = CaseHelper.toCamelCaseCaps(tableSnakeCasePlural);
        String tableTitleCaseSingular = CaseHelper.toTitleCase(tableSnakeCaseSingular);
        String tableTitleCasePlural = CaseHelper.toTitleCase(tableSnakeCasePlural);
        String tableSmallCaseSingular = CaseHelper.toSmallCase(tableSnakeCaseSingular);
        String tableSmallCasePlural = CaseHelper.toSmallCase(tableSnakeCasePlural);
        String tableCodeCase = CaseHelper.toCodeCase(tableSnakeCaseSingular);
        // String tableCodeCaseCaps = CaseHelper.toCodeCaseCaps(tableSnakeCaseSingular);
        // custom code case
        if (tableCode.length() != 0)
            tableCodeCase = tableCode;
        String tableUpperCaseSingular = CaseHelper.toUpperCase(tableSnakeCaseSingular);
        String tableUpperCasePlural = CaseHelper.toUpperCase(tableSnakeCasePlural);

        // output=output.replace("[table]", tablePlural);

        output = output.replace("[" + prefix + "UpperCaseSingular]", tableUpperCaseSingular);
        output = output.replace("[" + prefix + "UpperCasePlural]", tableUpperCasePlural);

        output = output.replace("[" + prefix + "CamelCasePlural]", tableCamelCasePlural);
        output = output.replace("[" + prefix + "CamelCaseCapsPlural]", tableCamelCaseCapsPlural);
        output = output.replace("[" + prefix + "SnakeCasePlural]", tableSnakeCasePlural);

        output = output.replace("[" + prefix + "CamelCaseSingular]", tableCamelCaseSingular);
        output = output.replace("[" + prefix + "CamelCaseCapsSingular]", tableCamelCaseCapsSingular);
        output = output.replace("[" + prefix + "SnakeCaseSingular]", tableSnakeCaseSingular);

        output = output.replace("[" + prefix + "TitleCasePlural]", tableTitleCasePlural);
        output = output.replace("[" + prefix + "TitleCaseSingular]", tableTitleCaseSingular);
        output = output.replace("[" + prefix + "SmallCasePlural]", tableSmallCasePlural);
        output = output.replace("[" + prefix + "SmallCaseSingular]", tableSmallCaseSingular);
        output = output.replace("[" + prefix + "CodeCase]", tableCodeCase);
        // output = output.replace("[" + prefix + "CodeCaseCaps]", tableCodeCaseCaps);

        output = output.replace("[" + prefix + "NameField]", tableData.getNamefield());

        // do custom conversion tags
        if (tableData.getCustomTags() != null)
            for (CustomTag tag : tableData.getCustomTags()) {
                output = output.replace("[" + tag.getKey() + "]", tag.getValue());
            }

        return output;
    }
}
