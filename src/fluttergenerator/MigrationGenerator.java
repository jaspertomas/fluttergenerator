/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package fluttergenerator;

import java.io.IOException;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Map;
import utils.CaseHelper;
import utils.GeneratorHelper;
import utils.JsonHelper;
import utils.MySqlDBHelper;
import utils.YamlHelper;
import utils.fileaccess.FileReader;
import utils.fileaccess.FileWriter;

/**
 *
 * @author jaspertomas
 */
public class MigrationGenerator {

    static String database;
    static String hostname;
    static String username;
    static String password;
    static ArrayList<String> tables = new ArrayList<String>();

    public static void main(String args[]) {
        String yml = FileReader.read("migration_config.yml");
        Map<String, Object> json;
        try {
            json = JsonHelper.toMap(YamlHelper.convertYamlToJson(yml));
            database = (String) json.get("database");
            hostname = (String) json.get("hostname");
            username = (String) json.get("username");
            password = (String) json.get("password");
        } catch (IOException ex) {
            System.err.println("error with config file");
            System.err.println(ex.getMessage());
            System.err.println(ex.toString());
            System.exit(0);
        }

        String url = "jdbc:mysql://" + hostname + ":3306/" + database;

        MySqlDBHelper.init(url, username, password);
        Connection conn = MySqlDBHelper.getInstance().getConnection();

        Statement st = null;
        ResultSet rs = null;
        ArrayList<String> fields = new ArrayList<String>();
        ArrayList<String> fieldtypes = new ArrayList<String>();
        ArrayList<String> datatypes = new ArrayList<String>();
        String output = "";
        try {
            st = conn.createStatement();
            rs = st.executeQuery("Show Tables");

            while (rs.next()) {
                tables.add(rs.getString(1));
            }
            // tables are tables specified in config.yml
            for (String table : tables) {
                String tablePlural = CaseHelper.pluralize(table);
                // System.out.println(">>>>"+table);

                // rs = st.executeQuery("SHOW COLUMNS FROM "+tablePlural);
                rs = st.executeQuery("SHOW COLUMNS FROM " + table);
                fields.clear();
                fieldtypes.clear();
                datatypes.clear();

                output += "\nrails g model " + table + " ";

                while (rs.next()) {
                    String field = rs.getString(1);
                    // ignore id field
                    if (field.contentEquals("id"))
                        continue;
                    if (field.contentEquals("created_at"))
                        continue;
                    if (field.contentEquals("updated_at"))
                        continue;
                    String fieldtype = rs.getString(2);
                    if (CaseHelper.lastThreeLetters(field).contentEquals("_id")) {
                        field = CaseHelper.minusLastThreeLetters(field);
                        String fieldSingular = field;
                        String fieldSnakeCaseSingular = fieldSingular;
                        String fieldCamelCaseCapsSingular = CaseHelper.toCamelCaseCaps(fieldSnakeCaseSingular);
                        String fieldTitleCaseSingular = CaseHelper.toTitleCase(fieldSnakeCaseSingular);
                        String fieldCodeCase = CaseHelper.toCodeCase(fieldSnakeCaseSingular);

                        output += field + ":references ";
                    } else {
                        output += field + ":" + datatypeFor(fieldtype) + " ";
                    }

                    /*
                     * fields.add(rs.getString(1)); String fieldtype=rs.getString(2);
                     * if(fieldtype.contains("enum"))fieldtype="varchar(50)";
                     * fieldtypes.add(fieldtype); datatypes.add(datatypeFor(fieldtype));
                     */
                }
                /*
                 * System.out.println(table); System.out.println(fields);
                 * System.out.println(fieldtypes); System.out.println(datatypes);
                 */
                // FileWriter.write("./config.yml", output);
            }
            System.out.println(output);
        } catch (SQLException ex) {
            // Logger.getLogger(User.class.getName()).log(Level.SEVERE, null, ex);
            ex.printStackTrace();
        }
    }

    protected static String singularize(String table) {
        String singular = table;
        if (table.substring(table.length() - 3, table.length()).contentEquals("ies")) {
            singular = table.substring(0, table.length() - 3) + "y";
        } else if (table.substring(table.length() - 1, table.length()).contentEquals("s")) {
            singular = table.substring(0, table.length() - 1);
        }
        return singular;
    }

    public static String toCamelCase(String string) {
        String[] segments = string.split("_");
        String output = "";
        for (String s : segments)
            output += GeneratorHelper.capitalize(s);
        return output;
    }

    public static String datatypeFor(String type) {
        type = type.replaceAll("[,0-9]", "");
        // System.out.println(type);
        if (type.contentEquals("int") || type.contentEquals("int()"))
            return "integer";
        else if (type.contentEquals("varchar()"))
            return "string";
        else if (type.contentEquals("char()"))
            return "string";
        else if (type.contentEquals("text"))
            return "text";
        else if (type.contentEquals("tinytext"))
            return "string";
        else if (type.contentEquals("date"))
            return "date";
        // else if(type.contains("bigint"))
        // return "Long";
        else if (type.contains("tinyint"))
            return "boolean";
        else if (type.contains("smallint") || type.contains("mediumint"))
            return "integer";
        else if (type.contentEquals("decimal") || type.contentEquals("decimal()"))
            return "decimal";
        else if (type.contentEquals("float") || type.contentEquals("float()"))
            return "double";
        else if (type.contentEquals("double") || type.contentEquals("double()"))
            return "double";
        else if (type.contentEquals("boolean") || type.contentEquals("boolean()"))
            return "boolean";
        else if (type.contentEquals("datetime") || type.contentEquals("timestamp"))
            return "datetime";
        else if (type.contains("enum"))
            return "string";
        else
            return "";
        /*
         * <option value="INT" selected="selected">INT</option> <option
         * value="VARCHAR">VARCHAR</option> <option value="TEXT">TEXT</option> <option
         * value="DATE">DATE</option> <optgroup label="NUMERIC"><option
         * value="TINYINT">TINYINT</option> <option value="SMALLINT">SMALLINT</option>
         * <option value="MEDIUMINT">MEDIUMINT</option> <option value="INT"
         * selected="selected">INT</option> <option value="BIGINT">BIGINT</option>
         * <option value="-">-</option> <option value="DECIMAL">DECIMAL</option> <option
         * value="FLOAT">FLOAT</option> <option value="DOUBLE">DOUBLE</option> <option
         * value="REAL">REAL</option> <option value="-">-</option> <option
         * value="BIT">BIT</option> <option value="BOOLEAN">BOOLEAN</option> <option
         * value="SERIAL">SERIAL</option> </optgroup><optgroup
         * label="DATE and TIME"><option value="DATE">DATE</option> <option
         * value="DATETIME">DATETIME</option> <option
         * value="TIMESTAMP">TIMESTAMP</option> <option value="TIME">TIME</option>
         * <option value="YEAR">YEAR</option> </optgroup><optgroup
         * label="STRING"><option value="CHAR">CHAR</option> <option
         * value="VARCHAR">VARCHAR</option> <option value="-">-</option> <option
         * value="TINYTEXT">TINYTEXT</option> <option value="TEXT">TEXT</option> <option
         * value="MEDIUMTEXT">MEDIUMTEXT</option> <option
         * value="LONGTEXT">LONGTEXT</option> <option value="-">-</option> <option
         * value="BINARY">BINARY</option> <option value="VARBINARY">VARBINARY</option>
         * <option value="-">-</option> <option value="TINYBLOB">TINYBLOB</option>
         * <option value="MEDIUMBLOB">MEDIUMBLOB</option> <option
         * value="BLOB">BLOB</option> <option value="LONGBLOB">LONGBLOB</option> <option
         * value="-">-</option> <option value="ENUM">ENUM</option> <option
         * value="SET">SET</option> </optgroup><optgroup label="SPATIAL"><option
         * value="GEOMETRY">GEOMETRY</option> <option value="POINT">POINT</option>
         * <option value="LINESTRING">LINESTRING</option> <option
         * value="POLYGON">POLYGON</option> <option
         * value="MULTIPOINT">MULTIPOINT</option> <option
         * value="MULTILINESTRING">MULTILINESTRING</option> <option
         * value="MULTIPOLYGON">MULTIPOLYGON</option> <option
         * value="GEOMETRYCOLLECTION">GEOMETRYCOLLECTION</option> </optgroup>
         */
    }

    /*
     * public static String rsGetterFor(String type, String field) {
     * type=type.replaceAll("[,0-9]", "");
     * if(type.contentEquals("int")||type.contentEquals("int()")) return
     * "c.getInt"+"(c.getColumnIndex(\""+field+"\"));"; else
     * if(type.contains("varchar()")) return
     * "c.getString"+"(c.getColumnIndex(\""+field+"\"));"; else
     * if(type.contentEquals("char()")) return
     * "c.getString"+"(c.getColumnIndex(\""+field+"\"));"; else
     * if(type.contentEquals("text")) return
     * "c.getString"+"(c.getColumnIndex(\""+field+"\"));"; else
     * if(type.contentEquals("tinytext")) return
     * "c.getString"+"(c.getColumnIndex(\""+field+"\"));"; else
     * if(type.contentEquals("date")) return
     * "StandardDateHelper.toDate(c.getString"+"(c.getColumnIndex(\""+field+
     * "\")));"; else if(type.contains("bigint")) return
     * "c.getLong"+"(c.getColumnIndex(\""+field+"\"));"; else
     * if(type.contains("tinyint") || type.contains("smallint") ||
     * type.contains("mediumint")) return
     * "c.getInt"+"(c.getColumnIndex(\""+field+"\"));"; else
     * if(type.contentEquals("decimal")||type.contentEquals("decimal()")) return
     * "c.getBigDecimal"+"(c.getColumnIndex(\""+field+"\"));"; else
     * if(type.contentEquals("float")||type.contentEquals("float()")) return
     * "c.getFloat"+"(c.getColumnIndex(\""+field+"\"));"; else
     * if(type.contentEquals("double")||type.contentEquals("double()")) return
     * "c.getDouble"+"(c.getColumnIndex(\""+field+"\"));"; else
     * if(type.contentEquals("boolean")||type.contentEquals("boolean()")) return
     * "c.getBoolean"+"(c.getColumnIndex(\""+field+"\"));"; else
     * if(type.contentEquals("datetime")||type.contentEquals("timestamp")) return
     * "c.getTimestamp"+"(c.getColumnIndex(\""+field+"\"));"; else
     * if(type.contains("enum")) return
     * "c.getString"+"(c.getColumnIndex(\""+field+"\"));"; else return ""; }
     */
    public static String stringifier(String type) {
        type = type.replaceAll("[,0-9]", "");
        if (type.contentEquals("int") || type.contentEquals("int()"))
            return ".toString()";
        else if (type.contains("varchar()"))
            return "";
        else if (type.contentEquals("char()"))
            return "";
        else if (type.contentEquals("text"))
            return "";
        else if (type.contentEquals("tinytext"))
            return "";
        else if (type.contentEquals("date"))
            return ".toString()";
        else if (type.contains("bigint"))
            return ".toString()";
        else if (type.contains("tinyint") || type.contains("smallint") || type.contains("mediumint"))
            return ".toString()";
        else if (type.contentEquals("decimal") || type.contentEquals("decimal()"))
            return ".toString()";
        else if (type.contentEquals("float") || type.contentEquals("float()"))
            return ".toString()";
        else if (type.contentEquals("double") || type.contentEquals("double()"))
            return ".toString()";
        else if (type.contentEquals("boolean") || type.contentEquals("boolean()"))
            return ".toString()";
        else if (type.contentEquals("datetime") || type.contentEquals("timestamp"))
            return ".toString()";
        else if (type.contains("enum"))
            return "";
        else
            return "";
    }

    public static String stringifiedWithNull(String field, String type) {
        type = type.replaceAll("[,0-9]", "");
        if (type.contentEquals("int") || type.contentEquals("int()"))
            return field + "!=null?" + field + ".toString():null";
        else if (type.contains("varchar()"))
            return field;
        else if (type.contentEquals("char()"))
            return field;
        else if (type.contentEquals("text"))
            return field;
        else if (type.contentEquals("tinytext"))
            return field;
        else if (type.contentEquals("date"))
            return field + "!=null?" + field + ".toString():null";
        else if (type.contains("bigint"))
            return field + "!=null?" + field + ".toString():null";
        else if (type.contains("tinyint") || type.contains("smallint") || type.contains("mediumint"))
            return field + "!=null?" + field + ".toString():null";
        else if (type.contentEquals("decimal") || type.contentEquals("decimal()"))
            return field + "!=null?" + field + ".toString():null";
        else if (type.contentEquals("float") || type.contentEquals("float()"))
            return field + "!=null?" + field + ".toString():null";
        else if (type.contentEquals("double") || type.contentEquals("double()"))
            return field + "!=null?" + field + ".toString():null";
        else if (type.contentEquals("boolean") || type.contentEquals("boolean()"))
            return field + "!=null?" + field + ".toString():null";
        else if (type.contentEquals("datetime") || type.contentEquals("timestamp"))
            return field + "!=null?" + field + ".toString():null";
        else if (type.contains("enum"))
            return field;
        else
            return field;
    }

}
