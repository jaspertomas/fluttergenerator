/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fluttergenerator;

import java.io.IOException;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import utils.JsonHelper;
import utils.YamlHelper;
import utils.fileaccess.FileReader;

/**
 *
 * @author Jasper
 */
public class YamlReaderDemo {
    public static void main(String args[])
    {
        try {
            String yml=FileReader.read("yaml_sample.txt");
            String jsonString = YamlHelper.convertYamlToJson(yml);
            Map<String,Object> json=JsonHelper.toMap(jsonString);
            LinkedHashMap languages = (LinkedHashMap)json.get("languages");
            System.out.println(languages.get("ELK"));
            ArrayList domains = (ArrayList)json.get("domains");
            System.out.println(domains.get(2));
        } catch (IOException ex) {
            Logger.getLogger(YamlReaderDemo.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
