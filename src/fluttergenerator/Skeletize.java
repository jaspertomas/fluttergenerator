/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fluttergenerator;

import fluttergenerator.models.CustomTag;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import utils.CaseHelper;
import utils.JsonHelper;
import utils.YamlHelper;
import utils.fileaccess.FileHelper;
import utils.fileaccess.FileReader;
import utils.fileaccess.FileWriter;

/**
 *
 * @author Jasper This file converts a file provided by you and scans it for
 *         variations of the table name you specify example:
 *         table_name=break_fast [SnakeCaseSingular]=break_fast
 *         [SnakeCasePlural]=break_fasts [CamelCaseSingular]=breakFast
 *         [CamelCasePlural]=breakFasts [CamelCaseCapsSingular]=BreakFast
 *         [CamelCaseCapsPlural]=BreakFasts [TitleCaseCapsSingular]=Break Fast
 *         [TitleCaseCapsPlural]=Break Fasts [SmallCaseCapsSingular]=break fast
 *         [SmallCaseCapsPlural]=break fasts [UpperCaseCapsSingular]=BREAKFAST
 *         [UpperCaseCapsPlural]=BREAKFASTS [CodeCase]=bre then replaces words
 *         in the code with placeholders like [SnakeCasePlural] or
 *         [CamelCaseCapsSingular] converting it into a skeleton file for future
 *         use Todo: make it yaml driven
 */
public class Skeletize {
    static String inputPath;
    static String outputPath;
    static File inputDir;
    static File outputDir;

    public static void main(String args[]) {
        try {
            String yml = FileReader.read("skeletize_config.yml");
            System.out.println(yml);
            Map<String, Object> json = JsonHelper.toMap(YamlHelper.convertYamlToJson(yml));

            // get input path from yml
            inputPath = (String) json.get("input_path");
            // if no input path specified, use default input path
            if (inputPath == null)
                inputPath = ".\\skeletize_input\\";
            // make sure there's a slash at the end
            inputPath = (inputPath + "\\").replace("\\\\", "\\");
            // create input dir if necessary
            inputDir = new File(inputPath);
            System.out.println("Skeletize Input Path: " + inputPath);

            // get skeleton path from yml
            outputPath = (String) json.get("output_path");
            // if no skeleton path specified, use default skeleton path
            if (outputPath == null)
                outputPath = ".\\skeletize_output\\";
            // make sure there's a slash at the end
            outputPath = (outputPath + "\\").replace("\\\\", "\\");
            // create skeleton dir if necessary
            outputDir = new File(outputPath);
            outputDir.mkdir();
            System.out.println("Skeletize Output Path: " + outputPath);

            // validations
            if (!inputDir.isDirectory()) {
                System.err.println("Error: Skeletize input directory must be a directory");
                System.exit(0);
            }
            if (!inputDir.canRead()) {
                System.err.println("Error: Cannot read from skeletize input directory");
                System.exit(0);
            }

            // convert yml config into data hash
            LinkedHashMap tables = (LinkedHashMap) json.get("tables");
            ArrayList<GenData> tableDataArray = GenData.getArrayFromMap(tables);

            // validate and output configuration
            for (GenData tableData : tableDataArray) {
                String table = tableData.getSingular();
                System.out.println("Table: " + table);

                // // do validations here
                // if(tablesingular.charAt(tablesingular.length()-1)!='s')
                // {
                // System.err.println("Table name must be plural: "+tablesingular);
                // System.exit(0);
                // }

                System.out.println("plural for " + table + ": " + tableData.getPlural());
                // System.out.println("code for " + table + ": " + tableData.getCode());
                // System.out.println("namefield for " + table + ": " +
                // tableData.getNamefield());
                // System.out.println("type for " + table + ": " + tableData.getType());

                // ===============SUBTABLES==============
                if (tableData.getSubtables() != null)
                    for (GenData subtableData : tableData.getSubtables()) {
                        String subtablesingular = subtableData.getSingular();

                        // validations here

                        System.out.println("Subtable: " + subtablesingular);
                        System.out.println("plural for " + subtablesingular + ": " + subtableData.getPlural());
                        // System.out.println("code for " + subtablesingular + ": " +
                        // subtableData.getCode());
                        // System.out.println("type for " + subtablesingular + ": " +
                        // subtableData.getType());
                        // System.out.println("namefield for " + subtablesingular + ": " +
                        // subtableData.getNamefield());
                    }
                // ===============END SUBTABLES==============

                // ===============FIELDS==============
                if (tableData.getFields() != null)
                    for (GenData fieldData : tableData.getFields()) {
                        String field = fieldData.getSingular();

                        // validations here

                        System.out.println("Field: " + field);
                    }
                // ===============END FIELDS==============

                // extend here
                if (tableData.getSupertables() != null)
                    for (GenData supertableData : tableData.getSupertables()) {
                        String field = supertableData.getSingular();

                        // validations here

                        System.out.println("Field: " + field);
                    }
                // end extend here
            }

            // ----------------------
            // create an array of all files in input folder
            // scan skeleton folder and put filenames into array
            ArrayList<String> filenameArray = new ArrayList<String>();
            FileHelper.listFiles(inputDir, filenameArray);

            // for each filename in array
            // 1. convert filename to output filename
            // 2. convert contents
            // 3. write to output file
            for (String inputfilename : filenameArray) {
                // 1. convert filename to output filename
                String outputfilename = inputfilename;
                // replace input path with output path
                outputfilename = outputfilename.replace(inputDir.getAbsolutePath(), outputDir.getAbsolutePath());
                // more filename conversions later

                // get contents of input file
                String contents = FileReader.read(inputfilename);

                // for each table in yml config
                // replace key words (variations of table name) with placeholders
                for (GenData tableData : tableDataArray) {
                    // do validations here

                    // 1. convert filename to output filename
                    // convert input filename into output filename
                    // by replacing instances of table names with placeholders
                    outputfilename = convertString(tableData, "table", outputfilename);

                    // 2. convert contents
                    contents = convertString(tableData, "table", contents);

                    // ===============SUBTABLES==============
                    if (tableData.getSubtables() != null)
                        for (GenData subtableData : tableData.getSubtables()) {
                            // validations here

                            // 1. convert filename to output filename
                            outputfilename = convertString(subtableData, "subtable", outputfilename);

                            contents = convertString(subtableData, "subtable", contents);
                        }
                    // ===============END SUBTABLES==============

                    // ===============FIELDS==============
                    if (tableData.getFields() != null)
                        for (GenData fieldData : tableData.getFields()) {
                            // validations here

                            // 1. convert filename to output filename
                            outputfilename = convertString(fieldData, "field", outputfilename);

                            contents = convertString(fieldData, "field", contents);
                        }
                    // ===============END FIELDS==============

                    // extend here
                    if (tableData.getSupertables() != null)
                        for (GenData supertableData : tableData.getSupertables()) {
                            // validations here

                            outputfilename = convertString(supertableData, "supertable", outputfilename);

                            contents = convertString(supertableData, "supertable", contents);
                        }
                    // end extend here
                }

                // 3. write to output file
                System.out.println(outputfilename);
                FileWriter.write(outputfilename, contents);
            }
            // ----------------------
        } catch (IOException ex) {
            Logger.getLogger(YamlReaderDemo.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    public static String convertString(GenData tableData, String prefix, String output) {
        String tableSingular = tableData.getSingular();
        String tablePlural = tableData.getPlural();

        String tableSnakeCaseSingular = tableSingular;
        String tableSnakeCasePlural = tablePlural;
        if (tablePlural.isEmpty())
            tableSnakeCasePlural = CaseHelper.pluralize(tableSingular);
        String tableCamelCaseSingular = CaseHelper.toCamelCase(tableSnakeCaseSingular);
        String tableCamelCasePlural = CaseHelper.toCamelCase(tableSnakeCasePlural);
        String tableCamelCaseCapsSingular = CaseHelper.toCamelCaseCaps(tableSnakeCaseSingular);
        String tableCamelCaseCapsPlural = CaseHelper.toCamelCaseCaps(tableSnakeCasePlural);
        String tableTitleCaseSingular = CaseHelper.toTitleCase(tableSnakeCaseSingular);
        String tableTitleCasePlural = CaseHelper.toTitleCase(tableSnakeCasePlural);
        String tableSmallCaseSingular = CaseHelper.toSmallCase(tableSnakeCaseSingular);
        String tableSmallCasePlural = CaseHelper.toSmallCase(tableSnakeCasePlural);
        String tableUpperCaseSingular = CaseHelper.toUpperCase(tableSnakeCaseSingular);
        String tableUpperCasePlural = CaseHelper.toUpperCase(tableSnakeCasePlural);

        // output=output.replace(tableSingular, "[table]");

        output = output.replace(tableUpperCaseSingular, "[" + prefix + "UpperCaseSingular]");
        output = output.replace(tableUpperCasePlural, "[" + prefix + "UpperCasePlural]");

        output = output.replace(tableCamelCasePlural, "[" + prefix + "CamelCasePlural]");
        output = output.replace(tableCamelCaseCapsPlural, "[" + prefix + "CamelCaseCapsPlural]");
        output = output.replace(tableSnakeCasePlural, "[" + prefix + "SnakeCasePlural]");

        output = output.replace(tableCamelCaseSingular, "[" + prefix + "CamelCaseSingular]");
        output = output.replace(tableCamelCaseCapsSingular, "[" + prefix + "CamelCaseCapsSingular]");
        output = output.replace(tableSnakeCaseSingular, "[" + prefix + "SnakeCaseSingular]");

        output = output.replace(tableTitleCasePlural, "[" + prefix + "TitleCasePlural]");
        output = output.replace(tableTitleCaseSingular, "[" + prefix + "TitleCaseSingular]");
        output = output.replace(tableSmallCasePlural, "[" + prefix + "SmallCasePlural]");
        output = output.replace(tableSmallCaseSingular, "[" + prefix + "SmallCaseSingular]");

        /*
         * // code case no longer has a default value // if it is not defined, no
         * conversion will take place String tableCode = tableData.getCode(); //convert
         * only if tableCode is not empty if (tableCode.length() != 0) { output =
         * output.replace(tableCode, "[" + prefix + "CodeCase]"); }
         */

        // output = output.replace(tableData, "[" + prefix +
        // "NameField]".getNamefield());

        // do custom conversion tags
        ArrayList<CustomTag> customTags = tableData.getCustomTags();
        if (customTags != null)
            for (CustomTag tag : customTags) {
                output = output.replace(tag.getValue(), "[" + tag.getKey() + "]");
            }

        return output;
    }
}
