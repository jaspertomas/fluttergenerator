/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fluttergenerator;

import fluttergenerator.models.*;
import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedHashMap;
import utils.CaseHelper;

/**
 *
 * @author Jasper
 */
public class GenData {
    String name;
    String plural;
    String namefield;
    String type;
    String code;
    ArrayList<CustomTag> customTags;
    ArrayList<GenData> subtables;
    ArrayList<GenData> fields;
    // extend here
    ArrayList<GenData> supertables;
    // end extend here

    // this is a recursive function that traverses the yaml tree
    // but only for 2 levels.
    // there are 2 levels: tableLevel and subtableLevel
    // if tableLevel=false, further sublevels will not be processed
    public static GenData getFromMap(String key, LinkedHashMap map, Boolean tableLevel) {
        String singular = key;
        String plural = "";
        String code = "";
        String type = "";
        String namefield = "";
        GenData tableData;

        if (map == null) {
            tableData = new GenData(singular, plural, code, type, namefield);
        } else {
            // System.out.println("Table: "+tablesingular);
            // validate table name is plural
            // if(tablesingular.charAt(tablesingular.length()-1)!='s')
            // {
            // System.err.println("Table name must be plural: "+tablesingular);
            // System.exit(0);
            // }

            plural = (String) map.get("plural");
            if (plural == null)
                plural = CaseHelper.pluralize(singular);
            // System.out.println("plural for "+singular+": "+plural);

            code = (String) map.get("code");
            if (code == null) {
                code = CaseHelper.toCodeCase(singular);
            }
            // System.out.println("code for "+singular+": "+code);

            type = (String) map.get("type");
            if (type == null)
                type = "";
            // System.out.println("type for "+singular+": "+type);

            namefield = (String) map.get("namefield");
            if (namefield == null)
                namefield = "name";
            // System.out.println("namefield for "+singular+": "+namefield);

            tableData = new GenData(singular, plural, code, type, namefield);
            tableData.setCustomTags((ArrayList<CustomTag>) map.get("custom_tags"));

            // if at subtable level , do not process further sublevels
            if (!tableLevel)
                return tableData;

            tableData.setSubtables(getSubtableDataArrayFromMap((LinkedHashMap) map.get("subtables")));
            tableData.setFields(getSubtableDataArrayFromMap((LinkedHashMap) map.get("fields")));
            // extend here
            tableData.setSupertables(getSubtableDataArrayFromMap((LinkedHashMap) map.get("supertables")));
            // end extend here
        }
        return tableData;
    }

    public static ArrayList<GenData> getSubtableDataArrayFromMap(LinkedHashMap<String, LinkedHashMap> subtablesMap) {
        // subtablesMap is a hashmap
        // each key is the name of subtable
        // each value is a yaml tree that will be converted into a TableData
        //
        // return value is an array with structure
        // [TableData,TableData,TableData]

        ArrayList<GenData> subtableDataArray = new ArrayList<GenData>();
        // if yaml tree does not exist, return empty array
        if (subtablesMap == null)
            return subtableDataArray;

        // each subtableKey is the name of a subtable
        for (String subtableKey : subtablesMap.keySet()) {
            // use the key to get the value
            LinkedHashMap subtableMap = (LinkedHashMap) subtablesMap.get(subtableKey);
            // convert the key and value into a TableData
            GenData subtableData = getFromMap(subtableKey, subtableMap, false);
            // save the TableData into an array
            subtableDataArray.add(subtableData);
        }
        // return the array
        return subtableDataArray;
    }

    public static ArrayList<GenData> getArrayFromMap(LinkedHashMap<String, LinkedHashMap> tables) {
        ArrayList<GenData> tableDataArray = new ArrayList<GenData>();

        for (String key : tables.keySet()) {
            LinkedHashMap tableMap = (LinkedHashMap) tables.get(key);
            GenData tableData = getFromMap(key, tableMap, true);
            tableDataArray.add(tableData);
        }

        return tableDataArray;
    }

    public GenData(String name, String plural, String code, String type, String namefield) {
        this.name = name;
        this.plural = plural;
        this.namefield = namefield;
        this.type = type;
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public String getSingular() {
        return name;
    }

    public String getPlural() {
        return plural;
    }

    public String getCode() {
        return code;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getNamefield() {
        return namefield;
    }

    public void setNamefield(String namefield) {
        this.namefield = namefield;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public ArrayList<GenData> getSubtables() {
        return subtables;
    }

    public void setSubtables(ArrayList<GenData> subtables) {
        Collections.reverse(subtables);
        this.subtables = subtables;
    }
    // public void addSubtable(TableData subtable) {
    // this.subtables.add(subtable);
    // }

    // extend here
    public ArrayList<GenData> getSupertables() {
        return supertables;
    }

    public void setSupertables(ArrayList<GenData> supertables) {
        Collections.reverse(supertables);
        this.supertables = supertables;
    }
    // end extend here

    public ArrayList<GenData> getFields() {
        return fields;
    }

    // public void addField(TableData field) {
    // this.fields.add(field);
    // }

    public void setFields(ArrayList<GenData> fields) {
        Collections.reverse(fields);
        this.fields = fields;
    }

    public ArrayList<CustomTag> getCustomTags() {
        return customTags;
    }

    public void setCustomTags(ArrayList<CustomTag> customTags) {
        this.customTags = customTags;
    }

    public static GenData tableDataToGenData(TableConfig tableConfig) {
        GenData genData = new GenData(tableConfig.getSingular(), tableConfig.getPlural(), tableConfig.getCode(),
                tableConfig.getType(), tableConfig.getNamefield());
        genData.customTags = tableConfig.getCustomTags();
        return genData;
    }

    public static GenData tableDataToGenData(TableConfigDetail tableConfigDetail) {
        GenData genData = new GenData(tableConfigDetail.getSingular(), tableConfigDetail.getPlural(),
                tableConfigDetail.getCode(), tableConfigDetail.getType(), tableConfigDetail.getNamefield());
        genData.customTags = tableConfigDetail.getCustomTags();
        return genData;
    }

    public static GenData tableDataToGenData(Field field) {
        GenData genData = new GenData(field.getField(), "", "", field.getType(), "");
        genData.customTags = field.getCustomTags();
        return genData;
    }

    // this is for converting lists of fields into GenData format
    public static ArrayList<GenData> fieldListToGenDataList(ArrayList<Field> fieldList) {
        ArrayList<GenData> genDataList = new ArrayList<GenData>();
        if (fieldList != null)
            for (Field data : fieldList) {
                genDataList.add(tableDataToGenData(data));
            }
        return genDataList;
    }

    public static ArrayList<GenData> tableDataListToGenDataList(ArrayList<TableConfigDetail> tableConfigDetailList) {
        ArrayList<GenData> genDataList = new ArrayList<GenData>();
        if (tableConfigDetailList != null)
            for (TableConfigDetail data : tableConfigDetailList) {
                genDataList.add(tableDataToGenData(data));
            }
        return genDataList;
    }

}
