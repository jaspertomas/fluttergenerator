/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package fluttergenerator;

import fluttergenerator.models.*;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.yaml.YAMLFactory;
import java.io.File;
import java.io.IOException;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Map;
import utils.CaseHelper;
import utils.GeneratorHelper;
import utils.JsonHelper;
import utils.MySqlDBHelper;
import utils.YamlHelper;
import utils.fileaccess.FileReader;

/**
 *
 * @author jaspertomas
 */
public class SchemaGenerator {

    static String config_path;
    static String database;
    static String hostname;
    static String username;
    static String password;
    static ArrayList<String> skeletons;
    static ArrayList<String> tableNames = new ArrayList<String>();
    static ArrayList<Table> tables = new ArrayList<Table>();
    static ArrayList<Relationship> relationships = new ArrayList<Relationship>();
    static ArrayList<Field> fields;
    static TableSwitchBoard tableSwitchBoard = new TableSwitchBoard();

    public static void main(String args[]) {
        // read mysql config file
        String yml = FileReader.read("config.yml");
        Map<String, Object> json;
        try {
            json = JsonHelper.toMap(YamlHelper.convertYamlToJson(yml));
            config_path = (String) json.get("config_path");
            skeletons = (ArrayList<String>) json.get("skeletons");
            database = (String) json.get("database");
            hostname = (String) json.get("hostname");
            username = (String) json.get("username");
            password = (String) json.get("password");
            // tables = (ArrayList<String>) json.get("tables");
            // for(String s:tables){System.out.println(s);}
        } catch (IOException ex) {
            System.err.println("error with config file");
            System.err.println(ex.getMessage());
            System.err.println(ex.toString());
            System.exit(0);
        }

        // connect to database
        // according to config file
        String url = "jdbc:mysql://" + hostname + ":3306/" + database;
        MySqlDBHelper.init(url, username, password);
        Connection conn = MySqlDBHelper.getInstance().getConnection();

        // initialize variables
        Statement st = null;
        ResultSet rs = null;

        // start building string to be written to file
        /*
         * String output="output_path: D:\\src\\tmcprogram5\n" +
         * "# output_path: .\\output\n" + "# skeleton_path: .\\skeleton\\Symfony\n" +
         * "skeleton_path: .\\skeleton\\FlutterStacked\n" +
         * "# skeleton_path: .\\skeleton\\Rails\n" + "tables:\n" ;
         */

        try {
            st = conn.createStatement();

            // uncomment to scan all tables in database
            tables.clear();
            rs = st.executeQuery("Show Tables");

            // add all table names to tables array
            while (rs.next()) {
                String tableName = rs.getString(1);
                if (tableName.contentEquals("active_storage_attachments"))
                    continue;
                if (tableName.contentEquals("active_storage_blobs"))
                    continue;
                if (tableName.contentEquals("ar_internal_metadata"))
                    continue;
                if (tableName.contentEquals("schema_migrations"))
                    continue;
                if (tableName.contentEquals("histories"))
                    continue;
                if (tableName.contentEquals("images"))
                    continue;
                tableNames.add(tableName);
            }

            // tables are tables specified in config.yml
            // unless scanned using previous code
            // for each table in tables array
            for (String tableName : tableNames) {
                // String tablePlural=CaseHelper.pluralize(table);
                // System.out.println(">>>>"+table);

                // extract foreign key info about this table
                // from the database
                // table must use innodb
                String fkQuery = "SELECT TABLE_NAME,COLUMN_NAME,CONSTRAINT_NAME, REFERENCED_TABLE_NAME,REFERENCED_COLUMN_NAME\n"
                        + "FROM INFORMATION_SCHEMA.KEY_COLUMN_USAGE\n" + "WHERE REFERENCED_TABLE_SCHEMA = '" + database
                        + "' AND TABLE_NAME = '" + tableName + "';";
                rs = st.executeQuery(fkQuery);
                while (rs.next()) {
                    /*
                     * System.out.println("==============");
                     * System.out.println(rs.getString(1));//purchases
                     * System.out.println(rs.getString(2));//supplier_id
                     * System.out.println(rs.getString(3));//fk_rails_fd0b101ea2
                     * System.out.println(rs.getString(4));//suppliers
                     * System.out.println(rs.getString(5));//id
                     */
                    // this is a foreign key relationship
                    // 2 means has_many
                    // 1 means has_one
                    relationships.add(new Relationship(rs.getString(4), rs.getString(1), rs.getString(2)));
                }

                // rs = st.executeQuery("SHOW COLUMNS FROM "+tablePlural);
                rs = st.executeQuery("SHOW COLUMNS FROM " + tableName);
                fields = new ArrayList<Field>();
                // fieldtypes.clear();
                // datatypes.clear();

                // nameField is the first string field
                String nameField = "";
                while (rs.next()) {
                    String fieldName = rs.getString(1);
                    // ignore id field
                    if (fieldName.contentEquals("id"))
                        continue;
                    if (fieldName.contentEquals("created_at"))
                        continue;
                    if (fieldName.contentEquals("updated_at"))
                        continue;

                    String fieldType = rs.getString(2);
                    if (nameField.isEmpty() && datatypeFor(fieldType).contentEquals("string"))
                        nameField = fieldName;
                    fields.add(new Field(fieldName, datatypeFor(fieldType)));
                }
                /*
                 * System.out.println(table); System.out.println(fields);
                 * System.out.println(fieldtypes); System.out.println(datatypes);
                 */
                tables.add(new Table(tableName, nameField, fields));

                // add switchboard to switchboards
                tableSwitchBoard.getSwitchboard().add(new TableSwitch(tableName, skeletons));
            }

            try {
                File folder = new File(config_path + "/base");
                folder.mkdirs();

                ObjectMapper om = new ObjectMapper(new YAMLFactory());
                RelationshipList relationshipList = new RelationshipList(relationships);
                TableList tableList = new TableList(tables);

                om.writeValue(new File(config_path + "/base/_relationships.yml"), relationshipList);
                om.writeValue(new File(config_path + "/base/_tables.yml"), tableList);
                // om.writeValue(new File(config_path + "/_tables.yml"), new
                // TableNames(tableNames));

                // write switchboard file
                // if switchboard exists, do not overwrite
                File switchboardFile = new File(config_path + "/_switchboard.yml");
                if (!switchboardFile.exists())
                    om.writeValue(switchboardFile, tableSwitchBoard);

                for (Table table : tables) {
                    genConfigFile(table, tableList, relationshipList);
                }

            } catch (IOException ex) {
                System.err.println("error writing schema files");
                System.err.println(ex.getMessage());
                System.err.println(ex.getCause());
            }
        } catch (SQLException ex) {
            // Logger.getLogger(User.class.getName()).log(Level.SEVERE, null, ex);
            ex.printStackTrace();
        }
    }

    protected static String singularize(String table) {
        String singular = table;
        if (table.substring(table.length() - 3, table.length()).contentEquals("ies")) {
            singular = table.substring(0, table.length() - 3) + "y";
        } else if (table.substring(table.length() - 1, table.length()).contentEquals("s")) {
            singular = table.substring(0, table.length() - 1);
        }
        return singular;
    }

    public static String toCamelCase(String string) {
        String[] segments = string.split("_");
        String output = "";
        for (String s : segments)
            output += GeneratorHelper.capitalize(s);
        return output;
    }

    public static String datatypeFor(String type) {
        type = type.replaceAll("[,0-9]", "");
        // System.out.println(type);
        if (type.contentEquals("int") || type.contentEquals("int()"))
            return "integer";
        else if (type.contentEquals("bigint()"))
            return "bigint";
        else if (type.contentEquals("varchar()"))
            return "string";
        else if (type.contentEquals("char()"))
            return "string";
        else if (type.contentEquals("text"))
            return "text";
        else if (type.contentEquals("tinytext"))
            return "string";
        else if (type.contentEquals("date"))
            return "date";
        // else if(type.contains("bigint"))
        // return "Long";
        else if (type.contains("tinyint"))
            return "boolean";
        else if (type.contains("smallint") || type.contains("mediumint"))
            return "integer";
        else if (type.contentEquals("decimal") || type.contentEquals("decimal()"))
            return "decimal";
        else if (type.contentEquals("float") || type.contentEquals("float()"))
            return "double";
        else if (type.contentEquals("double") || type.contentEquals("double()"))
            return "double";
        else if (type.contentEquals("boolean") || type.contentEquals("boolean()"))
            return "boolean";
        else if (type.contentEquals("datetime") || type.contentEquals("timestamp"))
            return "datetime";
        else if (type.contains("enum"))
            return "string";
        else
            return "";
        /*
         * <option value="INT" selected="selected">INT</option> <option
         * value="VARCHAR">VARCHAR</option> <option value="TEXT">TEXT</option> <option
         * value="DATE">DATE</option> <optgroup label="NUMERIC"><option
         * value="TINYINT">TINYINT</option> <option value="SMALLINT">SMALLINT</option>
         * <option value="MEDIUMINT">MEDIUMINT</option> <option value="INT"
         * selected="selected">INT</option> <option value="BIGINT">BIGINT</option>
         * <option value="-">-</option> <option value="DECIMAL">DECIMAL</option> <option
         * value="FLOAT">FLOAT</option> <option value="DOUBLE">DOUBLE</option> <option
         * value="REAL">REAL</option> <option value="-">-</option> <option
         * value="BIT">BIT</option> <option value="BOOLEAN">BOOLEAN</option> <option
         * value="SERIAL">SERIAL</option> </optgroup><optgroup
         * label="DATE and TIME"><option value="DATE">DATE</option> <option
         * value="DATETIME">DATETIME</option> <option
         * value="TIMESTAMP">TIMESTAMP</option> <option value="TIME">TIME</option>
         * <option value="YEAR">YEAR</option> </optgroup><optgroup
         * label="STRING"><option value="CHAR">CHAR</option> <option
         * value="VARCHAR">VARCHAR</option> <option value="-">-</option> <option
         * value="TINYTEXT">TINYTEXT</option> <option value="TEXT">TEXT</option> <option
         * value="MEDIUMTEXT">MEDIUMTEXT</option> <option
         * value="LONGTEXT">LONGTEXT</option> <option value="-">-</option> <option
         * value="BINARY">BINARY</option> <option value="VARBINARY">VARBINARY</option>
         * <option value="-">-</option> <option value="TINYBLOB">TINYBLOB</option>
         * <option value="MEDIUMBLOB">MEDIUMBLOB</option> <option
         * value="BLOB">BLOB</option> <option value="LONGBLOB">LONGBLOB</option> <option
         * value="-">-</option> <option value="ENUM">ENUM</option> <option
         * value="SET">SET</option> </optgroup><optgroup label="SPATIAL"><option
         * value="GEOMETRY">GEOMETRY</option> <option value="POINT">POINT</option>
         * <option value="LINESTRING">LINESTRING</option> <option
         * value="POLYGON">POLYGON</option> <option
         * value="MULTIPOINT">MULTIPOINT</option> <option
         * value="MULTILINESTRING">MULTILINESTRING</option> <option
         * value="MULTIPOLYGON">MULTIPOLYGON</option> <option
         * value="GEOMETRYCOLLECTION">GEOMETRYCOLLECTION</option> </optgroup>
         */
    }

    /*
     * public static String rsGetterFor(String type, String field) {
     * type=type.replaceAll("[,0-9]", "");
     * if(type.contentEquals("int")||type.contentEquals("int()")) return
     * "c.getInt"+"(c.getColumnIndex(\""+field+"\"));"; else
     * if(type.contains("varchar()")) return
     * "c.getString"+"(c.getColumnIndex(\""+field+"\"));"; else
     * if(type.contentEquals("char()")) return
     * "c.getString"+"(c.getColumnIndex(\""+field+"\"));"; else
     * if(type.contentEquals("text")) return
     * "c.getString"+"(c.getColumnIndex(\""+field+"\"));"; else
     * if(type.contentEquals("tinytext")) return
     * "c.getString"+"(c.getColumnIndex(\""+field+"\"));"; else
     * if(type.contentEquals("date")) return
     * "StandardDateHelper.toDate(c.getString"+"(c.getColumnIndex(\""+field+
     * "\")));"; else if(type.contains("bigint")) return
     * "c.getLong"+"(c.getColumnIndex(\""+field+"\"));"; else
     * if(type.contains("tinyint") || type.contains("smallint") ||
     * type.contains("mediumint")) return
     * "c.getInt"+"(c.getColumnIndex(\""+field+"\"));"; else
     * if(type.contentEquals("decimal")||type.contentEquals("decimal()")) return
     * "c.getBigDecimal"+"(c.getColumnIndex(\""+field+"\"));"; else
     * if(type.contentEquals("float")||type.contentEquals("float()")) return
     * "c.getFloat"+"(c.getColumnIndex(\""+field+"\"));"; else
     * if(type.contentEquals("double")||type.contentEquals("double()")) return
     * "c.getDouble"+"(c.getColumnIndex(\""+field+"\"));"; else
     * if(type.contentEquals("boolean")||type.contentEquals("boolean()")) return
     * "c.getBoolean"+"(c.getColumnIndex(\""+field+"\"));"; else
     * if(type.contentEquals("datetime")||type.contentEquals("timestamp")) return
     * "c.getTimestamp"+"(c.getColumnIndex(\""+field+"\"));"; else
     * if(type.contains("enum")) return
     * "c.getString"+"(c.getColumnIndex(\""+field+"\"));"; else return ""; }
     */
    public static String stringifier(String type) {
        type = type.replaceAll("[,0-9]", "");
        if (type.contentEquals("int") || type.contentEquals("int()"))
            return ".toString()";
        else if (type.contains("varchar()"))
            return "";
        else if (type.contentEquals("char()"))
            return "";
        else if (type.contentEquals("text"))
            return "";
        else if (type.contentEquals("tinytext"))
            return "";
        else if (type.contentEquals("date"))
            return ".toString()";
        else if (type.contains("bigint"))
            return ".toString()";
        else if (type.contains("tinyint") || type.contains("smallint") || type.contains("mediumint"))
            return ".toString()";
        else if (type.contentEquals("decimal") || type.contentEquals("decimal()"))
            return ".toString()";
        else if (type.contentEquals("float") || type.contentEquals("float()"))
            return ".toString()";
        else if (type.contentEquals("double") || type.contentEquals("double()"))
            return ".toString()";
        else if (type.contentEquals("boolean") || type.contentEquals("boolean()"))
            return ".toString()";
        else if (type.contentEquals("datetime") || type.contentEquals("timestamp"))
            return ".toString()";
        else if (type.contains("enum"))
            return "";
        else
            return "";
    }

    public static String stringifiedWithNull(String field, String type) {
        type = type.replaceAll("[,0-9]", "");
        if (type.contentEquals("int") || type.contentEquals("int()"))
            return field + "!=null?" + field + ".toString():null";
        else if (type.contains("varchar()"))
            return field;
        else if (type.contentEquals("char()"))
            return field;
        else if (type.contentEquals("text"))
            return field;
        else if (type.contentEquals("tinytext"))
            return field;
        else if (type.contentEquals("date"))
            return field + "!=null?" + field + ".toString():null";
        else if (type.contains("bigint"))
            return field + "!=null?" + field + ".toString():null";
        else if (type.contains("tinyint") || type.contains("smallint") || type.contains("mediumint"))
            return field + "!=null?" + field + ".toString():null";
        else if (type.contentEquals("decimal") || type.contentEquals("decimal()"))
            return field + "!=null?" + field + ".toString():null";
        else if (type.contentEquals("float") || type.contentEquals("float()"))
            return field + "!=null?" + field + ".toString():null";
        else if (type.contentEquals("double") || type.contentEquals("double()"))
            return field + "!=null?" + field + ".toString():null";
        else if (type.contentEquals("boolean") || type.contentEquals("boolean()"))
            return field + "!=null?" + field + ".toString():null";
        else if (type.contentEquals("datetime") || type.contentEquals("timestamp"))
            return field + "!=null?" + field + ".toString():null";
        else if (type.contains("enum"))
            return field;
        else
            return field;
    }

    public static String lastThreeLetters(String s) {
        if (s.length() < 3)
            return "";
        return s.substring(s.length() - 3, s.length());
    }

    public static String minusLastThreeLetters(String s) {
        if (s.length() < 3)
            return s;
        return s.substring(0, s.length() - 3);
    }

    public static void genConfigFile(Table table, TableList tableList, RelationshipList relationshipList) {
        String tableName = table.getTable();
        TableConfig tableConfig = new TableConfig();
        // tableNames.add(tableName);

        tableConfig.setName(tableName);
        tableConfig.setSingular(CaseHelper.singularize(tableName));
        tableConfig.setPlural(tableName);
        tableConfig.setCode(CaseHelper.toCodeCase(tableName));
        tableConfig.setNamefield(table.getNameField());
        tableConfig.setType("");
        tableConfig.setFields(table.getFields());
        ArrayList<CustomTag> tableCustomTags = new ArrayList<CustomTag>();
        tableCustomTags.add(new CustomTag("tableTabCount", "2"));
        tableConfig.setCustomTags(tableCustomTags);

        // add supertables
        ArrayList<TableConfigDetail> supertables = new ArrayList<>();
        // for each relationship in relationship list
        // where child table = table name
        for (Relationship relationship : relationshipList.getSuperTableRelationships(tableName)) {
            String superTableName = relationship.getTable1();

            // load supertable from table list
            Table superTable = tableList.getTable(superTableName);

            // figure out foreign key field
            // remove _id if present, and set type to references
            // and update table config
            String fieldName = relationship.getField();
            String fieldNameSansId = fieldName;
            if (CaseHelper.lastThreeLetters(fieldName).contentEquals("_id")) {
                Field field = tableConfig.getField(fieldName);
                if (field == null)
                    continue;
                fieldNameSansId = CaseHelper.minusLastThreeLetters(fieldName);
                field.setField(fieldNameSansId);
                // add custom tags

                String refSnakeCaseSingular = CaseHelper.singularize(relationship.getTable1());
                String refCamelCaseCaps = CaseHelper.toCamelCaseCaps(refSnakeCaseSingular);
                String refTitleCase = CaseHelper.toTitleCase(refSnakeCaseSingular);
                String refNameField = superTable.getNameField();
                String fieldCodeCase = CaseHelper.toCodeCase(fieldNameSansId);
                String fieldCodeCaseCaps = CaseHelper.toCodeCaseCaps(fieldNameSansId);

                ArrayList<CustomTag> fieldCustomTags = new ArrayList<CustomTag>();
                fieldCustomTags.add(new CustomTag("refCamelCaseCaps", refCamelCaseCaps));
                fieldCustomTags.add(new CustomTag("refTitleCase", refTitleCase));
                fieldCustomTags.add(new CustomTag("refNameField", refNameField));
                fieldCustomTags.add(new CustomTag("fieldCodeCase", fieldCodeCase));
                fieldCustomTags.add(new CustomTag("fieldCodeCaseCaps", fieldCodeCaseCaps));
                field.setCustomTags(fieldCustomTags);
                field.setType("references");
                /*
                 * refCamelCaseCaps: InvoiceTemplate refTitleCase: Invoice Type refNameField:
                 * name fieldCodeCase: invtemp fieldCodeCaseCaps: InvTemp
                 */
            }

            // figure out supertable namefield
            String superTableNameField = "";// leave blank if null
            if (superTable != null)
                superTableNameField = superTable.getNameField();

            // String fieldCodeCase = CaseHelper.toCodeCase(fieldName);

            ArrayList<CustomTag> customTags = new ArrayList<CustomTag>();
            customTags.add(new CustomTag("fieldSnakeCaseSingular", fieldName));
            customTags.add(new CustomTag("fieldSansIdSnakeCaseSingular", fieldNameSansId));

            // add super table to tableConfig
            supertables.add(new TableConfigDetail(superTableName, fieldName, // field
                    CaseHelper.singularize(superTableName), superTableName, superTableNameField, "references",
                    CaseHelper.toCodeCase(superTableName), customTags, null));
        }
        tableConfig.setSuperTables(supertables);

        // add subtables
        ArrayList<TableConfigDetail> subtables = new ArrayList<>();
        for (Relationship relationship : relationshipList.getSubTableRelationships(tableName)) {
            String subTableName = relationship.getTable2();

            // ArrayList<CustomTag> customTags=new ArrayList<CustomTag>();
            // customTags.add(new CustomTag("fieldSnakeCaseSingular",fieldName));
            // customTags.add(new
            // CustomTag("fieldSansIdSnakeCasePlural",fieldNameSansIdPlural));

            Table subTable = tableList.getTable(subTableName);
            String subTableNameField = "";
            if (subTable != null)
                subTableNameField = subTable.getNameField();
            subtables.add(new TableConfigDetail(subTableName, relationship.getField(), // field
                    CaseHelper.singularize(subTableName), subTableName, subTableNameField, "",
                    CaseHelper.toCodeCase(subTableName),
                    // customTags,
                    null, null));
        }
        tableConfig.setSubTables(subtables);

        try {
            ObjectMapper om = new ObjectMapper(new YAMLFactory());
            om.writeValue(new File(config_path + "./base/" + tableName + ".yml"), tableConfig);
            File customConfigFile = new File(config_path + "./" + tableName + ".yml");
            if (!customConfigFile.exists()) {
                TableConfig tableCustomConfig = new TableConfig();
                tableCustomConfig.setName(tableConfig.getName());
                // leave everything else null
                // tableCustomConfig.setSingular(tableConfig.getSingular());
                // tableCustomConfig.setPlural(tableConfig.getPlural());
                // tableCustomConfig.setNamefield(tableConfig.getNamefield());
                om.writeValue(new File(config_path + "./" + tableName + ".yml"), tableCustomConfig);
            }
        } catch (IOException ex) {
            System.err.println("error writing mysql config to file for table " + tableName);
            System.err.println(ex.getMessage());
            System.err.println(ex.getCause());
        }

    }
}
