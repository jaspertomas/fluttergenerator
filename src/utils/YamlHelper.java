/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utils;

/**
 *
 * @author Jasper
 */
 
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.yaml.YAMLFactory;
 
public class YamlHelper {
    /*
    USAGE
    public static void main(String[] args) {
        String content = "";
        try {
            content = new String(Files.readAllBytes(Paths.get(
                    "D:\\NetBeansProjects\\MysqlModelGeneratorFlutter\\yaml.txt")));
            System.out.println("*********Content from YAML File ****************");
            System.out.println(content);
            String json = convertYamlToJson(content);
            System.out.println("*********Cnverted JSON from YAML File ****************");
            System.out.println(json);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
*/
 
    public static String convertYamlToJson(String yaml) {
        try {
            ObjectMapper yamlReader = new ObjectMapper(new YAMLFactory());
            Object obj = yamlReader.readValue(yaml, Object.class);
            ObjectMapper jsonWriter = new ObjectMapper();
            return jsonWriter.writerWithDefaultPrettyPrinter().writeValueAsString(obj);
        } catch (com.fasterxml.jackson.dataformat.yaml.snakeyaml.error.MarkedYAMLException ex) {
            System.err.println(ex.getOriginalMessage());
            System.exit(0);
        } catch (JsonProcessingException ex) {
            ex.printStackTrace();
        } catch (IOException ex) {
            ex.printStackTrace();
        }
        return null;
    }
}