/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utils.fileaccess;

import java.io.File;
import java.util.ArrayList;

/**
 *
 * @author Jasper
 */
public class FileHelper {
    // this walks through the file system and
    // writes filenames into a string array for later use
    // directories not written into array
    public static void listFiles(File folder, ArrayList<String> filenameArray) {
        for (File file : folder.listFiles()) {
            if (file.isDirectory()) {
                // System.out.println("Directory: " + file.getAbsolutePath());
                listFiles(file, filenameArray); // Calls same method again.
            } else {
                // System.out.println(file.getAbsolutePath().replace(systemPath, ""));
                filenameArray.add(file.getAbsolutePath());
            }
        }
    }

}
