/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utils;

/**
 *
 * @author Jasper
 */
public class CaseHelper {
    public static String ucfirst(String s)
    {
        if(s.length()==0)
        return "";
//        else if(s.length()==1)
//        return String.valueOf(s.charAt(0));
        else
        return s.substring(0, 1).toUpperCase()+s.substring(1);
    }
    public static Boolean isProbablySingular(String word)
    {
        if(singularize(word).contentEquals("[SingularizationError]"))return true;
        return false;
    }
    public static String singularize(String plural)
    {
        String singular=plural;
        //if length > 3 and last 3 letters = "ies"
        //convert ies to y
        if(plural.length()>3 && plural.substring(plural.length()-3, plural.length()).contentEquals("ies"))
        {
            singular=plural.substring(0,plural.length()-3)+"y";
        }
        //if length > 1 and last letter = "s"
        //just remove s
        else if(plural.length()>1 && plural.substring(plural.length()-1, plural.length()).contentEquals("s"))
        {
            singular=plural.substring(0,plural.length()-1);
        }
        else 
            singular= "[SingularizationError]";
        
        return singular;
    }
    public static String pluralize(String singular)
    {
        String plural=singular;
        //if length > 1 and last letter = "y"
        //just remove y and add ies
        if(singular.length()>1 && singular.substring(singular.length()-1, singular.length()).contentEquals("y"))
        {
            plural=singular.substring(0,singular.length()-1)+"ies";
        }
        //if length > 1 and last letter = "s"
        //just es
        else if(singular.length()>1 && singular.substring(singular.length()-1, singular.length()).contentEquals("s"))
        {
            plural=singular+"es";
        }
        //else just add s
        else 
            plural=singular+"s";
        
        return plural;
    }
    public static String toCamelCase(String string)
    {
        String[] segments=string.split("_");
        String output="";
        for(String s:segments)
            output+=ucfirst(s);
        //make first letter small
        String first=output.substring(0, 1);
        String rest=output.substring(1);
        return first.toLowerCase()+rest;
    }
    public static String toCamelCaseCaps(String string)
    {
        String[] segments=string.split("_");
        String output="";
        for(String s:segments)
            output+=ucfirst(s);
        return output;
    }
    public static String toTitleCase(String string)
    {
        String[] segments=string.split("_");
        String output="";
        for(String s:segments)
            output+=ucfirst(s)+" ";
        return output.trim();
    }
    public static String toSmallCase(String string)
    {
        String[] segments=string.split("_");
        String output="";
        for(String s:segments)
            output+=s.toLowerCase()+" ";
        return output.trim();
    }
    public static String toCodeCase(String string)
    {
//        return string.replaceAll("_", "").toLowerCase().substring(0, 3);
        String[] segments=string.split("_");
        String code="";
        for(String segment:segments)
        {
            switch(segment)
            {
                case "product":code+="prod";break;
                case "type":code+="type";break;
                case "warehouse":code+="ware";break;
                default: 
                    if(segment.length()<3)
                        code+=segment;
                    else
                        code+=segment.substring(0, 3);
            }
        }
        return code;
    }
    public static String toCodeCaseCaps(String string)
    {
//        return string.replaceAll("_", "").toLowerCase().substring(0, 3);
        String[] segments=string.split("_");
        String code="";
        for(String segment:segments)
        {
            switch(segment)
            {
                case "product":code+="Prod";break;
                case "type":code+="Type";break;
                case "warehouse":code+="Ware";break;
                default: 
                    if(segment.length()<3)
                        code+=CaseHelper.ucfirst(segment);
                    else
                        code+=CaseHelper.ucfirst(segment.substring(0, 3));
            }
        }
        return code;
    }
    public static String toUpperCase(String string)
    {
        return string.replaceAll("_", "").toUpperCase();
    }
    public static String lastThreeLetters(String s)
    {
        if(s.length()<3)return "";
        return s.substring(s.length()-3, s.length());
    }
    public static String minusLastThreeLetters(String s)
    {
        if(s.length()<3)return s;
        return s.substring(0,s.length()-3);
    }
}
