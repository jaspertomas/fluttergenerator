/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package utils;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Map;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

/**
 *
 * @author jaspertomas
 */
public class JsonHelper {
    public static void main(String args[]) {
        String json = "{\"name\":{\"first\":\"Joe\",\"last\":\"Sixpack\"},\"gender\":\"MALE\",\"verified\":false,\"userImage\":\"YWJjZA==\"}";
        try {
            Map<String, Object> map = toMap(json);
            for (String k : map.keySet()) {
                System.out.println(k);
                System.out.println(map.get(k));
            }
        } catch (IOException ex) {
            ex.printStackTrace();
        }

    }

    public static ObjectMapper mapper = new ObjectMapper(); // can reuse, share globally

    public static Map<String, Object> toMap(String json) throws IOException {
        // Map<String, Object> userData = mapper.readValue(json, Map.class);
        // return userData;
        return fromJSON(new TypeReference<Map<String, Object>>() {
        }, json);
    }

    public static String toJson(Map<String, Object> userData) throws IOException {
        return mapper.writeValueAsString(userData);
    }

    public static ArrayList<String> toArrayList(String json) throws IOException {
        // return mapper.readValue(json, ArrayList.class);

        return fromJSON(new TypeReference<ArrayList<String>>() {
        }, json);
    }

    public static String toJsonString(ArrayList<String> userData) throws IOException {
        return mapper.writeValueAsString(userData);
    }

    public static <T> T fromJSON(final TypeReference<T> type, final String jsonPacket) {
        T data = null;

        try {
            data = new ObjectMapper().readValue(jsonPacket, type);
        } catch (Exception e) {
            // Handle the problem
        }
        return data;
    }
}