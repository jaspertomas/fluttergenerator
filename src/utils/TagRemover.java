/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utils;

/**
 *
 * @author jaspertomas
 */
public class TagRemover {
    public static String process(String input)
    {
        String output="";
        Integer counter=0;
        for(Character c:input.toCharArray())
        {
            if(c=='<')counter++;
            else if(c=='>')counter--;
            else 
            {
                if(counter==0)output+=c;
            }
        }
        return output;
    }
}
