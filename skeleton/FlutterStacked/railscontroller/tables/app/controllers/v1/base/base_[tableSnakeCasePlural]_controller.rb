class V1::Base::Base[tableCamelCaseCapsPlural]Controller < ActionController::Base
  skip_before_action :verify_authenticity_token
  
  #upload image
  def upload_image
    user=User.check_logged_in(params[:token])
    return render json: {success: false, message: "Not logged in"} if user==nil

    [tableCamelCaseSingular]=[tableCamelCaseCapsSingular].find_by(id:params[:id])

    #validate attachments less than 20
    imagecount=[tableCamelCaseSingular].attachments.count
    return render json: {success: false, message: "Maximum number of pictures: #{Constants::MAX_PICTURES}. Delete some and wait for delete approval before taking more."} if imagecount >=Constants::MAX_PICTURES

    [tableCamelCaseSingular].gen_image_from_multipart_form_data(params[:image],params[:filename],user)
    
    return render json: {success: true, data: {
      image_url: [tableCamelCaseSingular].image_url,
      id:[tableCamelCaseSingular].id,
    }}
  end  

  #create or update a [tableCamelCaseSingular]
  def update
    user=User.check_logged_in(params[:token])
    return render json: {success: false, message: "Not logged in"} if user==nil

    # #validate name must be unique
    # params[:id]=0 if params[:id]==""
    # duplicate=[tableCamelCaseCapsSingular].where(name:params[:name]).where.not(id:params[:id]).first
    # return render json: {success: false, message: "A [tableCamelCaseSingular] with this name already exists"} if duplicate != nil

    [tableCamelCaseSingular]=[tableCamelCaseCapsSingular].find_by(id:params[:id])
    if([tableCamelCaseSingular]==nil)
      [tableCamelCaseSingular]=[tableCamelCaseCapsSingular].create!([tableCamelCaseSingular]_params)
      History.create!(
        historable_type:[tableCamelCaseSingular].class.name,
        historable_id:[tableCamelCaseSingular].id,
        description: "Status: #{params[:status]}; Remarks: #{params[:remarks]}",
        created_by:user,
      )
    else
      update=""
      # return render json: {success: false, message: "No change"} if(update=="")
      [tableCamelCaseSingular].update!([tableCamelCaseSingular]_params)
      History.create!(
        historable_type:[tableCamelCaseSingular].class.name,
        historable_id:[tableCamelCaseSingular].id,
        description: "Update: #{update}",
        created_by:user,
      )
    end

    return render json: {success: true, data: {id: [tableCamelCaseSingular].id}}
  end    

  def delete
    user=User.check_logged_in(params[:token])
    return render json: {success: false, message: "Not logged in"} if user==nil

    [tableCamelCaseSingular]=[tableCamelCaseCapsSingular].find_by(id:params["id"])
    return render json: {success: false, message: "Item not found"} if [tableCamelCaseSingular]==nil

    begin
      [tableCamelCaseSingular].destroy
      render json: {success: true}
    rescue => e
      return render json: {success: false, message: e.message}
    end
  end

  def mass_delete
    user=User.check_logged_in(params[:token])
    return render json: {success: false, message: "Not logged in"} if user==nil

    ids=YAML.load(params["ids"])
    begin
      [tableCamelCaseCapsSingular].where(id:ids).destroy_all
      render json: {success: true}
    rescue => e
      return render json: {success: false, message: e.message}
    end
  end

  def add_history
    user=User.check_logged_in(params[:token])
    return render json: {success: false, message: "Not logged in"} if user==nil

    [tableCamelCaseSingular]=[tableCamelCaseCapsSingular].find_by(id:params[:id])
    History.create!(
      historable_type:[tableCamelCaseSingular].class.name,
      historable_id:[tableCamelCaseSingular].id,
      description: params[:description],
      created_by:user,
    )

    return render json: {success: true, data: {id: [tableCamelCaseSingular].id}}
  end    
=begin
  def list
    parent_type=params[:parent_type]
    parent_id=params[:parent_id]
    
    user=User.check_logged_in(params[:token])
    return render json: {success: false, message: "Not logged in"} if user==nil

    case parent_type
    when 'c'
      focus="Citizen"
    when 'a'
      focus="Address"
    end
    items=[tableCamelCaseCapsSingular].where(focus:focus).order("created_at desc")

    render json: 
    {
      success: true,
      data: 
      {
        items: items.map { |item| item.to_formatted_hash(user,parent_type,parent_id) }
      }
    }
  end
  
  #publish or unpublish this and all its children
  def publish
    id=params[:id]
    publish=params[:is_published] == "true"
    user=User.check_logged_in(params[:token])
    return render json: {success: false, message: "Not logged in"} if user==nil

    [tableCamelCaseSingular]=[tableCamelCaseCapsSingular].find_by(id:id)
    return render json: {success: false, message: "Item not found"} if([tableCamelCaseCapsSingular]==nil)

    # [tableCamelCaseSingular].cascade_publish(publish)
    [tableCamelCaseSingular].update(is_published:publish)
    if publish
      [tableCamelCaseSingular].delete_request.destroy if [tableCamelCaseSingular].delete_request
      History.create!(
        historable_type:[tableCamelCaseSingular].class.name,
        historable_id:[tableCamelCaseSingular].id,
        description: "[tableCamelCaseCapsSingular] delete cancelled",
        created_by:user,
      )
    else
      duplicate=DeleteRequest.find_by(deletable: [tableCamelCaseSingular])
      DeleteRequest.create!(
        deletable: [tableCamelCaseSingular], reason:params[:reason], created_by_id:user.id) if duplicate == nil
      History.create!(
        historable_type:[tableCamelCaseSingular].class.name,
        historable_id:[tableCamelCaseSingular].id,
        description: "[tableCamelCaseCapsSingular] delete requested; Reason: #{params[:reason]}",
        created_by:user,
      )
    end
    return render json: {success: true, data:{}}
  end
=end


  def search
    user=User.check_logged_in(params[:token])
    return render json: {success: false, message: "Not logged in"} if user==nil

    params[:page]=0 if(!params.has_key?(:page))
    params[:search_string]="" if params[:search_string]==nil
    #search database for [tableCamelCaseSingular] that matches keywords or name
    search_keys=params[:search_string].strip.split /\s+/

    #put together query as string
    query=""
    search_keys.each do |k|
      #add 'or' if query string is not empty
      if(!query.empty?) then query+=" and " end
      #[tableCamelCaseSingular] code must be exact match
      query+="[tableNameField] like '%#{k}%'"
      # query+=" or description like '%#{k}%'"
    end  

    [tableCamelCasePlural]=[tableCamelCaseCapsSingular].where(query).order(:[tableNameField]) #is_published:true, #.where(barangay_id:barangay_id)
    total=[tableCamelCasePlural].count
    [tableCamelCasePlural]=[tableCamelCasePlural].paginate(page: params[:page], per_page: Constants::RECORDS_PER_PAGE)

    # print "==returned items:==#{[tableCamelCasePlural].length}========"
    render json: 
    {
      success: true, 
      data: 
      {
        total: total,
        # [tableCamelCasePlural]: [tableCamelCasePlural].collect(&:to_formatted_hash)
        items: [tableCamelCasePlural].map { |[tableCamelCaseSingular]| [tableCamelCaseSingular].to_formatted_hash(user) }
      }
    }
  end

  def select
    user=User.check_logged_in(params[:token])
    return render json: {success: false, message: "Not logged in"} if user==nil

    params[:page]=0 if(!params.has_key?(:page))
  
    query=""
  
    #add search criteria to query
    #params["search_strings"] is a string, convert it into a hash
    #convert '{[fieldCamelCaseSingular]: 0001, id: 1}' into '{\"[fieldCamelCaseSingular]\": \"0001\", \"id\": \"1\"}'
    #then parse using json
    search_strings=JSON.parse params["search_strings"].gsub("{","{\"").gsub("}","\"}").gsub(", ","\", \"").gsub(": ","\": \"")

    field_list=[
#[selectfields]
    ]

    field_list.each do |field|
      #allow only search strings for specified fields
      search_string=search_strings[field]

      #ignore field if search string is empty or null
      next if search_string.blank?

      #split search string into individual words
      search_keys=search_string.strip.split /\s+/
    
      #for each word, add condition to query
      search_keys.each do |k|
        #add 'or' if query string is not empty
        if(!query.empty?) then query+=" and " end
        #[tableCamelCaseSingular] [fieldCamelCaseSingular] must be exact match
        query+="#{field} like '%#{k}%'"
        # query+=" or description like '%#{k}%'"
      end  
    end 
    [tableCamelCasePlural]=[tableCamelCaseCapsSingular].where(query).order(:[tableNameField]) #is_published:true, #.where(barangay_id:barangay_id)
    total=[tableCamelCasePlural].count
    [tableCamelCasePlural]=[tableCamelCasePlural].paginate(page: params[:page], per_page: Constants::RECORDS_PER_PAGE)
  
    render json: 
    {
      success: true, 
      data: 
      {
        total: total,
        # [tableCamelCasePlural]: [tableCamelCasePlural].collect(&:to_formatted_hash)
        items: [tableCamelCasePlural].map { |[tableCamelCaseSingular]| [tableCamelCaseSingular].to_formatted_hash(user) }
      }
    }
  end

#[subtabletableFunctions]

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_[tableCamelCaseSingular]
      @[tableCamelCaseSingular] = [tableCamelCaseCapsSingular].find(params[:id])
    end

    # Only allow a list of trusted parameters through.
    def [tableCamelCaseSingular]_params
      # params.require(:[tableCamelCaseSingular]).permit(:name, :product_type, ...)
      params.permit(
#[permitfields]
      )
    end
end
