[tableRoutes]
------

     post '[tableCodeCase]/search', :to => '[tableSnakeCasePlural]#search'
     post '[tableCodeCase]/update/:token', :to => '[tableSnakeCasePlural]#update'
     post '[tableCodeCase]/image/:token', :to => '[tableSnakeCasePlural]#upload_image'
     # get '[tableCodeCase]/list/:parent_type/:parent_id/:token', :to => '[tableSnakeCasePlural]#list'
     # post '[tableCodeCase]/publish/:id/:token', :to => '[tableSnakeCasePlural]#publish'
     post '[tableCodeCase]/delete/:token', :to => '[tableSnakeCasePlural]#delete'
     post '[tableCodeCase]/mass_delete/:token', :to => '[tableSnakeCasePlural]#mass_delete'
     post '[tableCodeCase]/add_history/:id/:token', :to => '[tableSnakeCasePlural]#add_history'
     post '[tableCodeCase]/select/:token', :to => '[tableSnakeCasePlural]#select'
======
