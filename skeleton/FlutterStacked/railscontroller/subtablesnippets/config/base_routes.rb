[subtableTableRoutes]
------

     post '[tableCodeCase]/create_[subtableCodeCase]/:token', :to => '[tableSnakeCasePlural]#create_[subtableCodeCase]'
     post '[tableCodeCase]/update_[subtableCodeCase]/:token', :to => '[tableSnakeCasePlural]#update_[subtableCodeCase]'
     get '[tableCodeCase]/list_[subtableCodeCase]/:page/:[tableSnakeCaseSingular]_id/:token', :to => '[tableSnakeCasePlural]#list_[subtableCodeCase]'
======
