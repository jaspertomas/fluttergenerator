#[subtabletableFunctions]
------
# -------------- [subtableUpperCaseSingular] FUNCTIONS -------------------------
  #create a [subtableCamelCaseCapsSingular]
  def create_[subtableCodeCase]
    [tableCamelCaseSingular]_id=params[:[tableCamelCaseSingular]_id]

    user=User.check_logged_in(params[:token])
    return render json: {success: false, message: "Not logged in"} if user==nil

    # [tableCamelCaseSingular]=[subtableCamelCaseSingular].[tableCamelCaseSingular]
    [subtableCamelCaseCapsSingular].create!(
      name:params[:name],
      [tableCamelCaseSingular]_id:params[:[tableCamelCaseSingular]_id],
    )
    
    #create history
    update=""#{[subtableCamelCaseSingular].name}
    History.create!(
      historable_type:[tableCamelCaseSingular].class.name,
      historable_id:[tableCamelCaseSingular].id,
      description: "Created: #{update}",
      created_by:user,
    )

    #calc [tableCamelCaseSingular] receive status 
    #and create history if necessary
    #[tableCamelCaseSingular].calc_received(user)

    return render json: {success: true, data: {}}
  end    
  
  #update a [subtableCamelCaseSingular]
  def update_[subtableCodeCase]
    user=User.check_logged_in(params[:token])
    return render json: {success: false, message: "Not logged in"} if user==nil

    [subtableCamelCaseSingular]=[subtableCamelCaseCapsSingular].find_by(id:params[:id])
    return render json: {success: false, message: "No Change"} if params[:qty_received].to_i==[subtableCamelCaseSingular].qty_received

    [tableCamelCaseSingular]=[subtableCamelCaseSingular].[tableCamelCaseSingular]
    old_qty_received=[subtableCamelCaseSingular].qty_received
    [subtableCamelCaseSingular].update!(
      qty_received:params[:qty_received],
    )
    
    #create history
    update="#{[subtableCamelCaseSingular].product.name}: Receive qty = #{params[:qty_received]}, previously #{old_qty_received} "
    History.create!(
      historable_type:[tableCamelCaseSingular].class.name,
      historable_id:[tableCamelCaseSingular].id,
      description: "Update: #{update}",
      created_by:user,
    )

    #calc [tableCamelCaseSingular] receive status 
    #and create history if necessary
    [tableCamelCaseSingular].calc_received(user)

    return render json: {success: true, data: {}}
  end    

  def list_[subtableCodeCase]
    [tableCamelCaseSingular]_id=params[:[tableCamelCaseSingular]_id]
    
    user=User.check_logged_in(params[:token])
    return render json: {success: false, message: "Not logged in"} if user==nil

    items=[subtableCamelCaseCapsSingular].where([tableCamelCaseSingular]_id:[tableCamelCaseSingular]_id)

    render json: 
    {
      success: true,
      data: 
      {
        items: items.map { |item| item.to_formatted_hash(user) }
      }
    }
  end
======
