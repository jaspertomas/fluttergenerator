class V1::AttachmentsController < ActionController::Base
    skip_before_action :verify_authenticity_token
    
    #create or update a attachment
    def update
        user=User.check_logged_in(params[:token])
        return render json: {success: false, message: "Not logged in"} if user==nil
    
        attachment=Attachment.find_by(id:params[:id])
        # attachment.update!(
        #   is_published: params[:is_published],
        #   # updated_by:user,
        # )
        if(params[:is_published]=="false")
            # duplicate=DeleteRequest.find_by(deletable: attachment)
            # DeleteRequest.create!(barangay_id:attachment.attachable.barangay_id,deletable: attachment, reason:params[:reason], created_by_id:user.id) if duplicate == nil
            attachment.cascade_delete
        end

        return render json: {success: true, data: {id: attachment.id}}
    end    
    
    def list
        parent_type=params[:parent_type]
        parent_id=params[:parent_id]
        
        user=User.check_logged_in(params[:token])
        return render json: {success: false, message: "Not logged in"} if user==nil
        
        case parent_type
#[list]
        end

        #hide attachments that are "deleted"
        items=Attachment.where(attachable:parent).order("created_at desc")
        # items=items.paginate(page: params[:page], per_page: Constants::RECORDS_PER_PAGE)

        render json: 
        {
            success: true,
            data: 
            {
                items: items.map { |item| item.to_formatted_hash(user) }
            }
        }
    end
end