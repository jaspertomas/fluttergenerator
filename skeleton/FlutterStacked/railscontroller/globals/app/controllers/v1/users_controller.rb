class V1::UsersController < ActionController::Base
  skip_before_action :verify_authenticity_token

  def login
    #search database for user with same email
    username=params[:username]
    password=params[:password]
    #flutter login sends username, use the login field you want
    user=User.find_by(email:username, is_active:1)

    return render json: {success: false, error: "User not found"} if user==nil

    if(user.valid_password?(password))
      #create token
      user.gen_token
      return render json: {success: true, data: user.to_formatted_hash}
    else
      return render json: {success: false, error: "Invalid password"}
    end
  end

  def logout
    #search database for user with same email
    token=params[:token]
    user=User.find_by(token:token)
    user.update!(token:nil)

    return render json: {success: true, data: {}}
  end
end
#[donotgenerate]
