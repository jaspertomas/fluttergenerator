class V1::HistoriesController < ActionController::Base
    skip_before_action :verify_authenticity_token
  
    #create or update a history
    def update
        user=User.check_logged_in(params[:token])
        return render json: {success: false, message: "Not logged in"} if user==nil

        history=History.find_by(id:params[:id])
        if(history==nil)
            history=History.create!(
            description:params[:description],
            created_by:user,
            )
        else
            history.update!(
            request_description:params[:request_description],
            request_message:params[:request_message],
            kind:params[:kind],
            request_date:params[:request_date],
            updated_by:user,
            )
        end
    
        return render json: {success: true, data: {id: history.id}}
    end    
  
    
    def list
        parent_type=params[:parent_type]
        parent_id=params[:parent_id]
        
        user=User.check_logged_in(params[:token])
        return render json: {success: false, message: "Not logged in"} if user==nil

        case parent_type
#[list]
        end

        #prevent access to items outside barangay
        # items=items.where(is_published:true,barangay_id:barangay_id).order("created_at desc")
        items=items.order("created_at desc")
        items=items.paginate(page: params[:page], per_page: Constants::RECORDS_PER_PAGE)
    
        render json: 
        {
            success: true,
            data: 
            {
            items: items.map { |item| item.to_formatted_hash(user) }
            }
        }
    end
  
  end
  
