require_relative 'base_routes'

Rails.application.routes.draw do
  devise_for :users
  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html

  # get 'home/index'
  # get 'debug/index'
  namespace :v1, defaults: { format: :json } do 
    # get 'debug/index'
    post 'users/login'
    post 'users/logout'
    #all purpose search by id
    get 'view/:type/:id/:token', :to => 'search#view'
    get 'his/list/:page/:parent_type/:parent_id/:token', :to => 'histories#list'
    get 'att/list/:page/:parent_type/:parent_id/:token', :to => 'attachments#list'
    post 'att/update/:id/:token', :to => 'attachments#update'

    base_routes

  end
end
#[donotgenerate]
