#[import]
------
require_relative 'base_routes'
======
#[routes_block]
------
  namespace :v1, defaults: { format: :json } do 
    get 'debug/index'
    post 'users/login'
    post 'users/logout'
    #all purpose search by id
    get 'view/:type/:id/:token', :to => 'search#view'

    base_routes
    #[donotgenerate]
  end
======
