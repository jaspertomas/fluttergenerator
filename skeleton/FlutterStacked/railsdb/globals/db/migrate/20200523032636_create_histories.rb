class CreateHistories < ActiveRecord::Migration[5.2]
  def change
    create_table :histories do |t|
      t.integer :historable_id
      t.string :historable_type, limit: 25
      t.string :description
      t.string :meta

      t.bigint :created_by_id
      t.timestamps
    end
  end
end
