[formattedhashfields]
------
string
---
      [fieldSnakeCaseSingular]: [fieldSnakeCaseSingular],
===
datetime
---
      [fieldSnakeCaseSingular]: (user!=nil) ? [fieldSnakeCaseSingular].in_time_zone(user.time_zone) : [fieldSnakeCaseSingular],
===
picker
---
      [fieldSnakeCaseSingular]_id: [fieldSnakeCaseSingular]_id,
      [fieldSnakeCaseSingular]: [fieldSnakeCaseSingular].to_s,
===
references
---
      [fieldSnakeCaseSingular]_id: [fieldSnakeCaseSingular]_id,
      [fieldSnakeCaseSingular]: [fieldSnakeCaseSingular].to_s,
======