class RandomHelper
  def self.random_string(length=0)
    SecureRandom.hex(length)
  end
end
