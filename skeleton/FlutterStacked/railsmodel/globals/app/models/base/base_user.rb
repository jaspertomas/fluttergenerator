class Base::BaseUser < ApplicationRecord
  self.table_name = "users"
  #belongs_to :[supertableSnakeCaseSingular], optional: true
  #has_many :[subtableSnakeCasePlural]

  has_many :attachments, as: :attachable
  has_one_attached :qr_code_image

  def self.check_logged_in(token=nil)
    return nil if token==nil
    user=User.find_by(token:token, is_active:1)
    return nil if user==nil
    return nil if(DateTime.now>user.token_expiry)

    user.update!(token_expiry:DateTime.now+Constants::TOKEN_EXPIRES_AFTER_MINUTES.minutes)
    user
  end  

  def last_image
    attachments=self.attachments#.where(is_published:true)
    return nil if(attachments.count==0)
    attachments.last.image
  end
  def image_url
    attachments=self.attachments#.where(is_published:true)
    return nil if(attachments.count==0)
    attachments.last.url
  end
  def gen_image_from_multipart_form_data(data,filename,user)
    image=Attachment.create!(attachable:self, created_by: user)
    image.gen_image_from_multipart_form_data(data,filename)
    image
  end
  def gen_image_from_path(hash)
    image=Attachment.create!(attachable:self)
    image.gen_image_from_path(hash)
    image
  end

  def gen_qr_code_image
    require 'rqrcode'

    #if user already has a qr code, do nothing
    return if(self.qr_code_image.attached?)

    qrcode = RQRCode::QRCode.new("use;"+self.id)
    png = qrcode.as_png(
        bit_depth: 1,
        border_modules: 4,
        color_mode: ChunkyPNG::COLOR_GRAYSCALE,
        color: 'black',
        file: nil,
        fill: 'white',
        module_px_size: 6,
        resize_exactly_to: false,
        resize_gte_to: false,
        size: 120
        )
    self.qr_code_image.attach(io: StringIO.new(png.to_s),  filename: self.fullname+'.png') 
  end

  def to_formatted_hash(user=self)
    {
      type: 'use',
      id: id.to_s,
      image_url: self.image_url,
      first_name: first_name,
      middle_name: middle_name,
      last_name: last_name,
      is_active: is_active,
      # admin: admin,
      last_login: last_login,
      username: username,
      token: token,
      token_expiry: token_expiry,
      phone: phone,
      description: description,
      birthday: birthday,
      email: email,
      time_zone: time_zone,
      encrypted_password: encrypted_password,
      reset_password_token: reset_password_token,
      reset_password_sent_at: reset_password_sent_at,
      remember_created_at: remember_created_at,
      created_at: created_at.in_time_zone(user.time_zone),
      updated_at: updated_at.in_time_zone(user.time_zone),
    }
  end
  def to_s
    username
  end
end
