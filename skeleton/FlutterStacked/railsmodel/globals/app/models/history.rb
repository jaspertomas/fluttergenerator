class History < ApplicationRecord
    belongs_to :historable, polymorphic: true
    # has_many :images, as: :imagable

    belongs_to :created_by, class_name: "User", foreign_key: "created_by_id", optional:true

    def to_formatted_hash(user=nil)
        {
          type: 'h',
          id: id,
          description: description,
          meta: meta,
          historable_type:historable_type,
          historable_id:historable_id,
          created_by_id: created_by_id,
          created_by: created_by==nil ? "" : created_by.username,
          created_at: created_at,
        }
    end    
end
