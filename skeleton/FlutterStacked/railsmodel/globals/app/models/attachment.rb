class Attachment < ApplicationRecord
    #first and last by created_at coz id is uuid
    # self.implicit_order_column = "created_at"

    belongs_to :attachable, polymorphic: true
    belongs_to :created_by, class_name: "User", foreign_key: "created_by_id", optional: true
    belongs_to :updated_by, class_name: "User", foreign_key: "updated_by_id", optional: true

    has_one_attached :image

    def fullname
        attachable.fullname
    end

    def gen_image_from_multipart_form_data(data,filename)
        #self.image.attach(io: StringIO.new(data), filename: filename)#, content_type: content_type
        image = MiniMagick::Image.read(StringIO.new(data))
        image.resize "200x200"
        self.image.attach(io: URI.open(image.path), filename: filename)
    end
    def gen_image_from_path(hash)
        path=hash[:path]
        image = MiniMagick::Image.open(path)
        image.resize "200x200"
        filename=hash[:filename]
        content_type=hash[:content_type]
        self.image.attach(io: URI.open(image.path), filename: filename, content_type: content_type)
    end

    def url
        #if no attachment, delete self and return null
        if !self.image.attached?
            self.destroy
            return nil
        end
        Rails.application.routes.url_helpers.rails_blob_path(self.image, only_path: true)
    end   

    def cascade_delete
        self.image.purge
        self.destroy
    end

    def to_formatted_hash(user=nil)
        {
          id: id,
          date: (user!=nil) ? created_at.in_time_zone(user.time_zone) : created_at,
          url: self.url,
        }
    end    
end
