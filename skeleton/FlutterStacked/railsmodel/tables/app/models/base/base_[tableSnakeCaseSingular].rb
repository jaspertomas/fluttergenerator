class Base::Base[tableCamelCaseCapsSingular] < ApplicationRecord
  self.table_name = "[tableSnakeCasePlural]"
  #belongs_to :[supertableSnakeCaseSingular], optional: true
  #has_many :[subtableSnakeCasePlural]

  has_many :attachments, as: :attachable
  has_one_attached :qr_code_attachment

#[has_many]

#[belongs_to]

  def last_image
    last_attachment
  end
  def last_attachment
    attachments=self.attachments#.where(is_published:true)
    return nil if(attachments.count==0)
    attachments.last.attachment
  end
  def image_url
    attachment_url
  end
  def attachment_url
    attachments=self.attachments#.where(is_published:true)
    return nil if(attachments.count==0)
    attachments.last.url
  end
  def gen_image_from_multipart_form_data(data,filename,user)
    attachment=Attachment.create!(attachable:self, created_by: user)
    attachment.gen_image_from_multipart_form_data(data,filename)
    attachment
  end
  def gen_image_from_path(hash)
    attachment=Attachment.create!(attachable:self)
    attachment.gen_image_from_path(hash)
    attachment
  end

  def gen_qr_code_attachment
    require 'rqrcode'

    #if [tableCamelCaseSingular] already has a qr code, do nothing
    return if(self.qr_code_attachment.attached?)

    qrcode = RQRCode::QRCode.new("[tableCodeCase];"+self.id)
    png = qrcode.as_png(
        bit_depth: 1,
        border_modules: 4,
        color_mode: ChunkyPNG::COLOR_GRAYSCALE,
        color: 'black',
        file: nil,
        fill: 'white',
        module_px_size: 6,
        resize_exactly_to: false,
        resize_gte_to: false,
        size: 120
        )
    self.qr_code_attachment.attach(io: StringIO.new(png.to_s),  filename: self.fullname+'.png') 
  end

  def to_formatted_hash(user=nil)
    {
      type: '[tableCodeCase]',
      id: id.to_s,
      attachment_url: self.attachment_url,
[formattedhashfields]
      created_at: (user!=nil) ? created_at.in_time_zone(user.time_zone) : created_at,
      updated_at: (user!=nil) ? updated_at.in_time_zone(user.time_zone) : updated_at,
    }
  end
  def to_s
    [tableNameField]
  end
end
