class [subtableCamelCaseCapsSingular] < ApplicationRecord
  #self.table_name = "[subtableSnakeCaseSingular]"
  #belongs_to :[tableSnakeCaseSingular], optional: true

=begin
  has_many :images, as: :imagable
  has_one_attached :qr_code_image

  def last_image
    images=self.images#.where(is_published:true)
    return nil if(images.count==0)
    images.last.image
  end
  def image_url
    images=self.images#.where(is_published:true)
    return nil if(images.count==0)
    images.last.url
  end
  def gen_image_from_multipart_form_data(data,filename,user)
    image=Image.create!(imagable:self, created_by: user)
    image.gen_image_from_multipart_form_data(data,filename)
    image
  end
  def gen_image_from_path(hash)
    image=Image.create!(imagable:self)
    image.gen_image_from_path(hash)
    image
  end

  def gen_qr_code_image
    require 'rqrcode'

    #if [subtableCamelCaseSingular] already has a qr code, do nothing
    return if(self.qr_code_image.attached?)

    qrcode = RQRCode::QRCode.new("p;"+self.id)
    png = qrcode.as_png(
        bit_depth: 1,
        border_modules: 4,
        color_mode: ChunkyPNG::COLOR_GRAYSCALE,
        color: 'black',
        file: nil,
        fill: 'white',
        module_px_size: 6,
        resize_exactly_to: false,
        resize_gte_to: false,
        size: 120
        )
    self.qr_code_image.attach(io: StringIO.new(png.to_s),  filename: self.fullname+'.png') 
  end
=end

  def to_formatted_hash(user=nil)
    {
      type: '[subtableCodeCase]',
      id: id.to_s,
      name: name,
      image_url: self.image_url,
    }
  end
end
