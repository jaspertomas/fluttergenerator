import 'package:flutter/material.dart';
import 'package:stacked/stacked.dart';
// import '../../utils/DialogHelper.dart';
import '../../[tableCamelCaseCapsSingular]Edit/[tableCamelCaseCapsSingular]EditBaseView.dart';
import '[tableCamelCaseCapsSingular]EditTabModel.dart';
import '../[tableCamelCaseCapsSingular]ViewModel.dart';

class [tableCamelCaseCapsSingular]EditTab extends [tableCamelCaseCapsSingular]EditBaseView {
  final [tableCamelCaseCapsSingular]ViewModel parentModel;
  [tableCamelCaseCapsSingular]EditTab(this.parentModel) : super(parentModel.[tableCamelCaseSingular].id);
  @override
  Widget build(BuildContext context) {
    return ViewModelBuilder<[tableCamelCaseCapsSingular]EditTabModel>.reactive(
      viewModelBuilder: () => [tableCamelCaseCapsSingular]EditTabModel(parentModel),
      onModelReady: (model) async {
        // model.[tableCamelCaseSingular] = this.parentModel.[tableCamelCaseSingular];
        /*
          if (id != null) {
            await model.load(id);
            //comment this out to hide error messages on first load
            model.valid();
          }
          if (customerName != null) {
            model.[tableCamelCaseSingular].customerName = customerName;
            //comment this out to hide error messages on first load
            model.valid();
          }
          */
      },
      builder: (context, model, child) => Center(
        child: model.isBusy
            ? CircularProgressIndicator()
            : SingleChildScrollView(
                child: Column(
                  children: [
                    [tableNameField]Field(model, context),
                    submitButton(model, context),
                    cancelButton(),
                  ],
                ),
              ),
      ),
    );
  }
}
