import '../../../models/[tableCamelCaseCapsSingular].dart';
import '../../[tableCamelCaseCapsSingular]Edit/[tableCamelCaseCapsSingular]EditBaseViewModel.dart';
import '../[tableCamelCaseCapsSingular]ViewModel.dart';

class [tableCamelCaseCapsSingular]EditTabModel extends [tableCamelCaseCapsSingular]EditBaseViewModel {
  [tableCamelCaseCapsSingular]ViewModel parent;
  [tableCamelCaseCapsSingular]EditTabModel(this.parent);

  [tableCamelCaseCapsSingular] get [tableCamelCaseSingular] => parent.[tableCamelCaseSingular];
}
