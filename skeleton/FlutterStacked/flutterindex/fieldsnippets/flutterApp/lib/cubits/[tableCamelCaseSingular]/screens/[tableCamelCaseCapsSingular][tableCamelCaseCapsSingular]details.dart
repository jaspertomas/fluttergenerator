// // import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:flutter/material.dart';
// import '../[tableCamelCaseCapsSingular]ViewScreenSwitch.dart';
import '../[tableCamelCaseCapsSingular]Cubit.dart';
import './[tableCamelCaseCapsSingular][tableCamelCaseCapsSingular]detailListItem.dart';
import '../../../config/Constants.dart';

class [tableCamelCaseCapsSingular][tableCamelCaseCapsSingular]details extends StatefulWidget {
  const [tableCamelCaseCapsSingular][tableCamelCaseCapsSingular]details({Key? key}) : super(key: key);

  @override
  _[tableCamelCaseCapsSingular][tableCamelCaseCapsSingular]detailsState createState() =>
      _[tableCamelCaseCapsSingular][tableCamelCaseCapsSingular]detailsState();
}

class _[tableCamelCaseCapsSingular][tableCamelCaseCapsSingular]detailsState extends State<[tableCamelCaseCapsSingular][tableCamelCaseCapsSingular]details> {
  // ScrollController _scrollController;
  bool isLoading = true;
  bool isInit = false;

  @override
  void didChangeDependencies() async {
    if (!isInit) {
      await [tableCamelCaseCapsSingular]Cubit.load[tableCamelCaseCapsSingular]detailList(context);
      setState(() {
        isLoading = false;
        isInit = true;
      });
    }

    super.didChangeDependencies();
  }

  @override
  Widget build(BuildContext context) {
    // [tableCamelCaseCapsSingular] [tableCamelCaseSingular] = [tableCamelCaseCapsSingular](ItemCubit.item);
    List<dynamic> list = [tableCamelCaseCapsSingular]Cubit.[tableCamelCaseSingular]detailList;
    final deviceSize = MediaQuery.of(context).size;

    return Column(
      children: <Widget>[
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: <Widget>[
            Text("Details", style: Constants.textStyleFontSize20Bold),
            /*
            //--------NEW [tableUpperCaseSingular]DETAIL-------
            IconButton(
              icon: FaIcon(FontAwesomeIcons.plus, color: Colors.blue, size: 22),
              onPressed: () {
                // newStockEntry([tableCamelCaseSingular]);
                [tableCamelCaseCapsSingular]Cubit.mode = [tableCamelCaseCapsSingular]Cubit.NEW[tableUpperCaseSingular]DETAIL;
                [tableCamelCaseCapsSingular]ViewScreenSwitch.refresh();
              },
            ),
             */
            IconButton(
              icon: Icon(Icons.refresh, color: Colors.blue, size: 30),
              onPressed: () async {
                setState(() {
                  isLoading = true;
                });
                await [tableCamelCaseCapsSingular]Cubit.load[tableCamelCaseCapsSingular]detailList(context);
                setState(() {
                  isLoading = false;
                });
              },
            ),
          ],
        ),
        isLoading
            ? Center(
                child: CircularProgressIndicator(),
              )
            : Column(
                children: <Widget>[
                  list.length == 0
                      ? Text(
                          "No items found",
                          style: TextStyle(fontSize: 20),
                        )
                      : Container(
                          height: deviceSize.height - 310,
                          child: ListView(
                            // controller: _scrollController,
                            children: [
                              [tableCamelCaseCapsSingular][tableCamelCaseCapsSingular]detailListItem.headers(),
                              ...list
                                  .map((item) =>
                                      [tableCamelCaseCapsSingular][tableCamelCaseCapsSingular]detailListItem(item))
                                  .toList(),
                            ],
                          ),
                        ),
                ],
              )
      ],
    );
  }
}
