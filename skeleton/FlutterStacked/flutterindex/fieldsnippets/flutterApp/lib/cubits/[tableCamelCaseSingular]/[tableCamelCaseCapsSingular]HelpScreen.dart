import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:flutter/material.dart';
import 'package:flutter_html/flutter_html.dart';
import 'package:librarian/cubits/item/ItemCubit.dart';
import '../../config/Constants.dart';
import 'screens/[tableCamelCaseCapsSingular]Help.dart';

class [tableCamelCaseCapsSingular]HelpScreen extends StatelessWidget {
  final String title = [tableCamelCaseCapsSingular]Help.title;
  final String helpText = [tableCamelCaseCapsSingular]Help.helpText;
  @override
  Widget build(BuildContext context) {
    final deviceSize = MediaQuery.of(context).size;
    return Column(
      children: <Widget>[
        Row(
          children: [
            backButton(context),
            Text(title, style: Constants.textStyleFontSize20),
          ],
        ),
        Container(
          height: deviceSize.height - 175,
          child: SingleChildScrollView(
            child: Padding(
              padding: const EdgeInsets.all(8.0),
              child: Html(
                data: helpText,
              ),
            ),
          ),
        ),
      ],
    );
  }

  Widget backButton(BuildContext context) {
    return IconButton(
      icon: FaIcon(FontAwesomeIcons.arrowLeft, color: Colors.grey, size: 20),
      onPressed: () async {
        // RouterBloc.setActivity(Activity.BrgySelect);
        ItemCubit.cancelEdit(context);
      },
    );
  }
}
