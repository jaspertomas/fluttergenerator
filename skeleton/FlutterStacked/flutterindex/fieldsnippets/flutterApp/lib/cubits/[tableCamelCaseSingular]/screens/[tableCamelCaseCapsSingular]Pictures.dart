import 'package:flutter/material.dart';
import 'package:librarian/cubits/home/HomeCubit.dart';
import 'dart:io';
import '../[tableCamelCaseCapsSingular]ViewImageInput.dart';
import '../[tableCamelCaseCapsSingular]Api.dart';
import '../[tableCamelCaseCapsSingular]Cubit.dart';
import '../../item/ItemCubit.dart';
import '../../../utils/DialogHelper.dart';
import '../../../utils/[fieldCamelCaseCapsSingular]Helper.dart';
import '../../../config/Constants.dart';

class [tableCamelCaseCapsSingular]Pictures extends StatefulWidget {
  const [tableCamelCaseCapsSingular]Pictures({Key? key}) : super(key: key);

  @override
  _[tableCamelCaseCapsSingular]PicturesState createState() => _[tableCamelCaseCapsSingular]PicturesState();
}

class _[tableCamelCaseCapsSingular]PicturesState extends State<[tableCamelCaseCapsSingular]Pictures> {
  bool isActionLoading = false;
  bool isImageLoading = false;
  bool isLoading = true;
  bool isInit = false;
  List<dynamic> items = [];

  @override
  void didChangeDependencies() async {
    if (!isInit) {
      items = await [tableCamelCaseCapsSingular]Cubit.loadPictureList(context);
      setState(() {
        isLoading = false;
        isInit = true;
      });
    }
    super.didChangeDependencies();
  }

  @override
  Widget build(BuildContext context) {
    var previousImages = items.map((item) => pictureItem(item)).toList();
    return Expanded(
      child: Column(
        children: <Widget>[
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: <Widget>[
              [tableCamelCaseCapsSingular]ViewImageInput(
                fileHandler: fileHandler,
              ),
              IconButton(
                icon: Icon(Icons.refresh, color: Colors.blue, size: 30),
                onPressed: () async {
                  setState(() {
                    isLoading = true;
                  });
                  items = await [tableCamelCaseCapsSingular]Cubit.loadPictureList(context);
                  setState(() {
                    isLoading = false;
                  });
                },
              ),
            ],
          ),
          Expanded(
            child: isLoading
                ? Center(child: CircularProgressIndicator())
                : SingleChildScrollView(
                    child: Column(
                      children: <Widget>[
                        // imagePictureFrame(),
                        isImageLoading
                            ? Center(child: CircularProgressIndicator())
                            : SizedBox(height: 0),
                        ...previousImages
                      ],
                    ),
                  ),
          )
        ],
      ),
    );
  }

  Future<void> fileHandler(File picture) async {
    setState(() {
      isImageLoading = true;
    });
    try {
      //data contains image_url
      final data = await [tableCamelCaseCapsSingular]Api.uploadImage(
        picture,
        ItemCubit.item!["id"],
        HomeCubit.instance.token,
      );

      if (data != null) {
        //save image url to bloc to be displayed
        ItemCubit.item!["image_url"] = data["image_url"];
      }
      items = await [tableCamelCaseCapsSingular]Cubit.loadPictureList(context);
    } on Exception catch (e) {
      DialogHelper.exceptionDialog(e, context);
    }
    setState(() {
      isImageLoading = false;
    });
  }

  Widget imagePictureFrame() {
    if (ItemCubit.item == null)
      return SizedBox(
        height: 0,
      );
    final deviceSize = MediaQuery.of(context).size;

    String? imageUrl = ItemCubit.item!["image_url"];
    return Container(
      width: (deviceSize.width) / 8 * 7,
      height: deviceSize.height - 350,
      decoration: BoxDecoration(
        border: Border.all(width: 1, color: Colors.grey),
      ),
      alignment: Alignment.center,
      child: isImageLoading
          ? Center(child: CircularProgressIndicator())
          : imageUrl != null && imageUrl.isNotEmpty
              ? Image.network(
                  "${Constants.serverUrl}/$imageUrl",
                  width: deviceSize.width / 8 * 7,
                  fit: BoxFit.contain,
                )
              : Text(
                  'No Image Taken',
                  textAlign: TextAlign.center,
                ),
    );
  }

  Widget pictureItem(Map<String, dynamic> item) {
    return Card(
      child: ListTile(
        title: Text(
          [fieldCamelCaseCapsSingular]Helper.toPretty[fieldCamelCaseCapsSingular]Time([fieldCamelCaseCapsSingular]Helper.fromZulu(item["[fieldCamelCaseSingular]"])) ?? '',
          style: Constants.textStyleFontSize20,
        ),
        subtitle: Image.network(
          "${Constants.serverUrl}/${item["url"]}",
          fit: BoxFit.contain,
        ),

        trailing: isActionLoading
            ? CircularProgressIndicator()
            : TextButton(
                child: Icon(Icons.delete),
                onPressed: () async {
                  setState(() {
                    isActionLoading = true;
                  });
                  /*
                  var reason = await DialogHelper.inputDialog(
                      "Please give reason for deleting this image",
                      "",
                      context);
                  if (reason != null) {
                    if (reason.isEmpty) {
                      DialogHelper.messageDialog("Reason is required", context);
                    } else {
                      await [tableCamelCaseCapsSingular]Cubit.deleteImage(item, reason, context);
                      items = await [tableCamelCaseCapsSingular]Cubit.loadPictureList(context);
                      ItemCubit.item["image_url"] =
                          items.isEmpty ? '' : items[0]["url"];
                    }
                  }
                  */
                  final confirm = await DialogHelper.okCancelDialog(
                      "Delete picture: Are you sure?", context);
                  if (confirm) {
                    await [tableCamelCaseCapsSingular]Cubit.deleteImage(item, "", context);
                    items = await [tableCamelCaseCapsSingular]Cubit.loadPictureList(context);
                    ItemCubit.item!["image_url"] =
                        items.isEmpty ? '' : items[0]["url"];
                  }
                  setState(() {
                    isActionLoading = false;
                  });
                },
              ),
        // isThreeLine: true,
      ),
    );
  }
}
