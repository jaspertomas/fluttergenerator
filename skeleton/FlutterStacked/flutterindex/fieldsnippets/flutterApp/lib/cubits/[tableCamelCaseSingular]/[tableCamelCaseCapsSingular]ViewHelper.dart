import 'dart:io';
import 'dart:convert';
import 'package:http/http.dart' as http;
import 'package:librarian/cubits/item/ItemCubit.dart';

import '../../config/Constants.dart';
import '../../utils/HttpException.dart';
import '../../utils/HttpHelper.dart';

class [tableCamelCaseCapsSingular]Api {
  //----------UPLOAD IMAGE--------
  //on success, this returns the picture's url so it can be displayed on screen
  static Future<Map<String, dynamic>?> uploadImage(
      File? file, String? [tableCamelCaseSingular]Id, String authToken) async {
    if (file == null) return null;
    String fileName = file.path.split("/").last;

    if (ItemCubit.checkLastClick()) return null;
    final url = '${Constants.serverUrl}/v1/[tableCodeCase]/image/$authToken';

    var request = http.MultipartRequest("POST", Uri.parse(url));
    //add text fields
    request.fields["filename"] = fileName;
    request.fields["id"] = [tableCamelCaseSingular]Id ?? '';
    //create multipart using filepath, string or bytes
    var pic = http.MultipartFile.fromBytes("image", file.readAsBytesSync());
    //add multipart to request
    request.files.add(pic);

    var response = await request.send();

    if (response.[fieldCamelCaseSingular]Code == 404) {
      throw new HttpException("Server Error");
    } else if (response.[fieldCamelCaseSingular]Code == 200) {
      //-------------SUCCESS--------------
      var responseData = await response.stream.toBytes();
      var responseString = String.fromCharCodes(responseData);
      var result = json.decode(responseString);
      // print("response ${result}");
      if (result["success"])
        //result["data"] contains 2 things: image_url and setup_[fieldCamelCaseSingular]
        return result["data"];
      else
        throw new HttpException(result["message"]);
    } else {
      String message =
          "Error: ${response.[fieldCamelCaseSingular]Code.toString()}: ${response.toString()}";
      // print(message);
      throw new HttpException(message);
    }
  }

  static load[tableCamelCaseCapsSingular]Pictures(
      String parentType, String parentId, String authToken) async {
    if (ItemCubit.checkLastClick()) return null;
    final url =
        '${Constants.serverUrl}/v1/att/list/$parentType/$parentId/$authToken';
    final result = await HttpHelper.get(url);
    if (result["success"])
      return result['data']["items"];
    else {
      throw new HttpException("${result['message']}");
    }
  }

  static deleteImage(item, String reason, String authToken) async {
    if (ItemCubit.checkLastClick()) return null;
    final url =
        '${Constants.serverUrl}/v1/att/update/${item["id"]}/$authToken';
    final result =
        await HttpHelper.post(url, {"is_published": "false", "reason": reason});
    if (result["success"])
      return result['data']["items"];
    else {
      throw new HttpException("${result['message']}");
    }
  }

  static Future<List<dynamic>> load[tableCamelCaseCapsSingular]History(
      String parentType, String parentId, int page, String authToken) async {
    if (ItemCubit.checkLastClick()) return [];
    final url =
        '${Constants.serverUrl}/v1/his/list/${page.toString()}/$parentType/$parentId/$authToken';
    final result = await HttpHelper.get(url);
    if (result["success"])
      return result['data']["items"];
    else {
      throw new HttpException("${result['message']}");
    }
  }

/*
  static Future<List<dynamic>> load[tableCamelCaseCapsSingular]Petitions(
      String parentType, String parentId, int page, String authToken) async {
    if (ItemCubit.checkLastClick()) return null;
    final url =
        '${Constants.serverUrl}/v1/pet/list/x/${page.toString()}/$parentType/$parentId/$authToken';
    final result = await HttpHelper.get(url);
    if (result["success"])
      return result['data']["items"];
    else {
      throw new HttpException("${result['message']}");
    }
  }
*/
  static Future<List<dynamic>> load[tableCamelCaseCapsSingular][subtableCamelCaseCapsPlural](
      String parentId, String authToken) async {
    if (ItemCubit.checkLastClick()) return [];
    final url =
        '${Constants.serverUrl}/v1/[tableCodeCase]/list_[subtableCodeCase]/$parentId/$authToken';
    final result = await HttpHelper.get(url);
    if (result["success"])
      return result['data']["items"] ?? [];
    else {
      throw new HttpException("${result['message']}");
    }
  }

/*
  static Future<bool> delete(item, String reason, String authToken) async {
    if (ItemCubit.checkLastClick()) return null;
    final url =
        '${Constants.serverUrl}/v1/c/publish/${item["id"]}/$authToken';
    final result = await HttpHelper.post(
        url, {"is_published": "false", "reason": "$reason"});
    if (result["success"])
      return true;
    else {
      throw new HttpException("${result['message']}");
    }
  }

  static Future<bool> undelete(item, String authToken) async {
    if (ItemCubit.checkLastClick()) return null;
    final url =
        '${Constants.serverUrl}/v1/c/publish/${item["id"]}/$authToken';
    final result =
        await HttpHelper.post(url, {"is_published": "true", "reason": ""});
    if (result["success"])
      return true;
    else {
      throw new HttpException("${result['message']}");
    }
  }
*/
  static Future<bool> addHistory(
      String [tableCamelCaseSingular]Id, String update, String authToken) async {
    Map<String, String> body = {
      "id": [tableCamelCaseSingular]Id,
      "description": update,
    };
    if (ItemCubit.checkLastClick()) return false;
    final url =
        '${Constants.serverUrl}/v1/[tableCodeCase]/add_history/$[tableCamelCaseSingular]Id/$authToken';
    final result = await HttpHelper.post(url, body);
    if (result["success"])
      return true;
    else {
      throw new HttpException("${result['message']}");
    }
  }

  static Future<bool?> receiveAll(item, String authToken) async {
    if (ItemCubit.checkLastClick()) return null;
    final url =
        '${Constants.serverUrl}/v1/[tableCodeCase]/receive_all/$authToken';
    final result = await HttpHelper.post(url, {"id": item["id"]});
    if (result["success"])
      return true;
    else {
      throw new HttpException("${result['message']}");
    }
  }

  static Future<bool?> updateDetailReceiveQty(
      String detailId, int qty, String authToken) async {
    if (ItemCubit.checkLastClick()) return null;
    final url =
        '${Constants.serverUrl}/v1/[tableCodeCase]/update_[subtableCodeCase]/$authToken';
    final result = await HttpHelper.post(
        url, {"id": detailId, "qty_received": qty.toString()});
    if (result["success"])
      return true;
    else {
      throw new HttpException("${result['message']}");
    }
  }
}
