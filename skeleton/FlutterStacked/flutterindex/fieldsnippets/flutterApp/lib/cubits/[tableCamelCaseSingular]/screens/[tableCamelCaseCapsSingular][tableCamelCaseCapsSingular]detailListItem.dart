import 'package:flutter/material.dart';
import 'package:librarian/cubits/home/HomeCubit.dart';
// import 'package:provider/provider.dart';
// import '../../item/ItemCubit.dart';import '../../session_bloc/session_bloc.dart';
// import '../../[tableCamelCaseCapsSingular]detailViewBloc/[tableCamelCaseCapsSingular]detailViewBloc.dart';
// import '../[tableCamelCaseCapsSingular]Cubit.dart';
// import '../../../utils/NumberHelper.dart';
import '../../../utils/DialogHelper.dart';
import '../../../config/Constants.dart';
import '../../../models/[tableCamelCaseCapsSingular]Detail.dart';
import '../[tableCamelCaseCapsSingular]Api.dart';

class [tableCamelCaseCapsSingular][tableCamelCaseCapsSingular]detailListItem extends StatefulWidget {
  final Map<String, dynamic> item;
  // final [tableCamelCaseCapsSingular]Cubit [tableCamelCaseCapsSingular]Cubit;

  [tableCamelCaseCapsSingular][tableCamelCaseCapsSingular]detailListItem(this.item);

  @override
  _[tableCamelCaseCapsSingular][tableCamelCaseCapsSingular]detailListItemState createState() =>
      _[tableCamelCaseCapsSingular][tableCamelCaseCapsSingular]detailListItemState();

  static const List<int> widths = [2, 6, 2];
  static headers() {
    return Card(
        child: Row(children: [
      Expanded(
          flex: widths[0],
          child: Text("Qty", style: Constants.textStyleFontSize18)),
      Expanded(
          flex: widths[1],
          child: Text("Item", style: Constants.textStyleFontSize18)),
      Expanded(
          flex: widths[2],
          child: Text("Received", style: Constants.textStyleFontSize18)),
      // Expanded(
      //     flex: widths[3],
      //     child: Text("[fieldCamelCaseCapsSingular]", style: Constants.textStyleFontSize18)),
    ]));
  }
}

class _[tableCamelCaseCapsSingular][tableCamelCaseCapsSingular]detailListItemState
    extends State<[tableCamelCaseCapsSingular][tableCamelCaseCapsSingular]detailListItem> {
  bool isActionLoading = false;
  bool isLoading = false;

  // RouterBloc routerBloc;
  void select([tableCamelCaseCapsSingular]Detail [tableCamelCaseSingular]detail) async {
    final result = await DialogHelper.inputDialog(
        "Received: ${1}", "Enter new qty", context,
        textInputType: TextInputType.numberWithOptions(decimal: false));

    //if cancelled
    if (result.isEmpty) return;

    int qty = int.parse(result);

    //vali[fieldCamelCaseSingular] not negative
    if (qty < 0 || qty > double.parse([tableCamelCaseSingular]detail.qty)) {
      DialogHelper.messageDialog("Invalid qty", context);
    }

    try {
      final success = await [tableCamelCaseCapsSingular]Api.updateDetailReceiveQty(
          [tableCamelCaseSingular]detail.id, qty, HomeCubit.instance.token);
      if (success ?? false) {
        setState(() {
          [tableCamelCaseSingular]detail.qtyReceived = qty.toString();
        });
        DialogHelper.messageDialog("Receive qty updated", context);
      }
    } on Exception catch (e) {
      DialogHelper.exceptionDialog(e, context);
    }
  }

  @override
  Widget build(BuildContext context) {
    [tableCamelCaseCapsSingular]Detail [tableCamelCaseSingular]detail = [tableCamelCaseCapsSingular]Detail(widget.item);
    return Card(
      child: InkWell(
        onTap: () => select([tableCamelCaseSingular]detail),
        child: Row(
          children: [
            Expanded(
                flex: [tableCamelCaseCapsSingular][tableCamelCaseCapsSingular]detailListItem.widths[0],
                child: Text([tableCamelCaseSingular]detail.qty,
                    style: Constants.textStyleFontSize18)),
            Expanded(
                flex: [tableCamelCaseCapsSingular][tableCamelCaseCapsSingular]detailListItem.widths[1],
                child: Text([tableCamelCaseSingular]detail.product,
                    style: Constants.textStyleFontSize18)),
            Expanded(
                flex: [tableCamelCaseCapsSingular][tableCamelCaseCapsSingular]detailListItem.widths[0],
                child: Text([tableCamelCaseSingular]detail.qtyReceived,
                    style: Constants.textStyleFontSize18)),
          ],
        ),
      ),
    );
  }
}
