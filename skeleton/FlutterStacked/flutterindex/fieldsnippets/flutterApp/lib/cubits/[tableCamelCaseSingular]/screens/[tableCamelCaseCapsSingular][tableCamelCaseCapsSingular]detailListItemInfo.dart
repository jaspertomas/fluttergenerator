import 'package:flutter/material.dart';
// import 'package:provider/provider.dart';
// import '../../item/ItemCubit.dart';// import '../../[tableCamelCaseCapsSingular]detailViewBloc/[tableCamelCaseCapsSingular]detailViewBloc.dart';
// import '../[tableCamelCaseCapsSingular]Cubit.dart';
// import '../../../utils/DialogHelper.dart';
import '../../../config/Constants.dart';
import '../../../models/[tableCamelCaseCapsSingular]Detail.dart';
import '../../../utils/NumberHelper.dart';

class [tableCamelCaseCapsSingular][tableCamelCaseCapsSingular]detailListItemInfo extends StatefulWidget {
  final Map<String, dynamic> item;
  // final [tableCamelCaseCapsSingular]Cubit [tableCamelCaseCapsSingular]Cubit;

  [tableCamelCaseCapsSingular][tableCamelCaseCapsSingular]detailListItemInfo(this.item);

  @override
  _[tableCamelCaseCapsSingular][tableCamelCaseCapsSingular]detailListItemInfoState createState() =>
      _[tableCamelCaseCapsSingular][tableCamelCaseCapsSingular]detailListItemInfoState();

  static const List<int> widths = [2, 6, 3, 3];
  static headers() {
    return Card(
        child: Row(children: [
      Expanded(
          flex: widths[0],
          child: Text("Qty", style: Constants.textStyleFontSize18)),
      Expanded(
          flex: widths[1],
          child: Text("Item", style: Constants.textStyleFontSize18)),
      Expanded(
          flex: widths[2],
          child: Text("Price", style: Constants.textStyleFontSize18)),
      Expanded(
          flex: widths[3],
          child: Text("[fieldCamelCaseCapsSingular]", style: Constants.textStyleFontSize18)),
    ]));
  }
}

class _[tableCamelCaseCapsSingular][tableCamelCaseCapsSingular]detailListItemInfoState
    extends State<[tableCamelCaseCapsSingular][tableCamelCaseCapsSingular]detailListItemInfo> {
  bool isActionLoading = false;
  bool isLoading = false;

/*
  // RouterBloc routerBloc;
  void select() async {
    // setState(() {
    //   isLoading = true;
    // });
    [tableCamelCaseCapsSingular]detailViewBloc.mode = [tableCamelCaseCapsSingular]detailViewBloc.INFO;
    RouterBloc.setItem(widget.item);
    RouterBloc.setActivity(Activity.[tableCamelCaseCapsSingular]detailView);
    // setState(() {
    //   isLoading = true;
    // });
  }
*/
  @override
  Widget build(BuildContext context) {
    [tableCamelCaseCapsSingular]Detail [tableCamelCaseSingular]detail = [tableCamelCaseCapsSingular]Detail(widget.item);
    return Card(
      child: Row(
        children: [
          Expanded(
              flex: [tableCamelCaseCapsSingular][tableCamelCaseCapsSingular]detailListItemInfo.widths[0],
              child: Text([tableCamelCaseSingular]detail.qty,
                  style: Constants.textStyleFontSize18)),
          Expanded(
              flex: [tableCamelCaseCapsSingular][tableCamelCaseCapsSingular]detailListItemInfo.widths[1],
              child: Text([tableCamelCaseSingular]detail.product,
                  style: Constants.textStyleFontSize18)),
          Expanded(
              flex: [tableCamelCaseCapsSingular][tableCamelCaseCapsSingular]detailListItemInfo.widths[2],
              child: Text(
                  NumberHelper.decimalFormat(
                      double.parse([tableCamelCaseSingular]detail.price)),
                  style: Constants.textStyleFontSize18)),
          Expanded(
              flex: [tableCamelCaseCapsSingular][tableCamelCaseCapsSingular]detailListItemInfo.widths[3],
              child: Text(
                  NumberHelper.decimalFormat(
                      double.parse([tableCamelCaseSingular]detail.[fieldCamelCaseSingular])),
                  style: Constants.textStyleFontSize18)),
        ],
      ),
    );
  }
}
