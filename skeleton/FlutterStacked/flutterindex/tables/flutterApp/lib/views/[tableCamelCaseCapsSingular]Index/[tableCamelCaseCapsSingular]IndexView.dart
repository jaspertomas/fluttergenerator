import 'package:flutter/material.dart';
import 'package:flutterApp/widgets/LayoutTemplate/LayoutTemplate.dart';
import 'package:stacked/stacked.dart';
import '[tableCamelCaseCapsSingular]IndexBaseView.dart';
import '[tableCamelCaseCapsSingular]IndexViewModel.dart';

class [tableCamelCaseCapsSingular]IndexView extends [tableCamelCaseCapsSingular]IndexBaseView {
  @override
  Widget build(BuildContext context) {
    final deviceSize = MediaQuery.of(context).size;
    return LayoutTemplate(
      child: ViewModelBuilder<[tableCamelCaseCapsSingular]IndexViewModel>.reactive(
        viewModelBuilder: () => [tableCamelCaseCapsSingular]IndexViewModel(),
        onModelReady: (model) async {
          await model.load(context);
        },
        builder: (context, model, child) => Center(
          child: Row(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Expanded(
                flex: 3,
                child: recordList(context, model, deviceSize.height),
              ),
              Expanded(
                flex: 1,
                child: searchFilters(context, model, deviceSize.height),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
