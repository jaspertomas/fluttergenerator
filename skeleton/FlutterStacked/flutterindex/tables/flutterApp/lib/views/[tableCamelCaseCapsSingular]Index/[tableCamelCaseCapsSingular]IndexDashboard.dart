import 'package:flutter/material.dart';
import '../../config/Constants.dart';
// import 'package:flutterApp/config/locator.dart';
// import 'package:flutterApp/services/PickerService.dart';
// import 'package:flutterApp/widgets/DashboardTools.dart';

import '[tableCamelCaseCapsSingular]IndexViewModel.dart';

class [tableCamelCaseCapsSingular]IndexDashboard extends StatelessWidget {
  final [tableCamelCaseCapsSingular]IndexViewModel model;
  const [tableCamelCaseCapsSingular]IndexDashboard(this.model, {Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Text(
      "[tableCamelCaseCapsSingular] Index",
      // textAlign: TextAlign.start,
      style: Constants.textStyleFontSize20Bold,
    );
  }
}
