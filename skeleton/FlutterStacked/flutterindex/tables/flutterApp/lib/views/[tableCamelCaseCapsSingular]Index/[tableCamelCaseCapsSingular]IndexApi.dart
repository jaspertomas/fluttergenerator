import '../../config/locator.dart';
import '../../config/Constants.dart';
import '../../utils/Exceptions.dart';
import '../../utils/HttpHelper.dart';
import '../../services/SessionService.dart';

class [tableCamelCaseCapsSingular]IndexApi {
  static final _sessionService = locator<SessionService>();
  static Future<Map<String, dynamic>> select(
      Map<String, String> searchStrings, int page) async {
    final url = '${Constants.serverUrl}/v1/[tableCodeCase]/select/${_sessionService.token}';
    final body = {
      "search_strings": searchStrings,
      "page": page.toString(),
      "token": _sessionService.token,
    };
    final result = await HttpHelper.post(url, body);
    if (result["success"]) {
      return result['data'];
    } else {
      throw new HttpException("${result['message']}");
    }
  }

  static Future<Map<String, dynamic>> save(Map<String, dynamic> data) async {
    //todo: check last click
    final url = '${Constants.serverUrl}/v1/[tableCodeCase]/update/${_sessionService.token}';
    final result = await HttpHelper.post(url, data);
    if (result["success"])
      return result['data'];
    else {
      print('${result['message']}');
      throw new HttpException("${result['message']}");
    }
  }
  static Future<Map<String, dynamic>> delete(Map<String, dynamic> data) async {
    //todo: check last click
    final url = '${Constants.serverUrl}/v1/[tableCodeCase]/delete/${_sessionService.token}';
    final result = await HttpHelper.post(url, {"id": data["id"]});
    if (result["success"])
      return result['data'];
    else {
      print('${result['message']}');
      throw new HttpException("${result['message']}");
    }
  }

  static Future<Map<String, dynamic>> massDelete(List<String> ids) async {
    final url = '${Constants.serverUrl}/v1/[tableCodeCase]/mass_delete/${_sessionService.token}';
    final body = {
      "ids": ids,
    };
    final result = await HttpHelper.post(url, body);
    if (result["success"]) {
      return result['data'];
    } else {
      throw new HttpException("${result['message']}");
    }
  }
}
