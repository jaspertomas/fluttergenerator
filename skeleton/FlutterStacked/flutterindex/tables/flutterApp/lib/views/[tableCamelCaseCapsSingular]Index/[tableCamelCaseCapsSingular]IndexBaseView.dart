import 'package:flutter/material.dart';
import 'package:stacked_services/stacked_services.dart';
import 'package:stacked/stacked.dart';
import '../../utils/RegExInputFormatter.dart';
// ignore: unused_import
import '../../utils/DateHelper.dart';
import '../../utils/DialogHelper.dart';
import '../../config/Constants.dart';
import '../../config/locator.dart';
// ignore: unused_import
import '../../models/[tableCamelCaseCapsSingular].dart';
import '../../widgets/LayoutTemplate/LayoutTemplate.dart';
// ignore: unused_import
import '../../widgets/DashboardTools.dart';
import '../../services/SearchPicker/SearchPickerService.dart';
import '[tableCamelCaseCapsSingular]IndexDashboard.dart';
import '[tableCamelCaseCapsSingular]IndexItemView.dart';
import '[tableCamelCaseCapsSingular]IndexBaseViewModel.dart';
//[supertable_imports]

class [tableCamelCaseCapsSingular]IndexBaseView extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final deviceSize = MediaQuery.of(context).size;
    return LayoutTemplate(
      child: ViewModelBuilder<[tableCamelCaseCapsSingular]IndexBaseViewModel>.reactive(
        viewModelBuilder: () => [tableCamelCaseCapsSingular]IndexBaseViewModel(),
        onModelReady: (model) async {
          await model.load(context);
        },
        builder: (context, model, child) => Center(
          child: Row(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Expanded(
                flex: 3,
                child: recordList(context, model, deviceSize.height),
              ),
              Expanded(
                flex: 1,
                child: searchFilters(context, model, deviceSize.height),
              ),
            ],
          ),
        ),
      ),
    );
  }

  Widget recordList(BuildContext context, [tableCamelCaseCapsSingular]IndexBaseViewModel model,
      double deviceSizeHeight) {
    var windowHeight = deviceSizeHeight - 247;

    final _scrollController = ScrollController();
    //on scroll to bottom, load more records
    _scrollController.addListener(() {
      if (_scrollController.offset >=
              _scrollController.position.maxScrollExtent &&
          !_scrollController.position.outOfRange) {
        model.loadMore(context);
      }
    });

    return Column(
      mainAxisAlignment: MainAxisAlignment.start,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        [tableCamelCaseCapsSingular]IndexDashboard(model),
        Row(
          children: [
            addItemButton(context, model),
            SizedBox(width: 3),
            massDeleteButton(context, model),
          ],
        ),
        [tableCamelCaseCapsSingular]IndexItemView.headers(),
        Container(
          height: windowHeight,
          child: SingleChildScrollView(
            controller: _scrollController,
            child: Column(
              children: model.details.length == 0
                ? <Widget>[
                    Text("No items found", style: const TextStyle(fontSize: 21))
                  ]
                : <Widget>[
                    ...model.details
                        .map((item) => [tableCamelCaseCapsSingular]IndexItemView(model, item))
                        .toList(),
                    ...[
                      model.isBusy
                          ? CircularProgressIndicator()
                          : SizedBox(height: 0),
                    ]
                  ],
            ),
          ),
        ),
          // submitButton(model, context),
          // cancelButton(),
      ],
    );
  }

  Widget searchFilters(BuildContext context, [tableCamelCaseCapsSingular]IndexBaseViewModel model,
      double deviceSizeHeight) {
    var windowHeight = deviceSizeHeight - 185;
    final widths = [2, 3];
    return Column(
      children: [
        searchButton(model, context),
        Container(
          height: windowHeight,
          child: ListView(
            children: [
//[fields_search_filters]
            ],
          ),
        ),
      ],
    );
  }
  
//[fields_code]

  Widget searchButton([tableCamelCaseCapsSingular]IndexBaseViewModel model, BuildContext context) {
    return model.isBusy
        ? CircularProgressIndicator()
        : RaisedButton(
            child: Text('Search'),
            color: Colors.blue,
            onPressed: () async {
              await model.load(context);
            },
          );
  }

  submitPicker() {
  }

  addItemButton(BuildContext context, [tableCamelCaseCapsSingular]IndexBaseViewModel model) {
    return RaisedButton(
      child: Text('Add Item'),
      color: Colors.blue,
      onPressed: () {
        locator<NavigationService>().navigateTo("/[tableCamelCaseSingular]/new");
      },
    );
  }

  massDeleteButton(BuildContext context, [tableCamelCaseCapsSingular]IndexBaseViewModel model) {
    return RaisedButton(
      child: Text('Delete'),
      color: Colors.blue,
      onPressed: () async {
        final list = model.getChecked();
        final count = list.length;
        //if selected count == 0, error
        if (count == 0)
          DialogHelper.messageDialog("No records selected", context);
        else {
          final confirm = await DialogHelper.okCancelDialog(
              "Delete $count item${count > 1 ? "s" : ""}: Are you sure?",
              context);
          if (confirm) model.massDelete(context);
        }
        //ask for confirmation, are you sure
        //delete selected items
      },
    );
  }

  cancelButton() {
    return RaisedButton(
      child: Text('Reset'),
      color: Colors.blue,
      onPressed: () {
        locator<NavigationService>().navigateTo("/mainmenu");
      },
    );
  }
}
