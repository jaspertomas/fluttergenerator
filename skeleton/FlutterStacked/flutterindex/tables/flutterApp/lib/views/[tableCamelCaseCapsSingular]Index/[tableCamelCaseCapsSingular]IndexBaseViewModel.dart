import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:stacked/stacked.dart';
//ignore: unused_import
import 'package:decimal/decimal.dart';
import '../../models/[tableCamelCaseCapsSingular].dart';
import '../../utils/DialogHelper.dart';
import '[tableCamelCaseCapsSingular]IndexApi.dart';
//[supertable_imports]

class [tableCamelCaseCapsSingular]IndexBaseViewModel extends BaseViewModel {
  [tableCamelCaseCapsSingular]IndexBaseViewModel();

  // Customer get customer => parent.customer;
  List<[tableCamelCaseCapsSingular]> _details = [];
  List<[tableCamelCaseCapsSingular]> get details => _details;

  Map<String, bool> _checks = {};
  Map<String, bool> get checks => _checks;
  bool getCheck(String id) {
    return _checks[id] ?? false;
  }

  setCheck(String id, bool value) {
    _checks[id] = value;
  }

  //this takes the id of all checked records and puts them into a list
  List<String> getChecked() {
    List<String> output = [];
    _checks.forEach((id, checked) {
      if (checked) output.add(id);
    });
    return output;
  }

  Map<String, String> _filter = {"type": "[tableCodeCase]"};
  Map<String, String> get filter => _filter;
  int _page = 1;
  int get page => _page;
  bool _isBusy = true;
  bool get isBusy => _isBusy;

  load(BuildContext context) async {
    //todo: checkLastClick
    _page = 1;
    _isBusy = true;
    notifyListeners();

    final resultDetails =
        await [tableCamelCaseCapsSingular]IndexApi.select(filter, 1);
    List resultDetailsItems = resultDetails["items"];
    _details = [];
    _checks = {};
    resultDetailsItems.forEach((element) {
      [tableCamelCaseCapsSingular] [tableCamelCaseSingular] = [tableCamelCaseCapsSingular](element);
      _details.add([tableCamelCaseSingular]);
      _checks[[tableCamelCaseSingular].id] = false;
    });

    _isBusy = false;

    notifyListeners();
  }

  loadMore(BuildContext context) async {
    //todo: checkLastClick
    _page += 1;
    _isBusy = true;
    notifyListeners();

    final resultDetails =
        await [tableCamelCaseCapsSingular]IndexApi.select(filter, _page);
    List resultDetailsItems = resultDetails["items"];
    resultDetailsItems.forEach((element) {
      [tableCamelCaseCapsSingular] [tableCamelCaseSingular] = [tableCamelCaseCapsSingular](element);
      _details.add([tableCamelCaseSingular]);
      _checks[[tableCamelCaseSingular].id] = false;
    });

    _isBusy = false;

    notifyListeners();
  }

  //this is called by ItemViewModel
  delete(String id) async {
    details.removeWhere((element) => element.id == id);
    notifyListeners();
  }

  massDelete(BuildContext context) async {
    _isBusy = true;
    notifyListeners();

    try {
      await [tableCamelCaseCapsSingular]IndexApi.massDelete(getChecked());
      DialogHelper.messageDialog("Delete successful", context);
      load(context);
    } on Exception catch (e) {
      DialogHelper.exceptionDialog(e, context);
    }

    _isBusy = false;
    notifyListeners();
  }

//[fields_code]
}
