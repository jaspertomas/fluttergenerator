import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:stacked/stacked.dart';
import 'package:stacked_services/stacked_services.dart';
import '../../config/locator.dart';
import '../../config/Constants.dart';
// import '../../utils/DialogHelper.dart';
import '../../models/[tableCamelCaseCapsSingular].dart';
import '[tableCamelCaseCapsSingular]IndexItemViewModel.dart';
import '[tableCamelCaseCapsSingular]IndexViewModel.dart';

class [tableCamelCaseCapsSingular]IndexItemView extends StatelessWidget {
  final [tableCamelCaseCapsSingular]IndexViewModel [tableCamelCaseSingular]IndexViewModel; //parent model
  final [tableCamelCaseCapsSingular] [tableCamelCaseSingular];
  [tableCamelCaseCapsSingular]IndexItemView(this.[tableCamelCaseSingular]IndexViewModel, this.[tableCamelCaseSingular]);

  static List<int> widths = [3, 3, 1, 1, 1];

  static headers() {
    return Card(
        // color: Constants.white,
        child: Row(children: [
      Container(
        width: 50,
      ),
      Expanded(
          flex: widths[0],
          child: Text("Id", style: Constants.textStyleFontSize20)),
      Expanded(
          flex: widths[1],
          child: Text("[tableNameField]", style: Constants.textStyleFontSize20)),
      Container(
        width: 150,
      ),
    ]));
  }

  @override
  Widget build(BuildContext context) {
    return ViewModelBuilder<[tableCamelCaseCapsSingular]IndexItemViewModel>.reactive(
      viewModelBuilder: () =>
          [tableCamelCaseCapsSingular]IndexItemViewModel([tableCamelCaseSingular]IndexViewModel),
      // onModelReady: (model) async {},
      builder: (context, model, child) => Center(
        child: model.isBusy
            ? CircularProgressIndicator()
            : Card(
                // color: Constants.white,
                child: InkWell(
                  // onTap: () => select(item),
                  child: Row(
                    children: [
                      Container(
                        width: 50,
                        child: Checkbox(
                          value: model.getCheck([tableCamelCaseSingular]),
                          onChanged: (bool value) => model.setCheck([tableCamelCaseSingular], value),
                        ),
                      ),
                      Expanded(
                        flex: widths[0],
                        child: Text([tableCamelCaseSingular].id,
                            style: Constants.textStyleFontSize20),
                      ),
                      Expanded(
                        flex: widths[1],
                        child: Text([tableCamelCaseSingular].[tableNameField],
                            style: Constants.textStyleFontSize20),
                      ),
                      Container(
                        width: 150,
                        child: Row(
                          children: [
                            IconButton(
                              icon: FaIcon(FontAwesomeIcons.bookOpen,
                                  color: Colors.grey, size: 20),
                              onPressed: () async {
                                locator<NavigationService>().navigateTo("/[tableCamelCaseSingular]/view/${[tableCamelCaseSingular].id}");
                              },
                            ),
                            IconButton(
                              icon: FaIcon(FontAwesomeIcons.edit,
                                  color: Colors.grey, size: 20),
                              onPressed: () async {
                                locator<NavigationService>().navigateTo("/[tableCamelCaseSingular]/edit/${[tableCamelCaseSingular].id}");
                              },
                            ),
                            IconButton(
                              icon: FaIcon(FontAwesomeIcons.trashAlt,
                                  color: Colors.grey, size: 20),
                              onPressed: () async {
                                model.delete(context, [tableCamelCaseSingular]);
                              },
                            ),
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
              ),
      ),
    );
  }
}
