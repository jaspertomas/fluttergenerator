import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:stacked/stacked.dart';
// import 'package:decimal/decimal.dart';
import '../../utils/DialogHelper.dart';
import '../../models/[tableCamelCaseCapsSingular].dart';
// import '../../models/Customer.dart';
import '[tableCamelCaseCapsSingular]IndexApi.dart';
import '[tableCamelCaseCapsSingular]IndexViewModel.dart';

class [tableCamelCaseCapsSingular]IndexItemViewModel extends BaseViewModel {
  [tableCamelCaseCapsSingular]IndexViewModel parent;
  [tableCamelCaseCapsSingular]IndexItemViewModel(this.parent);

  bool getCheck([tableCamelCaseCapsSingular] [tableCamelCaseSingular]) => parent.getCheck([tableCamelCaseSingular].id);

  setCheck([tableCamelCaseCapsSingular] [tableCamelCaseSingular], bool value) {
    parent.setCheck([tableCamelCaseSingular].id, value);
    notifyListeners();
  }

  bool _isBusy = false;
  bool get isBusy => _isBusy;

  Future<bool> delete(BuildContext context, [tableCamelCaseCapsSingular] [tableCamelCaseSingular]) async {
    //todo: checkLastClick

    var confirm = await DialogHelper.okCancelDialog("Really delete ${[tableCamelCaseSingular].[tableNameField]}?", context);
    if (!confirm) return false;

    try {
      _isBusy = true;
      notifyListeners();

      //this deletes the item from the database
      final result = await [tableCamelCaseCapsSingular]IndexApi.delete([tableCamelCaseSingular].data);

      //if new, set id from result
      if ([tableCamelCaseSingular].isNew) [tableCamelCaseSingular].data["id"] = result["id"].toString();

      //this deletes the item from the list in memory
      parent.delete([tableCamelCaseSingular].id);

      DialogHelper.messageDialog("Delete successful", context);

      _isBusy = false;
      notifyListeners();
      return true;
    } on Exception catch (e) {
      DialogHelper.exceptionDialog(e, context);
      _isBusy = false;
      notifyListeners();
      return false;
    }
  }
}
