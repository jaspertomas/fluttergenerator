[fieldspec]
------
string
---
      '[fieldSnakeCaseSingular]': '',
===
boolean
---
      '[fieldSnakeCaseSingular]': false,
===
integer
---
      '[fieldSnakeCaseSingular]': 0,
===
decimal
---
      '[fieldSnakeCaseSingular]': Decimal.zero,
===
date
---
      '[fieldSnakeCaseSingular]': DateHelper.longTimeAgo,
===
datetime
---
      '[fieldSnakeCaseSingular]': DateHelper.longTimeAgo,
===
picker
---
      '[fieldSnakeCaseSingular]': '(Select One)',
      '[fieldSnakeCaseSingular]_id': '',
===
references
---
      '[fieldSnakeCaseSingular]': '(Select One)',
      '[fieldSnakeCaseSingular]_id': '',
======
[gettersandsetters]
------
string
---
  String get [fieldCamelCaseSingular] => _data["[fieldSnakeCaseSingular]"] ?? '';
  set [fieldCamelCaseSingular](String value) => _data["[fieldSnakeCaseSingular]"] = value;
===
picker
---
  String get [fieldCamelCaseSingular] => _data["[fieldSnakeCaseSingular]"] ?? '';
  set [fieldCamelCaseSingular](String value) => _data["[fieldSnakeCaseSingular]"] = value;
  String get [fieldCamelCaseSingular]Id => _data["[fieldSnakeCaseSingular]_id"].toString() ?? '';
  set [fieldCamelCaseSingular]Id(String value) => _data["[fieldSnakeCaseSingular]_id"] = value;
===
references
---
  String get [fieldCamelCaseSingular] => _data["[fieldSnakeCaseSingular]"] ?? '';
  set [fieldCamelCaseSingular](String value) => _data["[fieldSnakeCaseSingular]"] = value;
  String get [fieldCamelCaseSingular]Id => _data["[fieldSnakeCaseSingular]_id"].toString() ?? '';
  set [fieldCamelCaseSingular]Id(String value) => _data["[fieldSnakeCaseSingular]_id"] = value;
===
text
---
  String get [fieldCamelCaseSingular] => _data["[fieldSnakeCaseSingular]"] ?? '';
  set [fieldCamelCaseSingular](String value) => _data["[fieldSnakeCaseSingular]"] = value;
===
boolean
---
  bool get [fieldCamelCaseSingular] => _data["[fieldSnakeCaseSingular]"] ?? false;
  set [fieldCamelCaseSingular](bool value) => _data["[fieldSnakeCaseSingular]"] = value;
===
integer
---
  int get [fieldCamelCaseSingular] => _data["[fieldSnakeCaseSingular]"] ?? 0;
  set [fieldCamelCaseSingular](int value) => _data["[fieldSnakeCaseSingular]"] = value;
===
bigint
---
  int get [fieldCamelCaseSingular] => _data["[fieldSnakeCaseSingular]"] ?? 0;
  set [fieldCamelCaseSingular](int value) => _data["[fieldSnakeCaseSingular]"] = value;
===
decimal
---
  Decimal get [fieldCamelCaseSingular] {
    if (!(this._data["[fieldSnakeCaseSingular]"] is Decimal))
      _data["[fieldSnakeCaseSingular]"] = TypeHelper.toDecimal(_data["[fieldSnakeCaseSingular]"]);
    return _data["[fieldSnakeCaseSingular]"];
  }

  set [fieldCamelCaseSingular](Decimal value) => _data["[fieldSnakeCaseSingular]"] = value;
===
date
---
  DateTime get [fieldCamelCaseSingular] {
    if (!(this._data["[fieldSnakeCaseSingular]"] is DateTime))
      this._data["[fieldSnakeCaseSingular]"] = this._data["[fieldSnakeCaseSingular]"] == null
          ? DateHelper.longTimeAgo
          : DateTime.tryParse(this._data["[fieldSnakeCaseSingular]"]);
    return _data["[fieldSnakeCaseSingular]"];
  }

  String get [fieldCamelCaseSingular]String {
    return DateHelper.toShortDate(this.[fieldCamelCaseSingular]);
  }

  set [fieldCamelCaseSingular](DateTime value) => _data["[fieldSnakeCaseSingular]"] = value;
===
datetime
---
  DateTime get [fieldCamelCaseSingular] {
    if (!(this._data["[fieldSnakeCaseSingular]"] is DateTime))
      this._data["[fieldSnakeCaseSingular]"] = this._data["[fieldSnakeCaseSingular]"] == null
          ? DateHelper.longTimeAgo
          : DateTime.tryParse(this._data["[fieldSnakeCaseSingular]"]);
    return _data["[fieldSnakeCaseSingular]"];
  }

  String get [fieldCamelCaseSingular]String {
    return DateHelper.toShortDateTime(this.[fieldCamelCaseSingular]);
  }

  set [fieldCamelCaseSingular](DateTime value) => _data["[fieldSnakeCaseSingular]"] = value;
======