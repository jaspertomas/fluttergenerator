class ModelException implements Exception {
  final String message;

  ModelException(this.message);

  @override
  String toString() {
    return message;
    // return super.toString(); // Instance of HttpException
  }
}
