import './ModelException.dart';
import '../utils/DateHelper.dart';

class History {
  Map<String, dynamic> _item;
  History(this._item) {
    if (this._item["type"] != 'h') throw ModelException("Not a history");
  }
  Map<String, dynamic> get item => _item;
  String get description => _item["description"];

  String get username => item["created_by"];
  String get date =>
      DateHelper.toShortDate(DateHelper.fromSqlDate(item["created_at"]));
  // String get datetime =>
  //     DateHelper.toPrettyDateTime(DateHelper.fromZulu(item["datetime"]));
}
