import 'base/Base[tableCamelCaseCapsSingular].dart';

class [tableCamelCaseCapsSingular] extends Base[tableCamelCaseCapsSingular] {
  [tableCamelCaseCapsSingular](Map<String, dynamic> data) : super(data);
  [tableCamelCaseCapsSingular].create() : super.create();
  [tableCamelCaseCapsSingular].createFilter() : super.createFilter();
}
//[donotgenerate]
