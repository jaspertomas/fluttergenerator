// ignore: unused_import
import 'package:decimal/decimal.dart';
// ignore: unused_import
import '../../utils/DateHelper.dart';
// ignore: unused_import
import '../../utils/TypeHelper.dart';
import '../ModelException.dart';

class Base[tableCamelCaseCapsSingular] {
  late Map<String, dynamic> _data;
  Base[tableCamelCaseCapsSingular](this._data) {
    if (this._data["type"] != '[tableCodeCase]') throw ModelException("Not a [tableCamelCaseCapsSingular]");
  }
  Base[tableCamelCaseCapsSingular].create() {
    _data = {
      "type": "[tableCodeCase]",
      'id': null,
      'image_url': '',
[fieldspec]
    };
  }

  Base[tableCamelCaseCapsSingular].createFilter() {
    _data = {
      "type": "[tableCodeCase]",
    };
  }
  Map<String, dynamic> get data => _data;
  String get id => _data["id"] ?? '';
  bool get isNew => _data["id"] == null || _data["id"].isEmpty;
[gettersandsetters]
  String get createdAt => _data["created_at"] ?? '';
  set createdAt(String value) => _data["created_at"] = value;
  String get updatedAt => _data["updated_at"] ?? '';
  set updatedAt(String value) => _data["updated_at"] = value;

  bool get hasImage =>
      this._data["image_url"] != null && this._data["image_url"].isNotEmpty;

  String get imageUrl => this.hasImage ? this._data["image_url"] : null;
}
