import 'package:crypto/crypto.dart';
import 'dart:convert';

class EncryptionHelper {
  static String sha1Encode(String string) {
    if (string.isEmpty) return "";
    var bytes = utf8.encode(string); // data being hashed
    var digest = sha1.convert(bytes);
    return digest.toString();
  }
}
