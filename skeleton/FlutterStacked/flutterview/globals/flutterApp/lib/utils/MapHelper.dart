class MapHelper {
  static Map inverse(Map f) {
    return f.map((k, v) => MapEntry(v, k));
  }
}
