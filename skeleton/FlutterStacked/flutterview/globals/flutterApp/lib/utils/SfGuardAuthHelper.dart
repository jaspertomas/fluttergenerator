import '../config/Constants.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';
import './HttpException.dart';
import './HttpHelper.dart';

class SfGuardAuthHelper {
  //check if logged into server
  Future<Map<String, dynamic>> checkLoggedIn(String token) async {
    final url = '${Constants.serverUrl}/v1/users/check_login/$token';
    try {
      //check for internet connection
      if (!await HttpHelper.isConnectedToInternet()) {
        throw new HttpException("Please connect to the Internet");
      }

      final response = await http.get(Uri.parse(url));

      switch (response.statusCode) {
        case 404:
          print("404");
          throw new HttpException("Server Not Found");
        case 200:
          var result = json.decode(response.body);
          if (result["success"])
            return result['data'];
          else {
            print('Error logging into server: ${result['error']}');
            throw new HttpException("${result['error']}");
          }
        default:
          print("loginToServer fail");
          throw new HttpException(response.statusCode.toString());
      }
    } catch (error) {
      print(error);
      throw error;
    }
  }

  //--------LOGIN TO SERVER----------
  static Future<Map<String, dynamic>> loginToServer(
      String username, String password) async {
    final url = '${Constants.serverUrl}/v1/users/login';
    try {
      //check for internet connection
      if (!await HttpHelper.isConnectedToInternet()) {
        throw new HttpException("Please connect to the Internet");
      }

      final response = await http.post(
        Uri.parse(url),
        body: {
          'username': username,
          'password': password,
        },
      );

      switch (response.statusCode) {
        case 404:
          print("404");
          throw new HttpException("Server Not Found");
        case 200:
          var result = json.decode(response.body);
          if (result["success"])
            return result;
          else {
            print('Error logging into server: ${result['error']}');
            throw new HttpException("${result['error']}");
          }
        default:
          print("loginToServer fail");
          throw new HttpException(response.statusCode.toString());
      }
    } catch (error) {
      print(error.hashCode);
      print(error.runtimeType);
      print(error.toString());
      throw error;
    }
  }

  //----------LOGOUT--------
  Future<void> logOut() async {
    try {
      // await _auth.signOut();
    } catch (e) {
      print("error logging out");
    }
  }
}
