import 'package:intl/intl.dart';

class DateHelper {
  static final _zuluFormat = DateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'");
  static DateTime? fromZulu(String? dateString) {
    if (dateString == null || dateString.isEmpty) return null;
    return _zuluFormat.parse(dateString);
    // return DateTime.parse(dateString);
  }

  static String? toZulu(DateTime? dateTime) {
    if (dateTime == null) return null;
    return _zuluFormat.format(dateTime);
  }

  static final _sqlFormat = DateFormat("yyyy-MM-dd");
  static DateTime? fromSqlDate(String? dateString) {
    if (dateString == null || dateString.isEmpty) return null;
    return _sqlFormat.parse(dateString);
  }

  static String? toSqlDate(DateTime? dateTime) {
    if (dateTime == null) return null;
    return _sqlFormat.format(dateTime);
  }

  static final _sqlDateTimeFormat = DateFormat("yyyy-MM-dd HH:mm:ss");
  static DateTime? fromSqlDateTime(String? dateString) {
    if (dateString == null || dateString.isEmpty) return null;
    // if (!dateString.contains(' ')) return fromSqlDate(dateString);
    return _sqlDateTimeFormat.parse(dateString);
  }

  static String? toSqlDateTime(DateTime? dateTime) {
    if (dateTime == null) return null;
    return _sqlDateTimeFormat.format(dateTime);
  }

  static final _prettyDateTimeFormat = new DateFormat('MMMM d, yyyy, hh:mm a');
  // static final prettyDateTimeFormat = DateFormat.yMMMMd("en_US").add_jm();
  static String? toPrettyDateTime(DateTime? dateTime) {
    if (dateTime == null) return null;
    return _prettyDateTimeFormat.format(dateTime);
  }

  static final _prettyDateFormat = new DateFormat('MMMM d, yyyy ');
  static String? toPrettyDate(DateTime? dateTime) {
    if (dateTime == null) return null;
    return _prettyDateFormat.format(dateTime);
  }

  static final _shortDateFormat = new DateFormat('MMM d, yyyy ');
  static String? toShortDate(DateTime? dateTime) {
    if (dateTime == null) return null;
    return _shortDateFormat.format(dateTime);
  }

  static final _shortDateTimeFormat = new DateFormat('MMM d, yyyy, hh:mm a ');
  static String? toShortDateTime(DateTime? dateTime) {
    if (dateTime == null) return null;
    return _shortDateTimeFormat.format(dateTime);
  }

  static void addDays(DateTime date, int days) {
    date.add(Duration(days: days));
  }
}
