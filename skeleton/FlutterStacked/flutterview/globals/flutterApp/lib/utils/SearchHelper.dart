import '../../cubits/item/ItemCubit.dart';

import '../config/Constants.dart';
import './HttpException.dart';
import './HttpHelper.dart';
// import './DateHelper.dart';
// import '../bloc/router_bloc/router_bloc.dart';

class SearchHelper {
  /*
  //----------Favorites--------
  static Future<Map<String, dynamic>> favorites(
      String type, int page, String authToken) async {
    if (RouterBloc.checkLastClick()) return null;
    final url =
        '${Constants.serverUrl}/v1/favorites/$type/$page/$authToken';
    final result = await HttpHelper.get(url);
    if (result["success"])
      return result['data'];
    else {
      print('Error on favorites: ${result['message']}');
      throw new HttpException("Favorites Error: ${result['error']}");
    }
  }

  //----------Search--------
  static Future<Map<String, dynamic>> search(
      String type, String searchString, int page, String authToken) async {
    if (searchString == null) return null;
    searchString = searchString.trim();
    if (searchString.isEmpty) return null;

    if (RouterBloc.checkLastClick()) return null;
    final url =
        '${Constants.serverUrl}/v1/search/$type/$searchString/$page/$authToken';
    final result = await HttpHelper.get(url);
    if (result["success"])
      return result['data'];
    else {
      print('Error on search: ${result['message']}');
      throw new HttpException("Search Error: ${result['error']}");
    }
  }
  */

  //----------Item View--------
  static Future<Map<String, dynamic>?> view(
      String code, String authToken) async {
    //code has format p;asdf
    //where p is data type (product / product type etc)
    //and asdf is uuid of that object
    code = code.trim();
    if (code.isEmpty) return null;
    final segments = code.split(";");

    //no semicolon - old style code
    if (segments.length == 1) {
      throw new HttpException("Unrecognized code format");
    } else {
      final type = segments[0];
      final id = segments[1];
      if (ItemCubit.checkLastClick()) return null;
      final url = '${Constants.serverUrl}/v1/view/$type/$id/$authToken';
      final result = await HttpHelper.get(url);
      if (result["success"])
        return result['data'];
      else {
        print('Error loading view data: ${result['message']}');
        throw new HttpException("${result['message']}");
      }
    }
  }
/*
  //----------LIKE / UNLIKE--------
  static Future<bool> like(
      String itemType, String itemId, int likeValue, String authToken) async {
    if (RouterBloc.checkLastClick()) return null;
    final url =
        '${Constants.serverUrl}/v1/like/$itemType/$itemId/$likeValue/$authToken';
    final result = await HttpHelper.get(url);
    if (result["success"])
      return true;
    else {
      print('Error executing toggle like: ${result['message']}');
      throw new HttpException("Like Error: ${result['error']}");
    }
  }

  //----------BOOKMARK--------
  static Future<bool> bookmark(String itemType, String itemId,
      int bookmarkValue, String authToken) async {
    if (RouterBloc.checkLastClick()) return null;
    final url =
        '${Constants.serverUrl}/v1/bookmark/$itemType/$itemId/$bookmarkValue/$authToken';
    final result = await HttpHelper.get(url);
    if (result["success"])
      return true;
    else {
      print('Error executing toggle bookmark: ${result['message']}');
      throw new HttpException("Bookmark Error: ${result['error']}");
    }
  }
  */
}
