import 'package:flutter/material.dart';
import '../../cubits/item/ItemCubit.dart';
// import '../bloc/router_bloc/router_bloc.dart';

class DialogHelper {
  static Future<dynamic> exceptionDialog(
      Exception error, BuildContext context) async {
    return messageDialog(error.toString(), context);
  }

  static Future<dynamic> errorDialog(Error error, BuildContext context) async {
    return messageDialog(error.toString(), context);
  }

  static Future<dynamic> messageDialog(
      String message, BuildContext context) async {
    if (ItemCubit.checkLastClick()) return null;
    await showDialog(
      context: context,
      builder: (ctx) => AlertDialog(
        // title: Text('Error'),
        content: Text(message.toString()),
        actions: <Widget>[
          ElevatedButton(
            child: Text('OK'),
            onPressed: () {
              Navigator.of(ctx).pop();
            },
          )
        ],
      ),
    );
  }

  static Future<dynamic> messageDialogWithTitle(
    String message,
    String title,
    BuildContext context,
  ) async {
    if (ItemCubit.checkLastClick()) return null;
    await showDialog(
      context: context,
      builder: (ctx) => AlertDialog(
        title: Text(title),
        content: Text(message),
        actions: <Widget>[
          ElevatedButton(
            child: Text('OK'),
            onPressed: () {
              Navigator.of(ctx).pop();
            },
          )
        ],
      ),
    );
  }

  static Future<String> inputDialog(
      String title, String inputLabel, BuildContext context,
      {String hint = "",
      bool hasCancel = true,
      // bool isNumeric = false,
      bool isMultiline = false,
      bool obscureText = false,
      TextInputType textInputType = TextInputType.text}) async {
    if (ItemCubit.checkLastClick()) return "";
    String inputValue = '';
    var result = await showDialog<String>(
      context: context,
      barrierDismissible:
          false, // dialog is dismissible with a tap on the barrier
      builder: (BuildContext context) {
        return AlertDialog(
          title: Text(title),
          content: new Row(
            children: <Widget>[
              new Expanded(
                  child: isMultiline
                      ? TextField(
                          obscureText: obscureText,
                          keyboardType: TextInputType.multiline,
                          textInputAction: TextInputAction.done,
                          maxLines: null,
                          autofocus: true,
                          decoration: new InputDecoration(
                            labelText: inputLabel,
                            hintText: hint,
                          ),
                          onChanged: (value) {
                            inputValue = value;
                          },
                        )
                      : new TextField(
                          obscureText: obscureText,
                          autofocus: true,
                          decoration: new InputDecoration(
                            labelText: inputLabel,
                            hintText: hint,
                          ),
                          keyboardType: textInputType,
                          onChanged: (value) {
                            inputValue = value;
                          },
                        ))
            ],
          ),
          actions: <Widget>[
            if (hasCancel)
              ElevatedButton(
                child: Text('Cancel'),
                onPressed: () {
                  Navigator.of(context).pop(null);
                },
              ),
            ElevatedButton(
              child: Text('OK'),
              onPressed: () {
                Navigator.of(context).pop(inputValue);
              },
            ),
          ],
        );
      },
    );
    return result ?? "";
  }

  static Future<bool> okCancelDialog(String title, BuildContext context,
      {String message = ""}) async {
    if (ItemCubit.checkLastClick()) return false;
    var result = await showDialog<bool>(
      context: context,
      barrierDismissible:
          false, // dialog is dismissible with a tap on the barrier
      builder: (BuildContext context) {
        return AlertDialog(
          title: Text(title),
          content: message.isNotEmpty ? Text(message) : null,
          actions: <Widget>[
            ElevatedButton(
              child: Text('Cancel'),
              onPressed: () {
                Navigator.of(context).pop(false);
              },
            ),
            ElevatedButton(
              child: Text('OK'),
              onPressed: () {
                Navigator.of(context).pop(true);
              },
            ),
          ],
        );
      },
    );
    return result ?? false;
  }
}
