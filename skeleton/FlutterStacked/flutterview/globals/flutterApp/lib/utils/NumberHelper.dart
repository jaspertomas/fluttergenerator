import 'package:intl/intl.dart';

class NumberHelper {
  static bool isNumeric(String str) {
    String patttern = r'(^[0-9]+$)';
    RegExp regExp = new RegExp(patttern);
    return (regExp.hasMatch(str));
  }

  static final dFormat = new NumberFormat("#,##0.00", "en_US");
  static String decimalFormat(double d) {
    return dFormat.format(d);
  }
}
