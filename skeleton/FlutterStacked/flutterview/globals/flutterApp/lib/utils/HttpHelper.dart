import 'package:connectivity/connectivity.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';
import './HttpException.dart';

class HttpHelper {
  //--------CHECK INTERNET CONNECTION--------
  static Future<bool> isConnectedToInternet() async {
    var connectivityResult = await (Connectivity().checkConnectivity());
    if (connectivityResult == ConnectivityResult.mobile) {
      // I am connected to a mobile network.
      return true;
    } else if (connectivityResult == ConnectivityResult.wifi) {
      // I am connected to a wifi network.
      return true;
    }
    return false;
  }

  static Future<dynamic> get(String url) async {
    //check for internet connection
    if (!await isConnectedToInternet()) {
      throw new HttpException("Please connect to the Internet");
    }

    final response = await http.get(Uri.parse(url));
    if (response.statusCode == 404) {
      throw new HttpException("Server Error");
    } else if (response.statusCode == 200) {
      //-------------SUCCESS--------------
      var result = json.decode(response.body);
      return result;
    } else {
      String message =
          "Error: ${response.statusCode.toString()}: ${response.toString()}";
      // print(message);
      throw new HttpException(message);
    }
  }

  static Future<dynamic> post(String url, dynamic body) async {
    //check for internet connection
    if (!await isConnectedToInternet()) {
      throw new HttpException("Please connect to the Internet");
    }

    final response = await http.post(Uri.parse(url), body: body);
    if (response.statusCode == 404) {
      throw new HttpException("Server Error");
    } else if (response.statusCode == 200) {
      //-------------SUCCESS--------------
      var result = json.decode(response.body);
      return result;
    } else {
      String message =
          "Error: ${response.statusCode.toString()}: ${response.toString()}";
      // print(message);
      throw new HttpException(message);
    }
  }
}
