import 'package:flutter/material.dart';

class DatePickerHelper {
  static Future<void> launch(
      {required Function pickHandler,
      required Function cancelHandler,
      required BuildContext context,
      DateTime? initialDate,
      DateTime? firstDate,
      DateTime? lastDate}) async {
    DateTime today = DateTime.now();

    if (initialDate == null) initialDate = today;
    if (firstDate == null)
      firstDate = new DateTime(
          initialDate.year - 20, initialDate.month, initialDate.day);
    if (lastDate == null)
      lastDate = new DateTime(
          initialDate.year + 50, initialDate.month, initialDate.day);

    final DateTime? picked = await showDatePicker(
      context: context,
      initialDate: initialDate,
      firstDate: firstDate,
      lastDate: lastDate,
    );
    //picked==null means cancelled
    if (picked == null)
      cancelHandler();
    else
      pickHandler("${picked.toLocal()}".split(' ')[0]);
  }
}
