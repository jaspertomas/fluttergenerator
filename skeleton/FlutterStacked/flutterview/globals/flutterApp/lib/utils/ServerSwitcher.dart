import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import '../cubits/home/HomeCubit.dart';
import '../cubits/home/HomePage.dart';

import '../config/Constants.dart';
import './DialogHelper.dart';

class ServerSwitcher {
  static Future<String?> codeCheck(String code, BuildContext context) async {
    List<String> segments = code.split(";");

    //validate code has 2 or 4 segments
    if (segments.length != 2 && segments.length != 4) {
      await DialogHelper.messageDialog("Invalid Format", context);
      return null;
    }
    //when it's time to return
    //return barcode minus filler and server code
    String returnCode = "${segments[0]};${segments[1]}";

    //if only 2 segments, old code format, do nothing
    if (segments.length == 2) return returnCode;

    //validate filler
    //filler fills id so it returns 0 when divided by 100
    //this is for error checking
    //if filler code is wrong, invalid code
    final filler = int.parse(segments[2]);
    final id = int.parse(segments[1]);
    if ((id + filler) % 100 != 0) {
      await DialogHelper.messageDialog("Invalid Code", context);
      return null;
    }

    String serverCode = segments[3];
    //if server in constants does not match server in code
    if (Constants.serverCode != serverCode) {
      Server? server = getServerWithCode(serverCode);
      if (server == null) {
        //server is null
        await DialogHelper.messageDialog("Invalid Server", context);
        return null;
      } else if (!HomeCubit.instance.isMultiServer) {
        await DialogHelper.messageDialog(
            "Sorry, You have no cross server permissions", context);
        return null;
      } else {
        //server is not null
        DialogHelper.messageDialog(
            "Switching to server ${server.name}", context);
        setServer(server);
        HomePage.rebuild();
      }
    }
    return returnCode;
  }

  static void setServer(Server server) {
    Constants.serverCode = server.code;
    Constants.serverName = server.name;
    Constants.serverUrl = server.url;
  }

  static reset() {
    setServer(servers[0]);
  }

  static Server? getServerWithCode(String code) {
    for (int i = 0; i < servers.length; i++) {
      Server s = servers[i];
      if (s.code == code) return s;
    }
    return null;
  }

  static List<Server> servers = [
    Server(
      "TMC Manila",
      "T",
      Constants.serverUrl,
    ),
    Server(
      "TMC Wholesale",
      "W",
      Constants.dev ? "http://10.0.2.2:3003" : "http://192.168.1.110:3003",
    ),
  ];
}

class Server {
  Server(this.name, this.code, this.url);
  String name;
  String code;
  String url;
}
