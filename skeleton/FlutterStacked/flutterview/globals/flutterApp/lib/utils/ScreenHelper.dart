import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:flutter_html/flutter_html.dart';
import '../config/Constants.dart';

class ScreenHelper {
  /*
  static likesAndFavoritesRow(product, type, setState) {
    return Padding(
      padding: EdgeInsets.all(5),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        children: <Widget>[
          FlatButton(
            onPressed: () => toggleLike(product, type, setState),
            child: Row(
              children: <Widget>[
                FaIcon(
                  FontAwesomeIcons.solidThumbsUp,
                  color: product['user_likes'] ? Colors.blue : Colors.black,
                ),
                SizedBox(
                  width: 6,
                ),
                product['user_likes']
                    ? Text(product["likes"] == 1
                        ? 'You like this'
                        : product["likes"] == 2
                            ? 'You and 1 other likes this'
                            : 'You and ${product["likes"] - 1} others like this')
                    : Text('${product["likes"]} likes'),
              ],
            ),
          ),
          FlatButton(
            onPressed: () => toggleBookmark(product, type, setState),
            child: Row(
              children: <Widget>[
                FaIcon(
                  FontAwesomeIcons.solidBookmark,
                  color:
                      product['user_bookmarked'] ? Colors.blue : Colors.black,
                ),
                SizedBox(
                  width: 6,
                ),
                Text(product['user_bookmarked']
                    ? "Added to Favorites"
                    : 'Favorite'),
              ],
            ),
          ),
        ],
      ),
    );
  }
*/
/*
  static void toggleLike(Map<String, dynamic> item, type, setState) async {
    var success = await SearchHelper.like(
        type, item["id"], item['user_likes'] ? 0 : 1, SessionBloc.token);
    if (success) {
      setState(() {
        item['user_likes'] = !item['user_likes'];
        item['user_likes'] ? item['likes']++ : item['likes']--;
      });
    }
  }

  static void toggleBookmark(Map<String, dynamic> item, type, setState) async {
    var success = await SearchHelper.bookmark(
        type, item["id"], item['user_bookmarked'] ? 0 : 1, SessionBloc.token);
    if (success) {
      setState(() {
        item['user_bookmarked'] = !item['user_bookmarked'];
      });
    }
  }
*/
  static imageWithName(String imageUrl, String name) {
    return Stack(
      children: <Widget>[
        ClipRRect(
            borderRadius: BorderRadius.only(
              topLeft: Radius.circular(15),
              topRight: Radius.circular(15),
            ),
            child: Image.network(
              imageUrl.isNotEmpty
                  ? "${Constants.serverUrl}/$imageUrl"
                  : "${Constants.serverUrl}/person_placeholder.jpg",
              height: 250,
              width: double.infinity,
              fit: BoxFit.contain,
            )),
        Positioned(
          bottom: 20,
          right: 10,
          child: Container(
            width: 300,
            color: Colors.black54,
            padding: EdgeInsets.symmetric(
              vertical: 5,
              horizontal: 20,
            ),
            child: textWithPadding(
              name,
              style: TextStyle(fontSize: 26, color: Colors.white),
            ),
          ),
        )
      ],
    );
  }

  static textWithPadding(String text, {style = Constants.textStyleFontSize20}) {
    return Padding(
      padding: EdgeInsets.all(10),
      child: Container(
        width: double.infinity,
        child: Html(
          data: text,
        ),
      ),
    );
  }

  static addToCartAndCheckOutRow(
      Map<String, dynamic> product, BuildContext context) {
    return Padding(
      padding: EdgeInsets.all(5),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        children: <Widget>[
          TextButton(
            onPressed: null,
            child: Row(
              children: <Widget>[
                Icon(
                  Icons.add_shopping_cart,
                ),
                SizedBox(
                  width: 6,
                ),
                Text('Add to Cart'),
              ],
            ),
          ),
          TextButton(
            onPressed: null,
            child: Row(
              children: <Widget>[
                FaIcon(
                  FontAwesomeIcons.cartArrowDown,
                ),
                SizedBox(
                  width: 6,
                ),
                Text('Buy Now'),
              ],
            ),
          ),
        ],
      ),
    );
  }

/*
  static Future<void> addToCart(
      Map<String, dynamic> product, BuildContext context) async {
    final result = await DialogHelper.inputDialog(
        "Enter quantity", "Qty", context,
        textInputType:
            TextInputType.numberWithOptions(decimal: true, signed: false));
    if (result != null) {
      try {
        final qty = int.parse(result);
        return await CartHelper.setQty(product["id"], qty, SessionBloc.token);
      } on FormatException catch (_) {
        DialogHelper.stringDialog("Invalid Quantity", context);
      }
    }
  }
*/
  static priceAndInventoryRow(
      Map<String, dynamic> product, BuildContext context) {
    return Padding(
      padding: EdgeInsets.all(5),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        children: <Widget>[
          Row(
            children: <Widget>[
              Icon(
                Icons.attach_money,
              ),
              SizedBox(
                width: 6,
              ),
              Text('${product["price"]} pesos'),
            ],
          ),
          Row(
            children: <Widget>[
              Icon(
                Icons.widgets,
              ),
              SizedBox(
                width: 6,
              ),
              Text('${product["qty_available"]} units left'),
            ],
          ),
        ],
      ),
    );
  }

  static Widget tableRow(List<int> widths, List<String> strings) {
    return Row(children: [
      Expanded(
          flex: widths[0],
          child: Text(strings[0], style: Constants.textStyleFontSize18)),
      Expanded(
          flex: widths[1],
          child: Text(strings[1], style: Constants.textStyleFontSize18)),
    ]);
  }

  static Widget tableRowForWidgets(List<int> widths, List<Widget> widgets) {
    return Row(children: [
      Expanded(flex: widths[0], child: widgets[0]),
      Expanded(flex: widths[1], child: widgets[1]),
    ]);
  }
}
