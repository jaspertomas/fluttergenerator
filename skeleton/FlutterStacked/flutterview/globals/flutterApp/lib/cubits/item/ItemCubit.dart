import 'package:bloc/bloc.dart';
import 'package:flutter/material.dart';
import 'package:meta/meta.dart';
import '../../config/Constants.dart';
import '../../cubits/home/HomeCubit.dart';
import 'package:auto_route/auto_route.dart';
import '../../utils/DialogHelper.dart';
import '../../utils/SearchHelper.dart';
import '../../utils/ServerSwitcher.dart';
import '../../config/app_router.gr.dart' as r;
//[imports]

part 'ItemState.dart';

/*
Previous Item system works like this
Only views are stored in history. Edits not stored in history.
Always reload from server when Back is pressed. Best have a fresh version always.
*/

class ItemCubit extends Cubit<ItemState> {
  ItemCubit() : super(ItemInitial());

  static ItemCubit? _instance;

  static ItemCubit get instance {
    if (_instance == null) _instance = new ItemCubit();
    return _instance!;
  }

  static void dispose() {
    if (_instance == null) return;
    _instance!.close();
    _instance = null;
  }

//--------------

  static DateTime lastClick = DateTime.now();
  static void resetLastClick() {
    lastClick = DateTime.now();
  }

  static bool checkLastClick() {
    final now = DateTime.now();
    final difference = now.difference(lastClick).inMinutes;
    if (difference >= Constants.MAX_IDLE_MINUTES) {
      HomeCubit.instance.logout();
      return true;
    } else {
      lastClick = now;
      return false;
    }
  }

  static Map<String, dynamic>? _editItem;
  static Map<String, dynamic>? get editItem => _editItem;
  static String? get editItemId =>
      _editItem == null ? null : _editItem!["id"] ?? '';
  static bool? get editItemIsNew => _editItem == null
      ? null
      : _editItem!["id"] != null && _editItem!["id"].isEmpty;
  static void setEditItem(Map<String, dynamic> value) => _editItem = value;

  static Map<String, dynamic>? _item;
  static Map<String, dynamic>? get item => _item;
  static String get itemId => _item == null ? '' : _item!["id"] ?? '';
  static String get itemType => _item == null ? '' : _item!["type"];

  static void setItem(Map<String, dynamic> item) async {
    //if previous items exceed 10 in length, remove first item
    if (_previousItems.length > 10) _previousItems.removeAt(0);
    //save current item for later use if item type = a, f or c

    //if item being saved is last item in list, do not add to list
    if (_previousItems.isEmpty || item['id'] != _previousItems.last.id) {
      _previousItems.add(new PreviousItem(item["type"], item["id"]));
    }
    _item = item;
  }

  static List<PreviousItem> _previousItems = [];

  //use this in back button to go to previously viewed item
  //will do nothing if there's no previously viewed item
  //do not use this to cancel an edit operation
  //because when you cancel an edit
  //you want to view the current item not the previous item
  static void back(BuildContext context) {
    //if no previous items, go to dashboard
    if (_previousItems.isEmpty) {
      context.router.push(r.Dashboard());
    } else {
      //if current item and last item in queue are the same
      //discard last item
      if (_item != null && _item!["id"] == _previousItems.last.id) {
        _previousItems.removeLast();
      }

      //view last item
      PreviousItem previousItem = _previousItems.removeLast();
      viewHandler(previousItem.code(), context);
    }
  }

  //use this to cancel edits
  static void cancelEdit(BuildContext context) {
    if (_item != null)
      viewHandler("${_item!["type"]};${_item!["id"]}", context);
    else
      context.router.push(r.Dashboard());
  }

  //use this to clear history
  static void clearHistory() {
    _previousItems = [];
  }

  static Map<String, dynamic> getNewStreet(String barangayId) {
    return {
      'type': 'a',
      'id': '',
      'name': '',
      'shortname': '',
      'compound': '',
      'purok': '',
      'barangay_id': barangayId,
      'is_published': "true",
      'delete_reason': '',
    };
  }

  static Future<bool> viewHandler(String? code, BuildContext context,
      {bool edit = false}) async {
    if (checkLastClick()) return false;
    if (code == null) return false;

    try {
      //see if code belongs to different server
      //also remove checker and server code components to barcode
      code = await ServerSwitcher.codeCheck(code, context);

      //if code is invalid, quit
      if (code == null) return false;

      //---send request to server---
      final result = await SearchHelper.view(code, HomeCubit.instance.token);
      //set product data
      // print("-----${json.decode(result["item"])}--------");
      // setItem(result["item"]);
      //show products screen
      if (result == null ||
          !result.containsKey("item") ||
          !result["item"].containsKey("type")) return false;
      setItem(result["item"]);
      switch (result["item"]["type"]) {
//[viewHandlerSwitch]
        default:
          // RouterBloc.setActivity(Activity.ItemView);
          // RouterBloc.setActivity(Activity.AddressSearchStreetList);
          break;
      }
      return true;
      //---there's a bigger error---
    } on Exception catch (e) {
      //popup error message
      await DialogHelper.exceptionDialog(e, context);
      return false;
    }
  }

  static void scanHandler(BuildContext context) {
    context.router.push(r.QrScanRoute());
  }

/*
  static BarcodeScanHelper barcodeScanHelper = new BarcodeScanHelper();
  static void scanHandler(BuildContext context) async {
    //if setup not finished, dont do anything
    if (SessionBloc.setupStatus != 1) return;

    //----------SCAN QRCODE ---------------
    await BarcodeScanHelper.scan();
    //----------check barcode result for errors--------
    if (BarcodeScanHelper.error != null)
      DialogHelper.messageDialog(
          "Barcode Scan error: ${BarcodeScanHelper.error}", context);
    //----------no errors, use code to load product from server----------
    else if (BarcodeScanHelper.result != null) {
      RouterBloc.viewHandler(BarcodeScanHelper.result, context);
    }
  }
  */
  /*
  static void productActionsNewStockEntry(BuildContext context) {
    ProductViewBloc.mode = ProductViewBloc.NEWSTOCKENTRY;
    RouterBloc.setActivity(Activity.ProductView);
    ProductViewBloc.refresh(context);
  }

  static void productActions(BuildContext context) {
    if (Dashboard.inventoryMode) {
      productActionsNewStockEntry(context);
      return;
    }
    Product product = Product(ItemCubit.item);
    PickerBloc.launch(
        title: product.name,
        list: [
          PickerItem(label: "Record Inventory", value: "Record Inventory"),
          PickerItem(label: "View Inventory", value: "View Inventory"),
          PickerItem(label: "Take a picture", value: 'take_picture'),
          PickerItem(label: "View Product", value: 'view_product'),
          PickerItem(label: "Back to Dashboard", value: "back"),
        ],
        pickHandler: (String value, {BuildContext context}) {
          // Product product = new Product(RouterBloc.editItem);
          switch (value) {
            case "back":
              context.router.push(r.Dashboard());
              break;
            case "view_product":
              ProductViewBloc.mode = ProductViewBloc.INFO;
              RouterBloc.setActivity(Activity.ProductView);
              ProductViewBloc.refresh(context);
              break;
            case "take_picture":
              ProductViewBloc.mode = ProductViewBloc.PICTURES;
              RouterBloc.setActivity(Activity.ProductView);
              ProductViewBloc.refresh(context);
              break;
            case "Record Inventory":
              productActionsNewStockEntry(context);
              break;
            case "View Inventory":
              ProductViewBloc.mode = ProductViewBloc.STOCKENTRYS;
              RouterBloc.setActivity(Activity.ProductView);
              ProductViewBloc.refresh(context);
              break;
          }
        },
        cancelHandler: () {
          context.router.push(r.Dashboard());
        });
  }*/
}

class PreviousItem {
  String id;
  String type;
  PreviousItem(this.type, this.id);
  String code() {
    return "$type;$id";
  }
}
