import 'package:flutter/material.dart';
import 'package:stacked/stacked.dart';
import '../../../config/Constants.dart';
// import '../../../utils/DialogHelper.dart';
import '../../../models/[subtableCamelCaseCapsSingular].dart';
import '[subtableCamelCaseCapsSingular]ItemViewModel.dart';
import '[subtableCamelCaseCapsSingular]TabModel.dart';

class [subtableCamelCaseCapsSingular]ItemView extends StatelessWidget {
  final [subtableCamelCaseCapsSingular]TabModel [subtableCamelCaseSingular]TabModel;
  final [subtableCamelCaseCapsSingular] [subtableCamelCaseSingular];
  [subtableCamelCaseCapsSingular]ItemView(this.[subtableCamelCaseSingular]TabModel, this.[subtableCamelCaseSingular]);

  static List<int> widths = [1, 1];

  static headers() {
    return Card(
        // color: Constants.white,
        child: Row(children: [
      Expanded(
          flex: widths[0],
          child: Text("id", style: Constants.textStyleFontSize20)),
      Expanded(
          flex: widths[1],
          child: Text("id", style: Constants.textStyleFontSize20)),
    ]));
  }

  @override
  Widget build(BuildContext context) {
    return ViewModelBuilder<[subtableCamelCaseCapsSingular]ItemViewModel>.reactive(
      viewModelBuilder: () =>
          [subtableCamelCaseCapsSingular]ItemViewModel([subtableCamelCaseSingular]TabModel, [subtableCamelCaseSingular]),
      // onModelReady: (model) async {},
      builder: (context, model, child) => Center(
        child: model.isBusy
            ? CircularProgressIndicator()
            : Card(
                // color: Constants.white,
                child: InkWell(
                  // onTap: () => select(item),
                  child: Row(
                    children: [
                      Expanded(
                        flex: widths[0],
                        child: Text([subtableCamelCaseSingular].id,
                            style: Constants.textStyleFontSize20),
                      ),
                      Expanded(
                        flex: widths[1],
                        child: Text([subtableCamelCaseSingular].id,
                            style: Constants.textStyleFontSize20),
                      ),
                    ],
                  ),
                ),
              ),
      ),
    );
  }
}
//[donotgenerate]
