import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:stacked/stacked.dart';
// import 'package:decimal/decimal.dart';
import '../../../models/[tableCamelCaseCapsSingular].dart';
import '../../../utils/DialogHelper.dart';
import '../../../models/[subtableCamelCaseCapsSingular].dart';
// import '../../models/Customer.dart';
import '[subtableCamelCaseCapsSingular]Api.dart';
import '../[tableCamelCaseCapsSingular]ViewModel.dart';

class [subtableCamelCaseCapsSingular]TabModel extends BaseViewModel {
  [tableCamelCaseCapsSingular]ViewModel parent;
  [subtableCamelCaseCapsSingular]TabModel(this.parent);

  [tableCamelCaseCapsSingular] get [tableCamelCaseSingular] => parent.[tableCamelCaseSingular];
  [tableCamelCaseCapsSingular]ViewModel get [tableCamelCaseSingular]ViewModel => parent;
  // Customer get customer => parent.customer;
  List<[subtableCamelCaseCapsSingular]> _details = [];
  List<[subtableCamelCaseCapsSingular]> get details => _details;
  // [tableCamelCaseCapsSingular]Template get template => parent.template;

  // set [tableCamelCaseSingular]([tableCamelCaseCapsSingular] [tableCamelCaseSingular]) => _item = [tableCamelCaseSingular];
  bool _isBusy = false;
  bool get isBusy => _isBusy;

  //vali[fieldCamelCaseSingular] on save - customize me
  bool valid() {
    bool valid = true;
    //vali[fieldCamelCaseSingular]
    //feel free to remove items that are not vali[fieldCamelCaseSingular]d
    if (!notesValid()) valid = false;
    // if (!isTemporaryValid()) valid = false;
    notifyListeners();
    return valid;
  }

  //load [tableCamelCaseSingular] to edit from server
  load() async {
    _isBusy = true;
    notifyListeners();

    final resultDetails =
        await [subtableCamelCaseCapsSingular]Api.selectDetails(int.parse([tableCamelCaseSingular].id));
    List resultDetailsItems = resultDetails["items"];
    _details = [];
    resultDetailsItems.forEach((element) {
      _details.add([subtableCamelCaseCapsSingular](element));
    });

    _isBusy = false;

    notifyListeners();
  }

  Future<bool> save(BuildContext context) async {
    //todo: checkLastClick
    // if (RouterBloc.checkLastClick()) return null;

    if (!valid()) {
      DialogHelper.messageDialog("Please correct the errors", context);
      return false;
    } else {
      _isBusy = true;
      notifyListeners();
      try {
        final result = await [subtableCamelCaseCapsSingular]Api.save([tableCamelCaseSingular].data);

        //if new, set id from result
        if ([tableCamelCaseSingular].isNew) [tableCamelCaseSingular].data["id"] = result["id"].toString();

        DialogHelper.messageDialog("Save successful", context);
        _isBusy = false;
        notifyListeners();
        return true;
      } on Exception catch (e) {
        DialogHelper.exceptionDialog(e, context);
        _isBusy = false;
        notifyListeners();
        return false;
      }
    }
  }

  String notesError;

  notesChanged(String value) {
    [tableCamelCaseSingular].notes = value;
    notesValid();
    notifyListeners();
  }

  notesValid() {
    if ([tableCamelCaseSingular].notes.isEmpty) {
      notesError = "Notes is required";
      return false;
    } else {
      notesError = null;
      return true;
    }
  }
}
//[donotgenerate]
