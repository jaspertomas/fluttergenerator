import '../../../config/locator.dart';
import '../../../config/Constants.dart';
import '../../../utils/Exceptions.dart';
import '../../../utils/HttpHelper.dart';
import '../../../services/SessionService.dart';

class [subtableCamelCaseCapsSingular]Api {
  static final _sessionService = locator<SessionService>();
  static Future<Map<String, dynamic>> select(int id) async {
    final url =
        '${Constants.serverUrl}/v1/view/inv/$id/${_sessionService.token}';
    final result = await HttpHelper.get(url);
    if (result["success"])
      return result['data'];
    else {
      print('Error on [tableCamelCaseSingular] search: ${result['message']}');
      throw new HttpException("${result['message']}");
    }
  }

  static Future<Map<String, dynamic>> selectDetails(int id) async {
    //  post '[subtableCodeCase]/list/:parent_type/:parent_id/:token', :to => '[tableCamelCaseSingular]_details#list'

    final url =
        '${Constants.serverUrl}/v1/[subtableCodeCase]/list/inv/$id/${_sessionService.token}';
    final result = await HttpHelper.get(url);
    if (result["success"])
      return result['data'];
    else {
      print('Error on [tableCamelCaseSingular] detail load: ${result['message']}');
      throw new HttpException("${result['message']}");
    }
  }

  static Future<Map<String, dynamic>> save(Map<String, dynamic> data) async {
    //todo: check last click
    final url = '${Constants.serverUrl}/v1/inv/update/${_sessionService.token}';
    final result = await HttpHelper.post(url, data);
    if (result["success"])
      return result['data'];
    else {
      print('${result['message']}');
      throw new HttpException("${result['message']}");
    }
  }
}
//[donotgenerate]
