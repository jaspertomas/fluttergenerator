import 'package:flutter/material.dart';
import 'package:flutterApp/widgets/DashboardTools.dart';
import 'package:stacked_services/stacked_services.dart';
import 'package:stacked/stacked.dart';
import '../../../config/Constants.dart';
import '../../../config/locator.dart';
import '../../../models/[subtableCamelCaseCapsSingular].dart';
import '[subtableCamelCaseCapsSingular]TabDashboard.dart';
import '[subtableCamelCaseCapsSingular]ItemView.dart';
import '[subtableCamelCaseCapsSingular]TabModel.dart';
import '../[tableCamelCaseCapsSingular]ViewModel.dart';

class [subtableCamelCaseCapsSingular]Tab extends StatelessWidget {
  final [tableCamelCaseCapsSingular]ViewModel parentModel;
  [subtableCamelCaseCapsSingular]Tab(this.parentModel);
  @override
  Widget build(BuildContext context) {
    return ViewModelBuilder<[subtableCamelCaseCapsSingular]TabModel>.reactive(
      viewModelBuilder: () => [subtableCamelCaseCapsSingular]TabModel(parentModel),
      onModelReady: (model) async {
        await model.load();
      },
      builder: (context, model, child) => Center(
        child: model.isBusy
            ? CircularProgressIndicator()
            : SingleChildScrollView(
                child: Column(
                  children: [
                    [subtableCamelCaseCapsSingular]TabDashboard(model),
                    SingleChildScrollView(
                      child: Column(
                        children: [
                          addItemButton(context, model),
                          [subtableCamelCaseCapsSingular]ItemView.headers(),
                          ...model.details
                              .map((item) => [subtableCamelCaseCapsSingular]ItemView(model, item))
                              .toList(),
                          submitButton(model, context),
                          cancelButton(),
                        ],
                      ),
                    ),
                  ],
                ),
              ),
      ),
    );
  }

  Widget submitButton([subtableCamelCaseCapsSingular]TabModel model, BuildContext context) {
    return model.isBusy
        ? CircularProgressIndicator()
        : RaisedButton(
            child: Text('Submit'),
            color: Colors.blue,
            onPressed: () async {
              bool success = await model.save(context);
              if (success) submitPicker();
            },
          );
  }

  submitPicker() {
    /*
        locator<SearchPickerService>().launch(context,
            title: "Company:",
            url: '/v1/com/search/',
            //comment this out to disable loading of first 20 items on launch
            preload: true,
            // initialSearchString: "",
            // header: () => SizedBox(height: 0),
            header: () => ComPickerItem.headers(),
            pickerItem: (item) => ComPickerItem(item),
            // moreButtons: <Widget>[
            //   RaisedButton(
            //       child: Text("Hello World!"),
            //       onPressed: () {
            //         //close modal window
            //         // Navigator.pop(context);
            //         //access the current search string
            //         // print(locator<SearchPickerService>().searchString);
            //         //go somewhere else
            //         // locator<NavigationService>().navigateTo("/customer/new");
            //       }),
            // ],
            pickHandler: (BuildContext context, Company item) {
              model.companyChanged(item);
            },
            cancelHandler: () {});  
    */
  }

  addItemButton(BuildContext context, [subtableCamelCaseCapsSingular]TabModel model) {
    return RaisedButton(
      child: Text('Add Item'),
      color: Colors.blue,
      onPressed: () {
        //add product to data
        [subtableCamelCaseCapsSingular] [subtableCamelCaseSingular] = [subtableCamelCaseCapsSingular].createFrom[tableCamelCaseCapsSingular](model.[tableCamelCaseSingular]);
        showDialog(
          context: context,
          builder: (BuildContext context) {
            return AlertDialog(content: Text("[subtableCamelCaseCapsSingular]EditView")
                //todo: add [subtableSmallCaseSingular] edit form here
                // [subtableCamelCaseCapsSingular]EditView(model, invdet),
                );
          },
        );
      },
    );
  }

  cancelButton() {
    return RaisedButton(
      child: Text('Reset'),
      color: Colors.blue,
      onPressed: () {
        locator<NavigationService>().navigateTo("/mainmenu");
      },
    );
  }

  //text
  Widget notesField([subtableCamelCaseCapsSingular]TabModel model, BuildContext context) {
    // TextEditingController notesController;
    // notesController.value =
    //     notesController.value.copyWith(text: snapshot.data);
    return TextFormField(
      // controller: notesController,
      initialValue: model.[tableCamelCaseSingular].notes,
      textCapitalization: TextCapitalization.sentences,
      onChanged: (String value) => model.notesChanged(value),
      keyboardType: TextInputType.multiline,
      textInputAction: TextInputAction.done,
      maxLines: null,
      decoration: InputDecoration(
        // hintText: '',
        labelText: 'Notes *',
        errorText: model.notesError,
        errorStyle: TextStyle(color: Constants.errorColor),
      ),
    );
  }
}
//[donotgenerate]
