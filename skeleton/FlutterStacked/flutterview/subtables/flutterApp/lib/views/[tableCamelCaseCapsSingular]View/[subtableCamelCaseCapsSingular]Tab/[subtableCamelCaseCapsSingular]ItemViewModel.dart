import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:stacked/stacked.dart';
// import 'package:decimal/decimal.dart';
import '../../../utils/DialogHelper.dart';
import '../../../models/[subtableCamelCaseCapsSingular].dart';
// import '../../models/Customer.dart';
import '[subtableCamelCaseCapsSingular]Api.dart';
import '[subtableCamelCaseCapsSingular]TabModel.dart';

class [subtableCamelCaseCapsSingular]ItemViewModel extends BaseViewModel {
  [subtableCamelCaseCapsSingular]TabModel parent;
  [subtableCamelCaseCapsSingular] _item;
  [subtableCamelCaseCapsSingular]ItemViewModel(this.parent, this._item);

  [subtableCamelCaseCapsSingular] get [tableCamelCaseSingular] => _item;

  bool _isBusy = false;
  bool get isBusy => _isBusy;

  //vali[fieldCamelCaseSingular] on save - customize me
  bool valid() {
    bool valid = true;
    //vali[fieldCamelCaseSingular]
    //feel free to remove items that are not vali[fieldCamelCaseSingular]d
    // if (!notesValid()) valid = false;
    // if (!isTemporaryValid()) valid = false;
    notifyListeners();
    return valid;
  }

  //load Receipt to edit from server
  load(int id) async {
    /*
    _isBusy = true;
    notifyListeners();
    final result = await ReceiptApi.select(id);
    _item = [subtableCamelCaseCapsSingular](result["item"]);
    _isBusy = false;

    notifyListeners();
    */
  }

  Future<bool> save(BuildContext context) async {
    //todo: checkLastClick
    // if (RouterBloc.checkLastClick()) return null;

    if (!valid()) {
      DialogHelper.messageDialog("Please correct the errors", context);
      return false;
    } else {
      _isBusy = true;
      notifyListeners();
      try {
        final result = await [subtableCamelCaseCapsSingular]Api.save([tableCamelCaseSingular].data);

        //if new, set id from result
        if ([tableCamelCaseSingular].isNew) [tableCamelCaseSingular].data["id"] = result["id"].toString();

        DialogHelper.messageDialog("Save successful", context);
        _isBusy = false;
        notifyListeners();
        return true;
      } on Exception catch (e) {
        DialogHelper.exceptionDialog(e, context);
        _isBusy = false;
        notifyListeners();
        return false;
      }
    }
  }
/*
  String notesError;

  notesChanged(String value) {
    [tableCamelCaseSingular].notes = value;
    notesValid();
    notifyListeners();
  }

  notesValid() {
    if ([tableCamelCaseSingular].notes.isEmpty) {
      notesError = "Notes is required";
      return false;
    } else {
      notesError = null;
      return true;
    }
  }
  */
}
//[donotgenerate]
