import 'package:flutter/material.dart';
import 'package:flutterApp/config/locator.dart';
import 'package:flutterApp/services/PickerService.dart';
import 'package:flutterApp/widgets/DashboardTools.dart';

import '[subtableCamelCaseCapsSingular]TabModel.dart';

class [subtableCamelCaseCapsSingular]TabDashboard extends StatelessWidget {
  final [subtableCamelCaseCapsSingular]TabModel model;
  const [subtableCamelCaseCapsSingular]TabDashboard(this.model, {Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    List<int> widthsA = [1, 2];
    List<int> widthsB = [1, 2];
    List<int> widthsC = [1, 2];
    return Row(
      children: [
        Expanded(
          child: Column(
            children: [
              DashboardTools.dashRow("hello", "world", widthsA[0], widthsA[1]),
            ],
          ),
        ),
        Expanded(
          child: Column(
            children: [
              DashboardTools.dashLink("Back to","Main Menu", widthsB[0], widthsB[1],
                  url: "/mainmenu"),
            ],
          ),
        ),
        Expanded(
          child: Column(
            children: [
              DashboardTools.dashButton(
                "Action",
                "Action",
                widthsC[0],
                widthsC[1],
                onTap: () {
                  //do something
                },
              ),
            ],
          ),
        ),
      ],
    );
  }
}
//[donotgenerate]
