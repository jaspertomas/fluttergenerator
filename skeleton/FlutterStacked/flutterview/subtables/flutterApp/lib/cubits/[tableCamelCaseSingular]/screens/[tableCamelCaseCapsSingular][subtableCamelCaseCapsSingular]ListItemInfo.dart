import 'package:flutter/material.dart';
// import 'package:provider/provider.dart';
// import '../../item/ItemCubit.dart';// import '../../[subtableCamelCaseCapsSingular]ViewBloc/[subtableCamelCaseCapsSingular]ViewBloc.dart';
// import '../[tableCamelCaseCapsSingular]Cubit.dart';
// import '../../../utils/DialogHelper.dart';
import '../../../config/Constants.dart';
import '../../../models/[subtableCamelCaseCapsSingular].dart';
import '../../../utils/NumberHelper.dart';

class [tableCamelCaseCapsSingular][subtableCamelCaseCapsSingular]ListItemInfo extends StatefulWidget {
  final Map<String, dynamic> item;
  // final [tableCamelCaseCapsSingular]Cubit [tableCamelCaseCapsSingular]Cubit;

  [tableCamelCaseCapsSingular][subtableCamelCaseCapsSingular]ListItemInfo(this.item);

  @override
  _[tableCamelCaseCapsSingular][subtableCamelCaseCapsSingular]ListItemInfoState createState() =>
      _[tableCamelCaseCapsSingular][subtableCamelCaseCapsSingular]ListItemInfoState();

  static const List<int> widths = [2, 6, 3, 3];
  static headers() {
    return Card(
        child: Row(children: [
      Expanded(
          flex: widths[0],
          child: Text("ID", style: Constants.textStyleFontSize18)),
      Expanded(
          flex: widths[1],
          child: Text("Name", style: Constants.textStyleFontSize18)),
    ]));
  }
}

class _[tableCamelCaseCapsSingular][subtableCamelCaseCapsSingular]ListItemInfoState
    extends State<[tableCamelCaseCapsSingular][subtableCamelCaseCapsSingular]ListItemInfo> {
  bool isActionLoading = false;
  bool isLoading = false;

/*
  // RouterBloc routerBloc;
  void select() async {
    // setState(() {
    //   isLoading = true;
    // });
    [subtableCamelCaseCapsSingular]ViewBloc.mode = [subtableCamelCaseCapsSingular]ViewBloc.INFO;
    RouterBloc.setItem(widget.item);
    RouterBloc.setActivity(Activity.[subtableCamelCaseCapsSingular]View);
    // setState(() {
    //   isLoading = true;
    // });
  }
*/
  @override
  Widget build(BuildContext context) {
    [subtableCamelCaseCapsSingular] [subtableCamelCaseSingular] = [subtableCamelCaseCapsSingular](widget.item);
    return Card(
      child: Row(
        children: [
          Expanded(
              flex: [tableCamelCaseCapsSingular][subtableCamelCaseCapsSingular]ListItemInfo.widths[0],
              child: Text([subtableCamelCaseSingular].id,
                  style: Constants.textStyleFontSize18)),
          Expanded(
              flex: [tableCamelCaseCapsSingular][subtableCamelCaseCapsSingular]ListItemInfo.widths[1],
              child: Text([subtableCamelCaseSingular].[subtableNameField],
                  style: Constants.textStyleFontSize18)),
          // Expanded(
          //     flex: [tableCamelCaseCapsSingular][subtableCamelCaseCapsSingular]ListItemInfo.widths[2],
          //     child: Text(
          //         NumberHelper.decimalFormat(
          //             double.parse([subtableCamelCaseSingular].price)),
          //         style: Constants.textStyleFontSize18)),
        ],
      ),
    );
  }
}
