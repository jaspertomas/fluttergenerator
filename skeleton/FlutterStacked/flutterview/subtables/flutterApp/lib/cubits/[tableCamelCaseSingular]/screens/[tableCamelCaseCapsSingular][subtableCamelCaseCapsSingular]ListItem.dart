import 'package:flutter/material.dart';
import 'package:librarian/cubits/home/HomeCubit.dart';
// import 'package:provider/provider.dart';
// import '../../item/ItemCubit.dart';import '../../session_bloc/session_bloc.dart';
// import '../../[subtableCamelCaseCapsSingular]ViewBloc/[subtableCamelCaseCapsSingular]ViewBloc.dart';
// import '../[tableCamelCaseCapsSingular]Cubit.dart';
// import '../../../utils/NumberHelper.dart';
import '../../../utils/DialogHelper.dart';
import '../../../config/Constants.dart';
import '../../../models/[subtableCamelCaseCapsSingular].dart';
import '../[tableCamelCaseCapsSingular]Api.dart';

class [tableCamelCaseCapsSingular][subtableCamelCaseCapsSingular]ListItem extends StatefulWidget {
  final Map<String, dynamic> item;
  // final [tableCamelCaseCapsSingular]Cubit [tableCamelCaseCapsSingular]Cubit;

  [tableCamelCaseCapsSingular][subtableCamelCaseCapsSingular]ListItem(this.item);

  @override
  _[tableCamelCaseCapsSingular][subtableCamelCaseCapsSingular]ListItemState createState() =>
      _[tableCamelCaseCapsSingular][subtableCamelCaseCapsSingular]ListItemState();

  static const List<int> widths = [2, 5];
  static headers() {
    return Card(
        child: Row(children: [
      Expanded(
          flex: widths[0],
          child: Text("Id", style: Constants.textStyleFontSize18)),
      Expanded(
          flex: widths[1],
          child: Text("Name", style: Constants.textStyleFontSize18)),
    ]));
  }
}

class _[tableCamelCaseCapsSingular][subtableCamelCaseCapsSingular]ListItemState
    extends State<[tableCamelCaseCapsSingular][subtableCamelCaseCapsSingular]ListItem> {
  bool isActionLoading = false;
  bool isLoading = false;

  // RouterBloc routerBloc;
  void select([subtableCamelCaseCapsSingular] [subtableCamelCaseSingular]) async {
    /*
    final result = await DialogHelper.inputDialog(
        "Received: ${1}", "Enter new qty", context,
        textInputType: TextInputType.numberWithOptions(decimal: false));

    //if cancelled
    if (result.isEmpty) return;

    int qty = int.parse(result);

    //validate not negative
    if (qty < 0 || qty > double.parse([subtableCamelCaseSingular].qty)) {
      DialogHelper.messageDialog("Invalid qty", context);
    }

    try {
      final success = await [tableCamelCaseCapsSingular]Api.updateDetailReceiveQty(
          [subtableCamelCaseSingular].id, qty, HomeCubit.instance.token);
      if (success ?? false) {
        setState(() {
          [subtableCamelCaseSingular].qtyReceived = qty.toString();
        });
        DialogHelper.messageDialog("Receive qty updated", context);
      }
    } on Exception catch (e) {
      DialogHelper.exceptionDialog(e, context);
    }
    */
  }

  @override
  Widget build(BuildContext context) {
    [subtableCamelCaseCapsSingular] [subtableCamelCaseSingular] = [subtableCamelCaseCapsSingular](widget.item);
    return Card(
      child: InkWell(
        onTap: () => select([subtableCamelCaseSingular]),
        child: Row(
          children: [
            Expanded(
                flex: [tableCamelCaseCapsSingular][subtableCamelCaseCapsSingular]ListItem.widths[0],
                child: Text([subtableCamelCaseSingular].id,
                    style: Constants.textStyleFontSize18)),
            Expanded(
                flex: [tableCamelCaseCapsSingular][subtableCamelCaseCapsSingular]ListItem.widths[1],
                child: Text([subtableCamelCaseSingular].[subtableNameField],
                    style: Constants.textStyleFontSize18)),
            // Expanded(
            //     flex: [tableCamelCaseCapsSingular][tableCamelCaseCapsSingular]detailListItem.widths[0],
            //     child: Text([tableCamelCaseSingular]detail.qtyReceived,
            //         style: Constants.textStyleFontSize18)),
          ],
        ),
      ),
    );
  }
}
