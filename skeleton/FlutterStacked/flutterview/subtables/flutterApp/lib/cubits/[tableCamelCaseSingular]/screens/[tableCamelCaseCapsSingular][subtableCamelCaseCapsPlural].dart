// // import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:flutter/material.dart';
// import '../[tableCamelCaseCapsSingular]ViewScreenSwitch.dart';
import '../[tableCamelCaseCapsSingular]Cubit.dart';
import './[tableCamelCaseCapsSingular][subtableCamelCaseCapsSingular]ListItem.dart';
import '../../../config/Constants.dart';

class [tableCamelCaseCapsSingular][subtableCamelCaseCapsPlural] extends StatefulWidget {
  const [tableCamelCaseCapsSingular][subtableCamelCaseCapsPlural]({Key? key}) : super(key: key);

  @override
  _[tableCamelCaseCapsSingular][subtableCamelCaseCapsPlural]State createState() =>
      _[tableCamelCaseCapsSingular][subtableCamelCaseCapsPlural]State();
}

class _[tableCamelCaseCapsSingular][subtableCamelCaseCapsPlural]State extends State<[tableCamelCaseCapsSingular][subtableCamelCaseCapsPlural]> {
  // ScrollController _scrollController;
  bool isLoading = true;
  bool isInit = false;

  @override
  void didChangeDependencies() async {
    if (!isInit) {
      await [tableCamelCaseCapsSingular]Cubit.load[subtableCamelCaseCapsSingular]List(context);
      setState(() {
        isLoading = false;
        isInit = true;
      });
    }

    super.didChangeDependencies();
  }

  @override
  Widget build(BuildContext context) {
    // [tableCamelCaseCapsSingular] [tableCamelCaseSingular] = [tableCamelCaseCapsSingular](ItemCubit.item);
    List<dynamic> list = [tableCamelCaseCapsSingular]Cubit.[subtableCamelCaseSingular]List;
    final deviceSize = MediaQuery.of(context).size;

    return Column(
      children: <Widget>[
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: <Widget>[
            Text("Details", style: Constants.textStyleFontSize20Bold),
            /*
            //--------NEW [tableUpperCaseSingular]DETAIL-------
            IconButton(
              icon: FaIcon(FontAwesomeIcons.plus, color: Colors.blue, size: 22),
              onPressed: () {
                // newStockEntry([tableCamelCaseSingular]);
                [tableCamelCaseCapsSingular]Cubit.mode = [tableCamelCaseCapsSingular]Cubit.NEW[subtableUpperCaseSingular];
                [tableCamelCaseCapsSingular]ViewScreenSwitch.refresh();
              },
            ),
             */
            IconButton(
              icon: Icon(Icons.refresh, color: Colors.blue, size: 30),
              onPressed: () async {
                setState(() {
                  isLoading = true;
                });
                await [tableCamelCaseCapsSingular]Cubit.load[subtableCamelCaseCapsSingular]List(context);
                setState(() {
                  isLoading = false;
                });
              },
            ),
          ],
        ),
        isLoading
            ? Center(
                child: CircularProgressIndicator(),
              )
            : Column(
                children: <Widget>[
                  list.length == 0
                      ? Text(
                          "No items found",
                          style: TextStyle(fontSize: 20),
                        )
                      : Container(
                          height: deviceSize.height - 310,
                          child: ListView(
                            // controller: _scrollController,
                            children: [
                              [tableCamelCaseCapsSingular][subtableCamelCaseCapsSingular]ListItem.headers(),
                              ...list
                                  .map((item) =>
                                      [tableCamelCaseCapsSingular][subtableCamelCaseCapsSingular]ListItem(item))
                                  .toList(),
                            ],
                          ),
                        ),
                ],
              )
      ],
    );
  }
}
