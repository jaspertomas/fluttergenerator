[subtable_imports]
------
import '[subtableCamelCaseCapsSingular]Tab/[subtableCamelCaseCapsSingular]Tab.dart';
======
[subtable_tabs]
------
                  Tab(
                      child: Text(
                    "[subtableCamelCaseCapsPlural]",
                    style: TextStyle(color: Constants.black),
                  )),
======
[subtable_tab_invocations]
------
                          [subtableCamelCaseCapsSingular]Tab(model),
======
