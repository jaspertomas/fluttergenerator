[subtablesnippetimports]
------
import './[tableCamelCaseCapsSingular][subtableCamelCaseCapsSingular]ListItemInfo.dart';
======
[detailsinitializer]
------
      await [tableCamelCaseCapsSingular]Cubit.load[subtableCamelCaseCapsSingular]List(context);
======
[detailsfunction]
------

  Widget details() {
    List<dynamic> list = [tableCamelCaseCapsSingular]Cubit.[subtableCamelCaseSingular]List;
    // final deviceSize = MediaQuery.of(context).size;
    return Column(
      children: <Widget>[
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: <Widget>[
            Text("Details", style: Constants.textStyleFontSize20Bold),
            IconButton(
              icon: Icon(Icons.refresh, color: Colors.blue, size: 30),
              onPressed: () async {
                setState(() {
                  isLoading = true;
                });
                await [tableCamelCaseCapsSingular]Cubit.load[subtableCamelCaseCapsSingular]List(context);
                setState(() {
                  isLoading = false;
                });
              },
            ),
          ],
        ),
        isLoading
            ? Center(
                child: CircularProgressIndicator(),
              )
            : list.length == 0
                ? Text(
                    "No items found",
                    style: TextStyle(fontSize: 20),
                  )
                : Column(
                    children: [
                      [tableCamelCaseCapsSingular][subtableCamelCaseCapsSingular]ListItemInfo.headers(),
                      ...list
                          .map((item) =>
                              [tableCamelCaseCapsSingular][subtableCamelCaseCapsSingular]ListItemInfo(item))
                          .toList(),
                    ],
                  ),
      ],
    );
  }
======