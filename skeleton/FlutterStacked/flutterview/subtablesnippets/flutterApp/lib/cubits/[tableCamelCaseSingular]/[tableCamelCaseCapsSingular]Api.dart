[subtablefunctions]
------
  static Future<List<dynamic>> load[tableCamelCaseCapsSingular][subtableCamelCaseCapsPlural](
      String parentId, String authToken) async {
    if (ItemCubit.checkLastClick()) return [];
    final url =
        '${Constants.serverUrl}/v1/[tableCodeCase]/list_[subtableCodeCase]/$parentId/$authToken';
    final result = await HttpHelper.get(url);
    if (result["success"])
      return result['data']["items"] ?? [];
    else {
      throw new HttpException("${result['message']}");
    }
  }
  
  /*
  static Future<bool?> updateDetailReceiveQty(
      String detailId, int qty, String authToken) async {
    if (ItemCubit.checkLastClick()) return null;
    final url =
        '${Constants.serverUrl}/v1/[tableCodeCase]/update_[subtableCodeCase]/$authToken';
    final result = await HttpHelper.post(
        url, {"id": detailId, "qty_received": qty.toString()});
    if (result["success"])
      return true;
    else {
      throw new HttpException("${result['message']}");
    }
  }
  */
======
[subtablesnippetsupdatedetail]
------
  static Future<bool?> updateDetailReceiveQty(
      String detailId, int qty, String authToken) async {
    if (ItemCubit.checkLastClick()) return null;
    final url =
        '${Constants.serverUrl}/v1/[tableCodeCase]/update_[subtableCodeCase]/$authToken';
    final result = await HttpHelper.post(
        url, {"id": detailId, "qty_received": qty.toString()});
    if (result["success"])
      return true;
    else {
      throw new HttpException("${result['message']}");
    }
  }
======

