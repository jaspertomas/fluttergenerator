[subtablesnippetimports]
------
import 'screens/[tableCamelCaseCapsSingular][subtableCamelCaseCapsPlural].dart';
======
[detailsmodeswitch]
------
    else if (state is [tableCamelCaseCapsSingular]State[subtableCamelCaseCapsPlural])
      return [tableCamelCaseCapsSingular][subtableCamelCaseCapsPlural]();
======
[detailsbutton]
------
        [subtableCamelCasePlural]button([tableCamelCaseSingular]),
======
[detailsfunction]
------
  Widget [subtableCamelCasePlural]button([tableCamelCaseCapsSingular] [tableCamelCaseSingular]) {
    return IconButton(
      // icon: Icon(Icons.links,
      //     color: [tableCamelCaseCapsSingular]Cubit.mode == [tableCamelCaseCapsSingular]Cubit.LINKS ? Colors.blue : Colors.white, size: 40),
      icon: FaIcon(FontAwesomeIcons.boxes,
          color: (state is [tableCamelCaseCapsSingular]State[subtableCamelCaseCapsPlural]) ? Colors.blue : Colors.grey,
          size: 35),
      onPressed: () {
        [tableCamelCaseCapsSingular]Cubit.showDetails(context);
      },
    );
  }
======
