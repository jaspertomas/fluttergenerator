[detailscode]
------
  static void showDetails(BuildContext context) async {
    instance.emit([tableCamelCaseCapsSingular]StateLoading());
    await load[subtableCamelCaseCapsSingular]List(context);
    instance.emit([tableCamelCaseCapsSingular]State[subtableCamelCaseCapsPlural]());
  }

  static List<dynamic> _[subtableCamelCaseSingular]List = [];
  static List<dynamic> get [subtableCamelCaseSingular]List => _[subtableCamelCaseSingular]List;

  static Future<void> load[subtableCamelCaseCapsSingular]List(BuildContext context) async {
    try {
      _[subtableCamelCaseSingular]List =
          await [tableCamelCaseCapsSingular]Api.load[tableCamelCaseCapsSingular][subtableCamelCaseCapsPlural](
              ItemCubit.itemId, HomeCubit.instance.token);
    } on Exception catch (e) {
      DialogHelper.exceptionDialog(e, context);
    }
  }
======
