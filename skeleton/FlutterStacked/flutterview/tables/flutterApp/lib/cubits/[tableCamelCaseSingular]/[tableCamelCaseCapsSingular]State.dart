part of '[tableCamelCaseCapsSingular]Cubit.dart';

@immutable
abstract class [tableCamelCaseCapsSingular]State {}

/*
class [tableCamelCaseCapsSingular]StateView extends [tableCamelCaseCapsSingular]State {
  [tableCamelCaseCapsSingular]StateView();
  @override
  bool operator ==(Object o) {
    return false;
  }

  @override  
  int get hashCode => item.hashCode ^ loading.hashCode;
}
*/

class [tableCamelCaseCapsSingular]StatePictures extends [tableCamelCaseCapsSingular]State {
  [tableCamelCaseCapsSingular]StatePictures();
}

class [tableCamelCaseCapsSingular]StatePayment extends [tableCamelCaseCapsSingular]State {
  [tableCamelCaseCapsSingular]StatePayment();
}

//[detailsstate]
class [tableCamelCaseCapsSingular]StateInfo extends [tableCamelCaseCapsSingular]State {
  [tableCamelCaseCapsSingular]StateInfo();
}

class [tableCamelCaseCapsSingular]StateHelp extends [tableCamelCaseCapsSingular]State {
  [tableCamelCaseCapsSingular]StateHelp();
}

class [tableCamelCaseCapsSingular]StateHistory extends [tableCamelCaseCapsSingular]State {
  [tableCamelCaseCapsSingular]StateHistory();
}

class [tableCamelCaseCapsSingular]StateLoading extends [tableCamelCaseCapsSingular]State {
  [tableCamelCaseCapsSingular]StateLoading();
}
