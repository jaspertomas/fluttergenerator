import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:flutter/material.dart';
import '../../cubits/home/MyScaffold.dart';
import '../home/HomeCubit.dart';
// import 'package:provider/provider.dart';
import '../../config/Constants.dart';
import 'screens/[tableCamelCaseCapsSingular]Help.dart';
import 'screens/[tableCamelCaseCapsSingular]Pictures.dart';
// import 'screens/[tableCamelCaseCapsSingular][tableCamelCaseCapsPlural].dart';
import 'screens/[tableCamelCaseCapsSingular]Info.dart';
import 'screens/[tableCamelCaseCapsSingular]History.dart';
//[subtablesnippetimports]
// import 'screens/[tableCamelCaseCapsSingular]Grants.dart';
import '../../utils/DialogHelper.dart';
// import '../../utils/NumberHelper.dart';
import '../../models/[tableCamelCaseCapsSingular].dart';
import '[tableCamelCaseCapsSingular]Cubit.dart';
import '[tableCamelCaseCapsSingular]Api.dart';
// import '../AddressViewBloc/AddressViewBloc.dart';
// import '../PickerBloc/PickerBloc.dart';
import 'package:auto_route/auto_route.dart';
import '../../config/app_router.gr.dart' as r;

class [tableCamelCaseCapsSingular]Page extends StatefulWidget {
  // final String message;
  static _[tableCamelCaseCapsSingular]PageState? instance;
  static refresh() {
    if (instance == null) return;
    instance!.refresh();
  }

  const [tableCamelCaseCapsSingular]Page({Key? key}) : super(key: key);

  @override
  _[tableCamelCaseCapsSingular]PageState createState() => _[tableCamelCaseCapsSingular]PageState();
}

class _[tableCamelCaseCapsSingular]PageState extends State<[tableCamelCaseCapsSingular]Page> {
  [tableCamelCaseCapsSingular]State state = [tableCamelCaseCapsSingular]StateInfo();
  bool isLoading = false;
  bool isInit = false;
  bool isDeleteLoading = false;
  refresh() {
    setState(() {});
  }

  @override
  Widget build(BuildContext context) {
    final deviceSize = MediaQuery.of(context).size;
    return WillPopScope(
      onWillPop: () async {
        // You can do some work here.
        // Returning true allows the pop to happen, returning false prevents it.
        // RouterBloc.back(context);
        context.router.push(r.Dashboard());
        return false;
      },
      child: BlocBuilder<[tableCamelCaseCapsSingular]Cubit, [tableCamelCaseCapsSingular]State>(
        bloc: [tableCamelCaseCapsSingular]Cubit.instance,
        /*
      listener: (context, state) {
        if (state is WeatherError) {
          Scaffold.of(context).showSnackBar(
            SnackBar(
              content: Text(state.message),
            ),
          );
        }
      },
      */
        builder: (context, _state) {
          state = _state;
          return MyScaffold(
            title: [tableCamelCaseCapsSingular]Cubit.item.name,
            child: Container(
              height: deviceSize.height - 95,
              child: Column(
                // crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  cancelButton(),
                  dash(),
                  modeSwitch(),
                ],
                //   ),
                // ),
              ),
            ),
          );
        },
      ),
    );
  }

  Widget imageFrame([tableCamelCaseCapsSingular] [tableCamelCaseSingular], Size deviceSize) {
    return [tableCamelCaseSingular].hasImage
        ? Image.network(
            "${Constants.serverUrl}/${[tableCamelCaseSingular].imageUrl}",
            // height: double.infinity,
            width: deviceSize.width / 2,
            fit: BoxFit.contain,
          )
        : SizedBox(height: 0);
  }

  Widget dash() {
    [tableCamelCaseCapsSingular] [tableCamelCaseSingular] = [tableCamelCaseCapsSingular]Cubit.item;
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: Row(
        children: <Widget>[
          Expanded(
            flex: 1,
            // width: deviceSize.width * 0.2,
            child: [tableCamelCaseSingular].hasImage
                ? InkWell(
                    onTap: () {
                      [tableCamelCaseCapsSingular]Cubit.showPictures(context);
                    },
                    child: Image.network(
                      "${Constants.serverUrl}/${[tableCamelCaseSingular].imageUrl}",
                      // height: 150,
                    ),
                  )
                : SizedBox(width: 0),
          ),
          Expanded(
            flex: 4,
            // width: deviceSize.width * 0.7,
            child: Column(
              children: <Widget>[
                // Expanded(
                //   child:
                // ),
                // Html(
                //   data: "<b>[tableCamelCaseCapsSingular]</b> : ${[tableCamelCaseSingular].fullname}",
                // ),
                buttonGrid([tableCamelCaseSingular]),
              ],
            ),
          ),
        ],
      ),
    );
  }

  Widget modeSwitch() {
    if (state is [tableCamelCaseCapsSingular]StateInfo)
      return [tableCamelCaseCapsSingular]Info();
    else if (state is [tableCamelCaseCapsSingular]StateHelp)
      return [tableCamelCaseCapsSingular]Help();
    else if (state is [tableCamelCaseCapsSingular]StatePictures)
      return [tableCamelCaseCapsSingular]Pictures();
    //[detailsmodeswitch]
    else if (state is [tableCamelCaseCapsSingular]StateHistory)
      return [tableCamelCaseCapsSingular]History();
    else if (state is [tableCamelCaseCapsSingular]StateLoading)
      return Center(child: CircularProgressIndicator());
    else
      return SizedBox(height: 0);
  }

  @override
  void dispose() {
    [tableCamelCaseCapsSingular]Cubit.dispose();
    super.dispose();
  }

  Widget buttonGrid([tableCamelCaseCapsSingular] [tableCamelCaseSingular]) {
    return Wrap(
      children: <Widget>[
        infoButton(),
        historyButton(),
        // receiveAllButton(),
        //[detailsbutton]
        // addButton([tableCamelCaseSingular]),
        editButton(),
        picturesButton(),
        refreshButton(),
        helpButton(),
        // backButton([tableCamelCaseSingular]),
      ],
    );
  }

  Widget helpButton() {
    return IconButton(
      icon: Icon(Icons.help,
          color: (state is [tableCamelCaseCapsSingular]StateHelp) ? Colors.blue : Colors.grey,
          size: 40),
      onPressed: () {
        [tableCamelCaseCapsSingular]Cubit.showHelp();
      },
    );
  }

  Widget backButton([tableCamelCaseCapsSingular] [tableCamelCaseSingular]) {
    return IconButton(
      icon: FaIcon(FontAwesomeIcons.arrowLeft, color: Colors.grey, size: 35),
      onPressed: () {
        context.router.push(r.Dashboard());
      },
    );
  }

  Widget infoButton() {
    return IconButton(
      // icon: Icon(Icons.info,
      //     color: [tableCamelCaseCapsSingular]Cubit.mode == [tableCamelCaseCapsSingular]Cubit.INFO ? Colors.blue : Colors.white, size: 40),
      icon: FaIcon(FontAwesomeIcons.infoCircle,
          color: (state is [tableCamelCaseCapsSingular]StateInfo) ? Colors.blue : Colors.grey,
          size: 35),
      onPressed: () {
        [tableCamelCaseCapsSingular]Cubit.showInfo();
      },
    );
  }

  Widget historyButton() {
    return IconButton(
      icon: FaIcon(FontAwesomeIcons.clock,
          color: (state is [tableCamelCaseCapsSingular]StateHistory) ? Colors.blue : Colors.grey,
          size: 35),
      onPressed: () {
        [tableCamelCaseCapsSingular]Cubit.showHistory(context);
      },
    );
  }

/*
  Widget addButton([tableCamelCaseCapsSingular] [tableCamelCaseSingular]) {
    return IconButton(
      // icon: Icon(Icons.info,
      //     color: [tableCamelCaseCapsSingular]Cubit.mode == [tableCamelCaseCapsSingular]Cubit.INFO ? Colors.blue : Colors.white, size: 40),
      icon: FaIcon(FontAwesomeIcons.plus,
          color: [tableCamelCaseCapsSingular]Cubit.mode == [tableCamelCaseCapsSingular]Cubit.NEW[tableUpperCaseSingular]DETAIL
              ? Colors.blue
              : Colors.grey,
          size: 35),
      onPressed: () {
        setState(() {
          [tableCamelCaseCapsSingular]Cubit.mode = [tableCamelCaseCapsSingular]Cubit.NEW[tableUpperCaseSingular]DETAIL;
        });
      },
    );
  }
*/
  Widget refreshButton() {
    return IconButton(
      // icon: Icon(Icons.info,
      //     color: [tableCamelCaseCapsSingular]Cubit.mode == [tableCamelCaseCapsSingular]Cubit.INFO ? Colors.blue : Colors.white, size: 40),
      icon: Icon(Icons.refresh, color: Colors.grey, size: 30),
      //this is disabled if item is null
      onPressed: () =>
          [tableCamelCaseCapsSingular]Cubit.viewHandler("[tableCodeCase];${[tableCamelCaseCapsSingular]Cubit.itemId}", context),
    );
  }

/*
  Widget receiveAllButton() {
    return IconButton(
      icon: FaIcon(FontAwesomeIcons.signInAlt, color: Colors.grey, size: 35),
      onPressed: () async {
        [tableCamelCaseCapsSingular]Cubit.showLoading();
        bool success =
            await [tableCamelCaseCapsSingular]Cubit.receiveAll([tableCamelCaseCapsSingular]Cubit.item.item, context);
        if (success)
          DialogHelper.messageDialog("Successfully Received", context);
        [tableCamelCaseCapsSingular]Cubit.showDetails(context);
      },
    );
  }
  */

  //[detailsfunction]

  Widget picturesButton() {
    return IconButton(
      icon: FaIcon(FontAwesomeIcons.camera,
          color: (state is [tableCamelCaseCapsSingular]StatePictures) ? Colors.blue : Colors.grey,
          size: 35),
      onPressed: () {
        [tableCamelCaseCapsSingular]Cubit.showPictures(context);
      },
    );
  }

  Widget editButton() {
    return IconButton(
      icon: FaIcon(FontAwesomeIcons.edit, color: Colors.grey, size: 35),
      onPressed: () async {
        /*
        RouterBloc.setEditItem([tableCamelCaseCapsSingular]Cubit.item);
        RouterBloc.setActivity(Activity.[tableCamelCaseCapsSingular]Edit);
        */
        String? message;
        try {
          message = await DialogHelper.inputDialog("Update", "", context,
              isMultiline: true);

          if (message.trim().isEmpty) {
          } else {
            setState(() {
              isLoading = true;
            });

            [tableCamelCaseCapsSingular]Cubit.showLoading();
            await [tableCamelCaseCapsSingular]Api.addHistory(
                [tableCamelCaseCapsSingular]Cubit.itemId, message, HomeCubit.instance.token);
            await [tableCamelCaseCapsSingular]Cubit.loadHistoryList(context);
          }
        } on Exception catch (e) {
          DialogHelper.exceptionDialog(e, context);
        }
        //reload histories
        [tableCamelCaseCapsSingular]Cubit.showHistory(context);

        setState(() {
          isLoading = false;
        });
      },
    );
  }

  Widget cancelButton() {
    return Column(
      children: [
        ElevatedButton(
          child: Text("Back"),
          onPressed: () async {
            // FolderCubit.viewHandler("fol;${cubit.id}", context);
            AutoRouter.of(context).pop();
          },
        ),
        SizedBox(
          height: 10,
        ),
      ],
    );
  }
}
