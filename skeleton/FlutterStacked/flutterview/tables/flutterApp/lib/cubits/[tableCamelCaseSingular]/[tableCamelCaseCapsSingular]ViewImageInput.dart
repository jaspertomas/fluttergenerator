import 'dart:io';

import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';
import 'package:librarian/cubits/home/HomePage.dart';
// import 'package:path/path.dart' as path;
// import 'package:path_provider/path_provider.dart' as syspaths;
import '../../config/Constants.dart';

class [tableCamelCaseCapsSingular]ViewImageInput extends StatelessWidget {
  final Function? fileHandler;
  final picker = ImagePicker();
  [tableCamelCaseCapsSingular]ViewImageInput({@required this.fileHandler});
  Future<File?> _takePicture() async {
    final imageFile = await picker.getImage(
      source: ImageSource.camera,
      maxWidth: 600,
    );
    if (imageFile == null) return null;
    return File(imageFile.path);
  }

  @override
  Widget build(BuildContext context) {
    return ElevatedButton.icon(
      icon: Icon(Icons.camera),
      label: Text('Take Picture (Max ${Constants.MAX_PICTURES})'),
      onPressed: () async {
        File? picture = await _takePicture();
        await fileHandler!(picture);
        HomePage.rebuild();
      },
    );
  }

/*
  void _choose(File file) async {
    file = await ImagePicker.pickImage(source: ImageSource.camera);
// file = await ImagePicker.pickImage(source: ImageSource.gallery);
  }
  */

}
