import 'package:flutter/material.dart';
import './[tableCamelCaseCapsSingular]HistoryListItem.dart';
import '../[tableCamelCaseCapsSingular]Cubit.dart';
import '../../../config/Constants.dart';

class [tableCamelCaseCapsSingular]History extends StatefulWidget {
  const [tableCamelCaseCapsSingular]History({Key? key}) : super(key: key);

  @override
  _[tableCamelCaseCapsSingular]HistoryState createState() => _[tableCamelCaseCapsSingular]HistoryState();
}

class _[tableCamelCaseCapsSingular]HistoryState extends State<[tableCamelCaseCapsSingular]History> {
  late ScrollController _scrollController;

  bool isLoading = true;
  bool isInit = false;
  @override
  void didChangeDependencies() async {
    if (!isInit) {
      [tableCamelCaseCapsSingular]Cubit.loadHistoryList(context);
      _scrollController = ScrollController();
      //when scroll reaches bottom, load more records
      _scrollController.addListener(() {
        if (_scrollController.position.pixels ==
            _scrollController.position.maxScrollExtent) {
          [tableCamelCaseCapsSingular]Cubit.loadMoreHistoryList(context);
        }
      });

      setState(() {
        isLoading = false;
        isInit = true;
      });
    }
    super.didChangeDependencies();
  }

  @override
  Widget build(BuildContext context) {
    List<dynamic> list = [tableCamelCaseCapsSingular]Cubit.historyList;
    return histories(list);
  }

  Widget histories(List list) {
    final deviceSize = MediaQuery.of(context).size;
    return Column(
      children: <Widget>[
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: <Widget>[
            Text("History", style: Constants.textStyleFontSize20Bold),
            IconButton(
              icon: Icon(Icons.refresh, color: Colors.blue, size: 30),
              onPressed: () async {
                setState(() {
                  isLoading = true;
                });
                await [tableCamelCaseCapsSingular]Cubit.loadHistoryList(context);
                setState(() {
                  isLoading = false;
                });
              },
            ),
          ],
        ),
        isLoading
            ? Center(
                child: CircularProgressIndicator(),
              )
            : list.length == 0
                ? Text(
                    "No items found",
                    style: TextStyle(fontSize: 20),
                  )
                : Container(
                    height: deviceSize.height - 285,
                    child: SingleChildScrollView(
                      child: Column(
                        children: list
                            .map((item) => [tableCamelCaseCapsSingular]HistoryListItem(item))
                            .toList(),
                      ),
                    ),
                  ),
      ],
    );
  }
}
