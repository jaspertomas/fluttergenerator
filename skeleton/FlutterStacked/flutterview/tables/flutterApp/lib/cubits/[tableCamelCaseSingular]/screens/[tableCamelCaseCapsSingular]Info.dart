// // import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:flutter/material.dart';
// import './[tableCamelCaseCapsSingular]HistoryListItem.dart';
//[subtablesnippetimports]
import '../[tableCamelCaseCapsSingular]Cubit.dart';
import '../../item/ItemCubit.dart';
// import '../../PickerBloc/PickerBloc.dart';
import '../../../config/Constants.dart';
import '../../../models/[tableCamelCaseCapsSingular].dart';
import '../../../utils/NumberHelper.dart';
import '../../../utils/DateHelper.dart';
import '../../../utils/ScreenHelper.dart';

class [tableCamelCaseCapsSingular]Info extends StatefulWidget {
  const [tableCamelCaseCapsSingular]Info({Key? key}) : super(key: key);

  @override
  _[tableCamelCaseCapsSingular]InfoState createState() => _[tableCamelCaseCapsSingular]InfoState();
}

class _[tableCamelCaseCapsSingular]InfoState extends State<[tableCamelCaseCapsSingular]Info> {
  // ScrollController _scrollController;

  bool isLoading = true;
  bool isInit = false;
  @override
  void didChangeDependencies() async {
    if (!isInit) {
      //[detailsinitializer]
      // _scrollController = ScrollController();
      //when scroll reaches bottom, load more records
      // _scrollController.addListener(() {
      //   if (_scrollController.position.pixels ==
      //       _scrollController.position.maxScrollExtent) {
      //     [tableCamelCaseCapsSingular]Cubit.loadMoreHistoryList(context);
      //   }
      // });

      setState(() {
        isLoading = false;
        isInit = true;
      });
    }
    super.didChangeDependencies();
  }

  @override
  Widget build(BuildContext context) {
    [tableCamelCaseCapsSingular] [tableCamelCaseSingular] = new [tableCamelCaseCapsSingular](ItemCubit.item!);
    const List<int> widths = [1, 2];

    return Expanded(
      child: Padding(
        padding: const EdgeInsets.symmetric(vertical: 0, horizontal: 8),
        child: SingleChildScrollView(
          child: Column(
            children: <Widget>[
              // ProductRow([tableCamelCaseSingular]),
              // [superCamelCaseSingular]Row([tableCamelCaseSingular]),
              SizedBox(height: 10),
              ScreenHelper.tableRow(widths, [
                "[tableNameField]: ",
                "${[tableCamelCaseSingular].[tableNameField]}",
              ]),
              SizedBox(height: 10),
              ScreenHelper.tableRow(widths, [
                "Created At: ",
                "${DateHelper.toShortDateTime(DateHelper.fromZulu([tableCamelCaseSingular].createdAt))}",
              ]),
              SizedBox(height: 10),
              // ScreenHelper.tableRowForWidgets(widths, [
              //   Text(
              //     "Reference: ",
              //     style: Constants.textStyleFontSize18,
              //   ),
              //   refRow(voucher),
              // ]),
              /*
              [tableCamelCaseSingular].hasImage
                  ? Image.network(
                      "${Constants.serverUrl}/${[tableCamelCaseSingular].item['image_url']}",
                      width: deviceSize.width / 8 * 7,
                      fit: BoxFit.contain,
                    )
                  : SizedBox(width: 0),
                  */
              details(),
            ],
            //   ),
            // ),
          ),
        ),
      ),
    );
  }

/*
  Widget [superCamelCaseSingular]Row([tableCamelCaseCapsSingular] [tableCamelCaseSingular]) {
    if ([tableCamelCaseSingular].[superCamelCaseSingular]Id.isEmpty) return SizedBox(height: 0);
    return InkWell(
      onTap: () {
        RouterBloc.viewHandler("[superCodeCase];${[tableCamelCaseSingular].[superCamelCaseSingular]Id}", context);
      },
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Expanded(
            child: Text(
              "Address: ${[tableCamelCaseSingular].[superCamelCaseSingular]}",
              style: Constants.textStyleFontSize18,
              textAlign: TextAlign.center,
            ),
          ),
          IconButton(
            icon: FaIcon(
              FontAwesomeIcons.chevronCircleRight,
              color: Colors.black,
            ),
            onPressed: null,
          )
        ],
      ),
    );
  }
  */
  //[detailsfunction]
}
