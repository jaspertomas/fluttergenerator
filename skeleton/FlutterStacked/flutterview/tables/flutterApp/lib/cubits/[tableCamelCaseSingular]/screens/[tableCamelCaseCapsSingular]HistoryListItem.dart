import 'package:flutter/material.dart';
// import 'package:provider/provider.dart';
// import '../../item/ItemCubit.dart';// import '../[tableCamelCaseCapsSingular]Cubit.dart';
// import '../../../utils/DialogHelper.dart';
import '../../../config/Constants.dart';
import '../../../models/History.dart';

class [tableCamelCaseCapsSingular]HistoryListItem extends StatefulWidget {
  final Map<String, dynamic> item;
  // final [tableCamelCaseCapsSingular]Cubit [tableCamelCaseCapsSingular]Cubit;

  [tableCamelCaseCapsSingular]HistoryListItem(this.item);

  @override
  _[tableCamelCaseCapsSingular]HistoryListItemState createState() =>
      _[tableCamelCaseCapsSingular]HistoryListItemState();
}

class _[tableCamelCaseCapsSingular]HistoryListItemState extends State<[tableCamelCaseCapsSingular]HistoryListItem> {
  bool isActionLoading = false;
  bool isLoading = false;

  @override
  Widget build(BuildContext context) {
    History history = History(widget.item);
    return Card(
      child: ListTile(
        // onTap: select,
        /*
        leading: history.hasImage
            ? Image.network(
                "${Constants.serverUrl}/${history.imageUrl}",
              )
            : SizedBox(width: 0),
        */
        title: Text(
          "${history.date} by ${history.username}: ${history.description}",
          style: Constants.textStyleFontSize20,
        ),
      ),
    );
  }
}
