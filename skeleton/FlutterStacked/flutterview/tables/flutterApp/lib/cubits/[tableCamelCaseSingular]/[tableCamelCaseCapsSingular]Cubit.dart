import 'package:flutter/material.dart';
import 'package:bloc/bloc.dart';
import 'package:meta/meta.dart';
import '../../cubits/home/HomeCubit.dart';
import '../../cubits/item/ItemCubit.dart';
import '../../models/[tableCamelCaseCapsSingular].dart';
import '../../utils/DialogHelper.dart';

import '[tableCamelCaseCapsSingular]Api.dart';

part '[tableCamelCaseCapsSingular]State.dart';

class [tableCamelCaseCapsSingular]Cubit extends Cubit<[tableCamelCaseCapsSingular]State> {
  [tableCamelCaseCapsSingular]Cubit() : super([tableCamelCaseCapsSingular]StateInfo()) {
    _item = new [tableCamelCaseCapsSingular](ItemCubit.item ?? {});
  }

  static [tableCamelCaseCapsSingular]Cubit? _instance;

  static [tableCamelCaseCapsSingular]Cubit get instance {
    if (_instance == null) _instance = new [tableCamelCaseCapsSingular]Cubit();
    return _instance!;
  }

  static void dispose() {
    if (_instance == null) return;
    _instance!.close();
    _instance = null;
  }

  late [tableCamelCaseCapsSingular] _item;
  static [tableCamelCaseCapsSingular] get item => instance._item;
  static String get itemId => instance._item.id;

  /*
  refresh(BuildContext context) {
    if (ItemCubit.item == null || ItemCubit.item!["type"] != '[tableCodeCase]')
      emit([tableCamelCaseCapsSingular]StateLoading());
    switch (mode) {
      case INFO:
        showInfo();
        break;
      case PICTURES:
        showPictures();
        break;
      case [subtableUpperCaseSingular]:
        showDetails(context);
        break;
    }
  }
  */

  static void showLoading() {
    instance.emit([tableCamelCaseCapsSingular]StateLoading());
  }

  static void showInfo() {
    instance.emit([tableCamelCaseCapsSingular]StateInfo());
  }

  static void showHelp() {
    instance.emit([tableCamelCaseCapsSingular]StateHelp());
  }

  static void showHistory(BuildContext context) async {
    instance.emit([tableCamelCaseCapsSingular]StateLoading());
    await loadHistoryList(context);
    instance.emit([tableCamelCaseCapsSingular]StateHistory());
  }

  static void showPictures(BuildContext context) async {
    instance.emit([tableCamelCaseCapsSingular]StateLoading());
    await loadPictureList(context);
    instance.emit([tableCamelCaseCapsSingular]StatePictures());
  }

  //[detailscode]
  
  static List<dynamic> _historyList = [];
  static List<dynamic> get historyList => _historyList;
  static int _historyPage = 0;
  static Future<void> loadHistoryList(BuildContext context) async {
    try {
      _historyList = await [tableCamelCaseCapsSingular]Api.load[tableCamelCaseCapsSingular]History(
          '[tableCodeCase]', ItemCubit.itemId, 1, HomeCubit.instance.token);
      _historyPage = 1;
    } on Exception catch (e) {
      DialogHelper.exceptionDialog(e, context);
    }
  }

  static Future<void> loadMoreHistoryList(BuildContext context) async {
    try {
      final data = await [tableCamelCaseCapsSingular]Api.load[tableCamelCaseCapsSingular]History('[tableCodeCase]',
          ItemCubit.itemId, (_historyPage + 1), HomeCubit.instance.token);

      if (data.isNotEmpty) {
        _historyPage += 1;
        _historyList = [..._historyList, ...data];
      }
    } on Exception catch (e) {
      DialogHelper.exceptionDialog(e, context);
    }
  }

  static loadPictureList(BuildContext context) async {
    try {
      return await [tableCamelCaseCapsSingular]Api.load[tableCamelCaseCapsSingular]Pictures(
          '[tableCodeCase]', ItemCubit.itemId, HomeCubit.instance.token);
    } on Exception catch (e) {
      DialogHelper.exceptionDialog(e, context);
    }
  }

  static Future<void> deleteImage(
      Map<String, dynamic> item, String reason, BuildContext context) async {
    try {
      await [tableCamelCaseCapsSingular]Api.deleteImage(
          item, reason, HomeCubit.instance.token);
    } on Exception catch (e) {
      DialogHelper.exceptionDialog(e, context);
    }
  }

/*
  static Future<void> delete(
      Map<String, dynamic> item, String reason, BuildContext context) async {
    try {
      await [tableCamelCaseCapsSingular]Api.delete(item, reason, HomeCubit.instance.token);
    } on Exception catch (e) {
      DialogHelper.exceptionDialog(e, context);
    }
  }

  static Future<void> undelete(
      Map<String, dynamic> item, BuildContext context) async {
    try {
      await [tableCamelCaseCapsSingular]Api.undelete(item, HomeCubit.instance.token);
    } on Exception catch (e) {
      DialogHelper.exceptionDialog(e, context);
    }
  }
  static Future<bool> receiveAll(
      Map<String, dynamic> item, BuildContext context) async {
    try {
      var result =
          await [tableCamelCaseCapsSingular]Api.receiveAll(item, HomeCubit.instance.token);
      return result ?? false;
    } on Exception catch (e) {
      DialogHelper.exceptionDialog(e, context);
      return false;
    }
  }
  */

  static Future<bool> viewHandler(String? code, BuildContext context) async {
    return await ItemCubit.viewHandler(code, context);
  }
}
