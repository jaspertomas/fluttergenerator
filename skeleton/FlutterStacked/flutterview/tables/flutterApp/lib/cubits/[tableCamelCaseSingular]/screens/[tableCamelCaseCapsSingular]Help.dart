import 'package:flutter/material.dart';
import 'package:flutter_html/flutter_html.dart';
// import '../../../config/Constants.dart';

class [tableCamelCaseCapsSingular]Help extends StatelessWidget {
  static final String title = "[tableCamelCaseCapsSingular] View Help";
  static final String helpText = "Hello World";
/*
  static final String helpText = """
<center><b>[tableCamelCaseCapsSingular] Screen Help</b></center>
  <p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Here are the meanings of the buttons in the [tableCamelCaseCapsSingular] screen.
  </p>
  <hr>
  <p> </p>
  <img width=50 src="${Constants.serverUrl}/images/menu.jpg">
  <center>Main Menu<br><br>Access more buttons like [tableCamelCaseCapsSingular] Search and QR Code Scan</center>
  <hr>
  <img width=50 src="${Constants.serverUrl}/images/back.jpg">
  <center>Back to Address</center>
  <hr>
  <img width=50 src="${Constants.serverUrl}/images/info.jpg">
  <center>Information and History<br><br>Contains past petitions reports and grants</center>
  <hr>
  <img width=50 src="${Constants.serverUrl}/images/house.jpg">
  <center>Go to Address</center>
  <hr>
  <img width=50 src="${Constants.serverUrl}/images/hands.jpg">
  <center>New Petition<br><br>Ask for food, medicine or other kinds of assistance for this [tableCamelCaseCapsSingular] from the barangay</center>
  <hr>
  <img width=50 src="${Constants.serverUrl}/images/flag.jpg">
  <center>New Report<br><br>Report what's going on regarding this [tableCamelCaseCapsSingular]</center>
  <hr>
  <img width=50 src="${Constants.serverUrl}/images/gift.jpg">
  <center>Grants<br><br>Keep track of whether or not this [tableCamelCaseCapsSingular] has received barangay giveaways</center>
  <hr>
  <img width=50 src="${Constants.serverUrl}/images/edit.jpg">
  <center>Edit</center>
  <hr>
  <img width=50 src="${Constants.serverUrl}/images/truck.jpg">
  <center>Transfer <br><br>Move [tableCamelCaseCapsSingular] to a different address. <br>Please create the new address first</center>
  <hr>
  <img width=50 src="${Constants.serverUrl}/images/trash.jpg">
  <center>Delete</center>
  <hr>
  <img width=50 src="${Constants.serverUrl}/images/reload.jpg">
  <center>Reload from Internet</center>
  <hr>
  <img width=50 src="${Constants.serverUrl}/images/help.jpg">
  <center>Help</center>
  <hr>




<hr>
<p> </p>
<h2>Creating [tableCamelCaseCapsPlural]</h2>
  <p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
  To create an [tableCamelCaseCapsSingular], you first need a you need an address. Go ahead and create the address first if necessary. For instructions, click on the Menu Button, then click Help, and then click Addresses.
  </p>

  <hr>
  <img width=50 src="${Constants.serverUrl}/images/menu.jpg">
  <center>Menu Button</center>
  <hr>
  <p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
  Once you have your address, go to its page. After selecting your barangay, select the street and then the address.
  </p>

  <p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
  This is the Address View. From here, click on the [tableCamelCaseCapsSingular] Button. 
  </p>

  <p> </p>
  <hr>
  <img src="${Constants.serverUrl}/images/addressview.jpg">
  <center>Address View Screen</center>
  <p> </p>
  <hr>
  <img width=50 src="${Constants.serverUrl}/images/person.jpg">
  <center>[tableCamelCaseCapsSingular] Button</center>
  <hr>
  <p> </p>

  <p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
  You will probably an empty list of Household Members.
  </p>

  <p> </p>
  <hr>
  <img src="${Constants.serverUrl}/images/address[tableCamelCaseCapsPlural].jpg">
  <hr>
  <p> </p>
 
  <p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
  Click on the Plus sign to add a [tableCamelCaseCapsSingular]. 
  </p>

  <p> </p>
  <hr>
  <img src="${Constants.serverUrl}/images/new[tableCamelCaseCapsSingular].jpg">
  <hr>
  <p> </p>
 
  <p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
  Enter a first name, a last name and any other details you wish to add. Click Submit. 
  </p>

  <p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
  If somebody with the same first, middle and last names already exist in your barangay, and error message will appear. 
  </p>

  <p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
  If there really are two people with the same exact name in your barangay, just change the capitalization a little. 
  </p>

  <p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
  After saving your changes, you will see this. 
  </p>

  <p> </p>
  <hr>
  <img src="${Constants.serverUrl}/images/[tableCamelCaseCapsPlural]ubmitactionlist.jpg">
  <hr>
  <p> </p>
 
  <p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
  Congratulations, you have entered a [tableCamelCaseCapsSingular] into the system.
  </p>


<hr>
<p> </p>
<h2>Moving a [tableCamelCaseCapsSingular] to another address</h2>
  <p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
  You can request that a [tableCamelCaseCapsSingular] be transferred to another address. To transfer everybody living in an address, go to the page of the current address and click on the Transfer Button there.
  </p>

  <p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
  To move a [tableCamelCaseCapsSingular] to another address, first make sure that the destination address exists. 
  </p>

  <p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
  Click on the Transfer Button. You will have to provide a reason for the transfer. 
  </p>

  <hr>
  <img width=50 src="${Constants.serverUrl}/images/truck.jpg">
  <center>Transfer Button</center>
  <hr>

  <p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
  A list of streets will appear. Select the destination street.
  </p>

<p> </p>
<hr>
  <img src="${Constants.serverUrl}/images/transfer.jpg">
<hr>
<p> </p>

  <p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
  Then a list of addresses will appear. Select the destination address.
  </p>

  <p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
  The barangay admins will have to approve your transfer before it actually happens. Until then, the [tableCamelCaseCapsPlural] will be marked as "For Transfer".
  </p>

<p>  </p>
<hr>
  <img src="${Constants.serverUrl}/images/fortransfer.jpg">
<hr>
<p>  </p>

  <p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
  </p>

  <p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
  </p>

  <p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
  </p>

  <p> </p>
  <p>
  </p>
  <p>
  </p>
  """;
  */
  @override
  Widget build(BuildContext context) {
    final deviceSize = MediaQuery.of(context).size;
    return Container(
      height: deviceSize.height - 260,
      child: SingleChildScrollView(
        child: Html(
          data: helpText,
        ),
      ),
    );
  }
}
